<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Musculo extends Model {
    protected $table = 'musculos';
    protected $primaryKey = 'cd_musculo';
    // Relationships
    public function exercicios() {
        return $this->belongsToMany('App\Exercicio', 'musculo_exercicio', 'cd_musculo', 'cd_exercicio');
    }
}
