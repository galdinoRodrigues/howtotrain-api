<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Treino extends Model {
    protected $table = 'treinos';
    protected $primaryKey = 'cd_treino';
    public $timestamps = false;
    // Relationships
    public function grupo() {
        return $this->belongsTo('App\Grupo', 'cd_grupo', 'cd_grupo');
    }
    public function exercises() {
        return $this->belongsToMany('App\Exercicio', 'treino_exercicio', 'cd_treino', 'cd_exercicio');
    }
    public function plans() {
        return $this->belongsToMany('App\Plano', 'plano_treino', 'cd_treino', 'cd_plano');
    }
}
