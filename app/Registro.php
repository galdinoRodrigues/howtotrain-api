<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registro extends Model {
    protected $table = 'registros';
    protected $primaryKey = 'cd_registro';
    public $timestamps = false;
    public $hidden = ['dt_alteracao', 'dt_inclusao', 'dt_ultima_utilizacao', 'fg_excluido'];
    // Relationships
    public function pessoa() {
        return $this->belongsTo('App\Pessoa', 'cd_registro', 'cd_registro');
    }
    public function login() {
        return $this->belongsTo('App\Login', 'cd_registro', 'cd_registro');
    }
}
