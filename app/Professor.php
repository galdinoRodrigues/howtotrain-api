<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model {
    protected $table = 'professores';
    protected $primaryKey = 'cd_professor';
    public $timestamps = false;
    // Relationships
    public function pessoa() {
        return $this->hasOne('App\Pessoa', 'cd_pessoa', 'cd_pessoa');
    }
    public function alunos() {
        return $this->belongsToMany('App\Aluno', 'aluno_professor', 'cd_professor', 'cd_aluno');
    }
    public function planos() {
        return $this->hasMany('App\Plano', 'cd_professor', 'cd_plano');
    }
}
