<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {
    protected $table = 'post';

    public function pessoa() {
        return $this->hasOne('App\Pessoa', 'cd_pessoa', 'cd_pessoa');
    }
    public function empresa() {
        return $this->hasOne('App\Empresa', 'cd_empresa', 'id');
    }

}
