<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plano extends Model {
    protected $table = 'planos';
    protected $primaryKey = 'cd_plano';
    // Relationships
    public function professor() {
        return $this->belongsTo('App\Professor', 'cd_professor', 'cd_professor');
    }
    public function planoOriginal() {
        return $this->belongsTo('App\Plano', 'cd_plano_original', 'cd_plano');
    }
    public function trains() {
        return $this->belongsToMany('App\Treino', 'plano_treino', 'cd_plano', 'cd_treino');
    }
    public function alunos() {
        return $this->belongsToMany('App\Aluno', 'aluno_plano', 'cd_plano', 'cd_aluno');
    }

    public $timestamps = false;
}
