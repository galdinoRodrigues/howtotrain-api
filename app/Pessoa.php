<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model {
    protected $table = 'pessoas';
    protected $primaryKey = 'cd_pessoa';
    public $timestamps = false;
    // Relationships
    public function aluno() {
        return $this->belongsTo('App\Aluno', 'cd_pessoa', 'cd_pessoa');
    }
    public function registro() {
        return $this->hasOne('App\Registro', 'cd_registro', 'cd_registro');
    }
    public function professor() {
        return $this->belongsTo('App\Professor', 'cd_pessoa', 'cd_pessoa');
    }
    public function academia() {
        return $this->hasOne('App\Empresa', 'cd_empresa', 'id');
    }
    public function friends() {
        return $this->belongsToMany('App\Pessoa', 'amizades', 'cd_from', 'cd_to');
    }
}
