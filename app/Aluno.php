<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model {
    protected $table = 'alunos';
    protected $primaryKey = 'cd_aluno';
    public $timestamps = false;
    // Relationships
    public function pessoa() {
        return $this->hasOne('App\Pessoa', 'cd_pessoa', 'cd_pessoa');
    }
    public function professores() {
        return $this->belongsToMany('App\Professor', 'aluno_professor', 'cd_aluno', 'cd_professor');
    }
    public function planos() {
        return $this->belongsToMany('App\Plano', 'aluno_plano', 'cd_aluno', 'cd_plano');
    }
}
