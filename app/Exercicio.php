<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercicio extends Model {
    protected $table = 'exercicios';
    protected $primaryKey = 'cd_exercicio';
    // Relationships
    public function grupo() {
        return $this->belongsTo('App\Grupo', 'cd_grupo', 'cd_grupo');
    }
    public function musculos() {
        return $this->belongsToMany('App\Musculo', 'musculo_exercicio', 'cd_exercicio', 'cd_musculo');
    }
}
