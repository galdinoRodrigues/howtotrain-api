<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model {
    protected $table = 'empresa';

    public function pessoa() {
        return $this->belongsTo('App\Pessoa', 'cd_empresa', 'id');
    }

    public function endereco() {
        return $this->hasOne('App\Endreco', 'cd_endereco', 'cd_endereco');
    }

}
