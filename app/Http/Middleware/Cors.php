<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    public function handle($request, Closure $next)
    {
        $allowedOrigins = [
            'howtotrain.com.br',
            'http://howtotrain.com.br',
            'http://howtotrain.com.br/',
            'https://howtotrain.com.br',
            'https://howtotrain.com.br/',
            'http://3.129.189.165',
            'http://3.129.189.165/'
        ];

        $origin =  $request->getHttpHost();

        if (in_array($origin, $allowedOrigins)) {
            return $next($request)->
                header('Access-Control-Allow-Origin', $origin)->
                header('Access-Control-Allow-Methods', 'POST, OPTIONS');
        }
        return $next($request);
    }
}
