<?php

namespace App\Http\Controllers;

use DB;
use Validator;

use App\Registro;
use App\Admin;
use App\Aluno;
use App\Professor;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class ReportsController extends Controller {
    public function countStudents() {
        $count = DB::
        select("
            SELECT
                COUNT(1) AS count
            FROM
                alunos AS a
            INNER JOIN
                pessoas AS p
                    ON p.cd_pessoa = a.cd_pessoa
            INNER JOIN
                registros AS r
                    ON r.cd_registro = p.cd_registro
            WHERE
                IFNULL(r.fg_excluido, 0) <> 1
            ;"
        );
        return Response([ 'count' => $count[0]->count ]);
    }
    public function countTeachers() {
        $count = DB::
        select("
            SELECT
                COUNT(1) AS count
            FROM
                professores AS prof
            INNER JOIN
                pessoas AS p
                    ON p.cd_pessoa = prof.cd_pessoa
            INNER JOIN
                registros AS r
                    ON r.cd_registro = p.cd_registro
            WHERE
                IFNULL(r.fg_excluido, 0) <> 1
            ;"
        );
        return Response([ 'count' => $count[0]->count ]);
    }
    public function countGyms() {
        $count = DB::
        select("
            SELECT
                COUNT(1) AS count
            FROM
                empresas
            WHERE
                id_tipo_empresa = 3 AND
                IFNULL(fg_excluido, 0) = 0 AND
                IFNULL(fg_ativo, 0) = 1
            ;"
        );
        return Response([ 'count' => $count[0]->count ]);
    }
    public function countExercises() {
        $count = DB::
        select("
            SELECT
                COUNT(1) AS count
            FROM
                exercicios
            ;"
        );
        return Response([ 'count' => $count[0]->count ]);
    }
    public function countTrains() {
        return Response([ 'count' => 0 ]);
    }
}
