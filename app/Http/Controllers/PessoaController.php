<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Login;
use App\Registro;
use App\Aluno;
use App\Pessoa;
use DB;
use JWTAuth;
use Auth;

class PessoaController extends Controller
{
    public function getProfileV2()
    {
        try
        {
            $jwtLogin = Auth::user();
            $login = Login::
                with('registro')
                ->where('cd_login', $jwtLogin->cd_login)
                ->first();

            if (!$login || !$login->registro )
            {
                return Response([ 'msg' => 'Não autorizado' ], 401);
            }

            $usuario = Registro::
                where('registros.cd_registro', $login->cd_registro)
                ->join('logins','logins.cd_registro','=','registros.cd_registro')
                ->join('pessoas','pessoas.cd_registro','=','registros.cd_registro')
                ->leftJoin('alunos','alunos.cd_pessoa','=','pessoas.cd_pessoa')
                ->leftJoin('professores','professores.cd_pessoa','=','pessoas.cd_pessoa')
                ->leftJoin('enderecos','enderecos.cd_endereco','=','registros.cd_endereco')
                ->leftJoin('cidades', 'cidades.cd_cidade', '=','enderecos.cd_cidade')
                ->leftJoin('estados', 'estados.cd_estado', '=','cidades.cd_estado')
                ->leftJoin('pais','estados.cd_pais','=','pais.cd_pais')
                ->leftJoin('empresa','empresa.id','=','pessoas.cd_empresa')
                ->select(
                    'registros.cd_registro as cdRegistroExterno',
                    'registros.ind_registro as indTipoRegistro',
                    'registros.ds_avatar as dsAvatar',
                    'pessoas.ind_sexo as sexo',
                    'pessoas.cd_pessoa as cdPessoa',
                    'logins.ds_email as email',
                    'registros.ds_nome as dsNome',
                    'alunos.cd_aluno',
                    'alunos.nr_peso as nrPeso',
                    'alunos.cm_altura as cmAltura',
                    'alunos.ind_condicao_fisica as indCondicaoFisica',
                    'alunos.ind_tipo_treino as indTipoTreino',
                    'professores.cd_professor',
                    'professores.ds_cref as dsCref',
                    'professores.dt_cref as dtCref',
                    'pessoas.dt_nascimento as sdtNascimento',
                    'professores.nr_valor_mensal as nrValorMensal',
                    'pessoas.ind_sexo as masculino',
                    'enderecos.ds_cidade as dsCidade',
                    'enderecos.ds_ut as dsUf',
                    'pais.ds_pais as dsPais',
                    'pais.ds_codigo as dsCodigoPais',
                    'empresa.id AS cdEmpresa',
                    'empresa.nome as dsNomeEmpresa'
                )
                ->first();

            if (count($usuario) != 0)
            {
                $usuario['sexo'] = ($usuario['sexo'] == 1) ? true : false;
                $usuario['masculino'] = ($usuario['masculino'] == 1) ? true : false;
                $usuario['endereco'] = [
                    'dsCidade' => $usuario['dsCidade'],
                    'dsUf' => $usuario['dsUf'],
                    'dsPais' => $usuario['dsPais'],
                    'dsCodigoPais' => $usuario['dsCodigoPais']
                ];

                if ($usuario['cdEmpresa'])
                {
                    $usuario['gym'] = [
                        'id' => $usuario['cdEmpresa'],
                        'name' => $usuario['dsNomeEmpresa']
                    ];
                }
                unset($usuario['cdEmpresa']);
                unset($usuario['dsNomeEmpresa']);

                if ($usuario['cd_aluno'])
                {
                    unset($usuario['cd_professor']);
                    unset($usuario['dsCref']);
                    unset($usuario['dtCref']);
                    unset($usuario['nrValorMensal']);
                }
                else
                {
                    unset($usuario['cd_aluno']);
                    unset($usuario['nrPeso']);
                    unset($usuario['cmAltura']);
                    unset($usuario['indCondicaoFisica']);
                    unset($usuario['indTipoTreino']);
                }

                $countNewFriends = DB::
                    select("
                        SELECT
                            COUNT(1) AS count
                        FROM
                            amizades
                        WHERE
                            fg_aceito <> 1 AND
                            cd_to = ?
                        ;",
                        [ $usuario['cdPessoa'] ]
                    );

                $usuario['friendship_notifications'] = $countNewFriends[0]->count;

                return Response($usuario, 200);
            }

            return Response([ 'msg' => 'Perfil não encontrado' ],404);
        }
        catch(\Exception $e)
        {
            return Response([
                'msg' => 'Erro ao consultar perfil',
                'error' => $e
            ], 400);
        }
    }
}
