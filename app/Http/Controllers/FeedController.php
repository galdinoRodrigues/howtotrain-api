<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Pessoa;
use App\Professor;
use App\Aluno;
use App\Login;
use JWTAuth;
use DB;
use Auth;
use Carbon\Carbon;

class FeedController extends Controller {
    function list(Request $request) {

        $data = Input::all();
        $rules = [
            'pageIndex' => 'integer',
            'pageSize' => 'integer'
        ];
        $validation = Validator::make($data, $rules);

        /** Faz a validação do token */

        $jwtLogin = Auth::user();

        $login = Login::
            with(
                'registro',
                'registro.pessoa'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa) {
            return Response([
                'msg' => 'Não autorizado'
            ], 401);
        }

        $gym = $login->registro->pessoa->cd_empresa;

        $pageIndex = $data['pageIndex'] ?? 1;
        $pageSize = $data['pageSize'] ?? 10;

        $pageIndex = (int) $pageIndex;
        $pageSize = (int) $pageSize;

        $from = ($pageIndex - 1) * $pageSize;

        $args = [
            $gym,
            $gym,
            $pageSize,
            $from
        ];

        $posts = DB::
            select("
                SELECT
                    post.id AS id,
                    r.ds_avatar AS picture_holder_url,
                    post.picture_url AS picture_post_url,
                    r.ds_nome AS holder_name,
                    post.description AS description,
                    post.qtd_like AS like_quantity,
                    post.qtd_comment AS comment_quantity,
                    0.0 AS price,
                    IFNULL(post.type_id, 1) AS type
                FROM post
                    INNER JOIN pessoas p ON
                        p.cd_pessoa = post.cd_pessoa
                    INNER JOIN registros r ON
                        r.cd_registro = p.cd_registro
                WHERE
                    post.cd_pessoa IS NOT NULL AND
                    p.cd_empresa = ?

                UNION

                SELECT
                    post.id AS id,
                    e.url_imagem AS picture_holder_url,
                    post.picture_url AS picture_post_url,
                    e.nome AS holder_name,
                    post.description AS description,
                    post.qtd_like AS like_quantity,
                    post.qtd_comment AS comment_quantity,
                    p.preco AS price,
                    IFNULL(post.type_id, 3) AS type
                FROM post
                    INNER JOIN product p ON
                        p.id = post.cd_produto
                    INNER JOIN empresa e ON
                        e.id = p.id_empresa
                WHERE
                    post.cd_produto IS NOT NULL

                UNION

                SELECT
                    post.id AS id,
                    e.url_imagem AS picture_holder_url,
                    post.picture_url AS picture_post_url,
                    e.nome AS holder_name,
                    post.description AS description,
                    post.qtd_like AS like_quantity,
                    post.qtd_comment AS comment_quantity,
                    0.0,
                    IFNULL(post.type_id, 2) AS type
                FROM post
                INNER JOIN
                    empresa e ON
                        e.id = post.cd_empresa
                WHERE
                    post.cd_empresa IS NOT NULL AND
                    (e.id_tipo_empresa = 3 AND post.cd_empresa = ?) 
                    OR e.id_tipo_empresa = 1
                ORDER BY id DESC

                LIMIT ?
                OFFSET ?
            ;",
            $args
        );


        $argsCount = [ $gym ];

        $count = DB::select("
            select
            (
                (
                    SELECT
                        count(1) AS countPosts
                    FROM post
                        INNER JOIN pessoas p ON
                            p.cd_pessoa = post.cd_pessoa
                        INNER JOIN registros r ON
                            r.cd_registro = p.cd_registro
                    WHERE
                        post.cd_pessoa IS NOT NULL AND
                        p.cd_empresa = ?
                ) +
                (
                    SELECT
                        count(1) AS countPosts
                    FROM post
                        INNER JOIN product p ON
                            p.id = post.cd_produto
                        INNER JOIN empresa e ON
                            e.id = p.id_empresa
                    WHERE
                        post.cd_produto IS NOT NULL

                ) +
                (

                    SELECT
                        count(1) AS countPosts
                    FROM post
                    INNER JOIN
                        empresa e ON
                            e.id = post.cd_empresa
                    WHERE
                        post.cd_empresa IS NOT NULL AND
                        e.id_tipo_empresa = 2
                )
            ) AS count
        ;",
            $argsCount
        );

        $total = 0;
        $totalPages = 0;
        if ($count && count($count) > 0) {
            $total = $count[0]->count ?? 0;
            $totalPages = ceil($total / $pageSize);
        }

        return Response([
            'posts' => $posts,
            'pageCount' => $totalPages,
            'count' => $total
        ]);
    }
}
