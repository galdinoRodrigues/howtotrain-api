<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller {
	public function home() {
		return view('home');
	}
	public function docs() {
		return view('welcome');
	}
	public function terms() {
		return view('terms');
	}
}
