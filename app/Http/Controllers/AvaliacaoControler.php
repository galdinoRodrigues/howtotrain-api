<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Validator;
use App\Login;
use App\Registro;
use App\Professor;
use App\Pessoa;
use DB;
use Auth;

class AvaliacaoControler extends Controller
{
    public function create(){
        $data = Input::all();
        $rules = [
            'idade' => 'required|integer',
            'masculino' => 'required|integer',

            'peso' => 'required|numeric',
            'gordura' => 'required|numeric',
            'massa_magra' => 'required|numeric',
            'massa_gorda' => 'required|numeric',

            'percentual_medio_minimo' => 'required|integer',
            'percentual_medio_maximo' => 'required|integer',

            'aluno_id' => 'required|integer',

            'dobras_cultaneas' => 'required|array',
            'perimetros' => 'array'
        ];
        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            return Response([
                'msg'=> $validation->getMessageBag()->first()
            ], 400);
        }

        $jwtLogin = Auth::user();
        $login = Login::
            with(
                'registro.pessoa.professor',
                'registro.pessoa.aluno'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa || !$login->registro->pessoa->professor) {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }

        $cd_professor = $login->registro->pessoa->professor->cd_professor;
        // date_default_timezone_set('America/Sao_Paulo');
        // date('Y-m-d H:i:s')

        $h = "3"; //HORAS DO FUSO ((BRASÍLIA = -3) COLOCA-SE SEM O SINAL -).
        $hm = $h * 60;
        $ms = $hm * 60;

        $PhysicalId = 
                    DB::table('avaliacao')
                    ->insertGetId([
                        'idade' => $data['idade'],
                        'masculino' => $data['masculino'],
                        'peso' => $data['peso'],
                        'gordura' => $data['gordura'],
                        'altura' => $data['altura'],
                        'massa_magra' => $data['massa_magra'],
                        'massa_gorda' => $data['massa_gorda'],
                        'percentual_medio_minimo' => $data['percentual_medio_minimo'],
                        'percentual_medio_maximo' => $data['percentual_medio_maximo'],
                        'aluno_id' => $data['aluno_id'],
                        'professor_id' => $cd_professor,
                        'created_at' => date('Y-m-d H:i:s', time()-($ms))
                        ]);

        foreach($data['dobras_cultaneas'] AS $dobras) {
            DB::table('dobras_cultaneas')
            ->insert([
                'description' => $dobras['description'],
                'value' => $dobras['value'],
                'id_avaliacao' => $PhysicalId
            ]);
        }

        if(isset($data['perimetros']) && !empty($data['perimetros'])){
            foreach($data['perimetros'] AS $perimetros) {
                DB::table('perimetros')
                ->insert([
                    'description' => $perimetros['description'],
                    'valueA' => !isset($perimetros['value']) ? 0 : $perimetros['value'],
                    'valueB' => !isset($perimetros['valueB']) ? 0 : $perimetros['valueB'],
                    'id_avaliacao' => $PhysicalId
                ]);
            }
        }

        if(!isset($PhysicalId)){
            return Response([ 'msg' => 'Algum erro aconteceu ao gravar a avaliação' ], 500);
        }
        return Response(
        [ 
            'physical_assessment_id' => $PhysicalId ,
            'msg' =>'SUCCESS'
        ], 200);
    }

    public function detail($id){
        if (!$id) {
            return Response([ 'msg' => 'Informe o ID da avaliação!' ], 404);
        }

        $jwtLogin = Auth::user();
        $login = Login::
            with(
                'registro.pessoa.professor',
                'registro.pessoa.aluno'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa || !$login->registro->pessoa->professor && 
        !$login->registro->pessoa->aluno) {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }

        $result = DB::select(
                "SELECT idade, 
                        masculino, 
                        peso, 
                        altura,
                        gordura, 
                        massa_magra, 
                        massa_gorda,
                        percentual_medio_minimo,
                        percentual_medio_maximo,
                        r2.ds_nome AS teacher_name,
                        created_at
                FROM avaliacao
                INNER JOIN professores p ON p.cd_professor = avaliacao.professor_id 
                INNER JOIN pessoas p2 ON p2.cd_pessoa  = p.cd_pessoa 
                INNER JOIN registros r2 ON r2.cd_registro = p2.cd_registro 
                where id = ?
            ",
            [$id]);

        if (count($result) <= 0) {
            return Response([
                'msg' => 'Informações não encontradas'
            ], 204);
        }

        $dobras = DB::select(
            "SELECT * 
            FROM dobras_cultaneas 
            WHERE id_avaliacao = ?
        ",
        [$id]); 

        $perimetros = DB::select(
            "SELECT * 
            FROM perimetros 
            WHERE id_avaliacao = ?
        ",
        [$id]); 

        $finalResult['created_at'] = date_create($result[0]->created_at)->format('d/m/y');
        $finalResult['idade'] =$result[0]->idade;
        $finalResult['altura'] =$result[0]->altura;
        $finalResult['masculino'] =$result[0]->masculino;
        $finalResult['peso'] =$result[0]->peso;
        $finalResult['gordura'] =$result[0]->gordura;
        $finalResult['massa_magra'] =$result[0]->massa_magra;
        $finalResult['massa_gorda'] =$result[0]->massa_gorda;
        $finalResult['percentual_medio_minimo'] =$result[0]->percentual_medio_minimo;
        $finalResult['percentual_medio_maximo'] =$result[0]->percentual_medio_maximo;
        $finalResult['professor_name'] =$result[0]->teacher_name;
        $finalResult['dobras_cultaneas'] = $dobras;
        $finalResult['perimetros'] = $perimetros;

        if (count($finalResult) <= 0) {
            return Response([
                'msg' => 'Usuário sem avaliações'
            ], 204);
        }
        else{
            return Response(
                $finalResult
            );
        }
    }


    public function list($aluno_id){
        if (!$aluno_id) {
            return Response([ 'msg' => 'Informe o ID do aluno!' ], 404);
        }

        $jwtLogin = Auth::user();
        $login = Login::
            with(
                'registro.pessoa.professor',
                'registro.pessoa.aluno'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa || !$login->registro->pessoa->professor && 
        !$login->registro->pessoa->aluno) {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }

        $result = DB::select(
                "SELECT a.id, 
                    r.ds_nome as professor_name,
                    a.peso,
                    a.massa_magra,
                    a.massa_gorda,
                    a.created_at
                FROM avaliacao a
                INNER JOIN professores p ON p.cd_professor = a.professor_id
                INNER JOIN pessoas p2 ON p2.cd_pessoa = p.cd_pessoa 
                INNER JOIN registros r ON r.cd_registro = p2.cd_registro 
                WHERE a.aluno_id = ?
                ORDER BY a.created_at DESC
            ",
            [$aluno_id]);
        if (count($result) <= 0) {
            return Response([
                'msg' => 'Usuário sem avaliações',
                'evaluations' => []
            ], 200);
        }
        else{
            return Response([
                'evaluations' => $result
            ]);
        }
    }
}
