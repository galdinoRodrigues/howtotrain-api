<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Validator;
use App\Registro;
use App\Login;
use App\Aluno;
use App\Pessoa;
use DB;
use Auth;

class AlunoController extends Controller {
    public function completeData($id)
    {
        $data = Input::all();
        $rules = [
            'sexo' => 'required|string',
            'dsNome' => 'required|string',
            'nrPeso' => 'required|numeric',
            'cmAltura' => 'required|numeric',
            'sdtNascimento' => 'required|string',
            'indCondicaoFisica' => 'required|numeric',
            'indTipoTreino' => 'required|numeric',
            'dsCidade' => 'required|string',
            'dsUf' => 'required|string',
            'dsPais' => 'string'
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails())
        {
            return Response(
                [ 'msg' => $validation->getMessageBag()->first() ],
                400
            );
        }

        try
        {
            Registro::
                where('cd_registro', $id)
                ->update([
                    'ds_nome' => $data['dsNome'],
                    'dt_alteracao' => date('Y-m-d H:i:s')
                ]);

            $fgPessoa = DB::
                table('pessoas')
                ->where('cd_registro', $id)
                ->first();

            if ($fgPessoa)
            {
                Pessoa::
                    where('cd_registro', $id)
                    ->update([
                        'dt_nascimento' => $data['sdtNascimento'],
                        'ind_sexo' => ($data['sexo'] === 'true') ? 1 : 0,
                        'fg_professor' => 0
                    ]);

                Aluno::
                    where('cd_pessoa', $fgPessoa->cd_pessoa)
                    ->update([
                        'cd_aluno_externo' => 0,
                        'nr_peso' => $data['nrPeso'],
                        'cm_altura' => $data['cmAltura'],
                        'ind_condicao_fisica' => $data['indCondicaoFisica'],
                        'ind_tipo_treino' => $data['indTipoTreino'],
                        'ind_nivel_experiencia' => 0
                    ]);

                if (isset($data['dsPais']))
                {
                    $pais = DB::
                        table('pais')
                        ->where('ds_pais', $data['dsPais'])
                        ->value('cd_pais');

                    if (count($pais) == NULL)
                    {
                        $pais = DB::
                            table('pais')
                            ->insertGetId(['ds_pais' => $data['dsPais']]);
                    }
                }
                else
                {
                    $pais = DB::
                        table('pais')
                        ->where('ds_pais','Brasil')
                        ->value('cd_pais');
                }

                $estado = DB::
                    table('estados')
                    ->where('ds_estado', $data['dsUf'])
                    ->where('cd_pais', $pais)
                    ->value('cd_estado');

                if (count($estado) == NULL)
                {
                    $estado = DB::
                        table('estados')
                        ->insertGetId([
                            'cd_pais' => $pais,
                            'ds_estado' => $data['dsUf']
                        ]);
                }

                $cidade = DB::
                    table('cidades')
                    ->where('ds_cidade', $data['dsCidade'])
                    ->where('cd_estado', $estado)
                    ->value('cd_cidade');

                if (count($cidade) == NULL)
                    $cidade = DB::table('cidades')
                        ->insertGetId([
                            'cd_estado' => $estado,
                            'ds_cidade' => $data['dsCidade']
                        ]);

                $cd_endereco = DB::
                    table('registros')
                    ->where('cd_registro', $id)
                    ->value('cd_endereco');

                DB::
                    table('enderecos')
                    ->where('cd_endereco', $cd_endereco)
                    ->update([
                        'cd_cidade' => $cidade,
                        'ds_ut' => $data['dsUf'],
                        'ds_cidade' => $data['dsCidade'],
                        'ds_bairro' => '',
                        'ds_endereco' => '',
                    ]);
            }
            else
            {
                $idPessoa = Pessoa::
                    insertGetId([
                        'cd_registro' => $id,
                        'dt_nascimento' => $data['sdtNascimento'],
                        'ind_sexo' => ($data['sexo']=='true') ? 1 : 0,
                        'fg_professor' => 0
                    ]);

                Aluno::
                    insert([
                        'cd_pessoa' => $idPessoa,
                        'cd_aluno_externo' => 0,
                        'nr_peso' => $data['nrPeso'],
                        'cm_altura' => $data['cmAltura'],
                        'ind_condicao_fisica' => $data['indCondicaoFisica'],
                        'ind_tipo_treino' => $data['indTipoTreino'],
                        'ind_nivel_experiencia' => 0
                    ]);

                if (isset($data['dsPais']))
                {
                    $pais = DB::
                        table('pais')
                        ->where('ds_pais', $data['dsPais'])
                        ->value('cd_pais');

                    if (count($pais) == NULL)
                        $pais = DB::
                            table('pais')
                            ->insertGetId(['ds_pais' => $data['dsPais']]);
                }
                else
                {
                    $pais = DB::
                        table('pais')
                        ->where('ds_pais','Brasil')
                        ->value('cd_pais');
                }

                $estado = DB::
                    table('estados')
                    ->where('ds_estado', $data['dsUf'])
                    ->where('cd_pais', $pais)
                    ->value('cd_estado');

                if (count($estado) == NULL)
                {
                    $estado = DB::
                        table('estados')
                        ->insertGetId([
                            'cd_pais' => $pais,
                            'ds_estado' => $data['dsUf']
                        ]);
                }
                $cidade = DB::
                    table('cidades')
                    ->where('ds_cidade', $data['dsCidade'])
                    ->where('cd_estado', $estado)
                    ->value('cd_cidade');

                if (count($cidade) == NULL)
                {
                    $cidade = DB::
                        table('cidades')
                        ->insertGetId([
                            'cd_estado' => $estado,
                            'ds_cidade' => $data['dsCidade']
                        ]);
                }
                $endereco = DB::
                    table('enderecos')
                    ->insertGetId([
                        'cd_cidade' => $cidade,
                        'ds_ut' => $data['dsUf'],
                        'ds_cidade' => $data['dsCidade'],
                        'ds_bairro' =>'',
                        'ds_endereco' =>'',
                    ]);

                DB::
                    table('registros')
                    ->where('cd_registro', $id)
                    ->update(['cd_endereco' => $endereco]);
            }
        }
        catch(\Excetion $e)
        {
            return Response([
                'msg' =>'Erro ao cadastrar novos dados',
                'erro' => $e
            ], 400);
        }
        return Response([ 'msg' =>'SUCCESS' ], 200);
    }

    public function completeDataV2($cd_registro)
    {
        $data = Input::all();
        $rules = [
            'sexo' => 'required|string',
            'dsNome' => 'required|string',
            'nrPeso' => 'required|numeric',
            'dsAvatar' => 'string',
            'cmAltura' => 'required|numeric',
            'sdtNascimento' => 'required|string',
            'indCondicaoFisica' => 'required|numeric',
            'indTipoTreino' => 'required|numeric',
            'dsCidade' => 'required|string',
            'dsUf' => 'required|string',
            'dsPais' => 'string',
            'dsCodigoPais' => 'required|string|max:6',
            'cd_empresa' => 'integer|exists:empresa,id'
        ];

        $validation = Validator::make($data, $rules);
        if ($validation->fails())
        {
            return Response(
                [ 'msg' => $validation->getMessageBag()->first() ],
                400
            );
        }

        try
        {
            Registro::
                where('cd_registro', $cd_registro)
                ->update([
                    'ds_nome' => $data['dsNome'],
                    'ds_avatar' => isset($data['dsAvatar']) ? $data['dsAvatar'] : null,
                    'dt_alteracao' => date('Y-m-d H:i:s')
                ]);

            $fgPessoa = DB::
                table('pessoas')
                ->where('cd_registro', $cd_registro)
                ->first();
            
            $studentId = -1;

            if ($fgPessoa)
            {
                $studentId = DB::
                select("
                    SELECT a.cd_aluno 
                    FROM registros r
                    INNER JOIN pessoas p ON p.cd_registro = r.cd_registro 
                    INNER JOIN alunos a ON p.cd_pessoa = a.cd_pessoa
                    WHERE r.cd_registro = ?
                    ",
                    [ $cd_registro ]
                )[0]->cd_aluno;

                Pessoa::
                    where('cd_registro', $cd_registro)
                    ->update([
                        'dt_nascimento' => $data['sdtNascimento'],
                        'ind_sexo' => ($data['sexo'] == 'true') ? 1 : 0,
                        'cd_empresa' => isset($data['cd_empresa']) ? $data['cd_empresa'] : null,
                        'fg_professor' => 0
                    ]);

                Aluno::
                    where('cd_pessoa', $fgPessoa->cd_pessoa)
                    ->update([
                        'cd_aluno_externo' => 0,
                        'nr_peso' => $data['nrPeso'],
                        'cm_altura' => $data['cmAltura'],
                        'ind_condicao_fisica' => $data['indCondicaoFisica'],
                        'ind_tipo_treino' => $data['indTipoTreino'],
                        'ind_nivel_experiencia' => 0
                    ]);

                $pais = DB::
                    table('pais')
                    ->where('ds_codigo', $data['dsCodigoPais'])
                    ->value('cd_pais');

                if (count($pais) == NULL)
                {
                    $pais = DB::
                        table('pais')->
                        insertGetId([
                            'ds_codigo' => $data['dsCodigoPais'],
                            'ds_pais' => isset($data['dsPais']) ? $data['dsPais'] : $data['dsCodigoPais']
                        ]);
                }

                $estado = DB::
                    table('estados')
                    ->where('ds_estado', $data['dsUf'])
                    ->where('cd_pais', $pais)
                    ->value('cd_estado');

                if (count($estado) == NULL)
                {
                    $estado = DB::
                        table('estados')
                        ->insertGetId([
                            'cd_pais' => $pais,
                            'ds_estado' => $data['dsUf']
                        ]);
                }

                $cidade = DB::
                    table('cidades')
                    ->where('ds_cidade', $data['dsCidade'])
                    ->where('cd_estado', $estado)
                    ->value('cd_cidade');

                if (count($cidade) == NULL)
                {
                    $cidade = DB::
                        table('cidades')
                        ->insertGetId([
                            'cd_estado' => $estado,
                            'ds_cidade' => $data['dsCidade']
                        ]);
                }

                $cd_endereco = DB::
                    table('registros')
                    ->where('cd_registro', $cd_registro)
                    ->value('cd_endereco');

                DB::
                    table('enderecos')
                    ->where('cd_endereco', $cd_endereco)
                    ->update([
                        'cd_cidade' => $cidade,
                        'ds_ut' => $data['dsUf'],
                        'ds_cidade' => $data['dsCidade'],
                        'ds_bairro' =>'',
                        'ds_endereco' =>'',
                    ]);
            }
            else
            {
                $idPessoa = Pessoa::
                    insertGetId([
                        'cd_registro' => $cd_registro,
                        'dt_nascimento' => $data['sdtNascimento'],
                        'ind_sexo' => ($data['sexo']=='true') ? 1 : 0,
                        'cd_empresa' => isset($data['cd_empresa']) ? $data['cd_empresa'] : null,
                        'fg_professor' => 0
                    ]);

                $studentId = Aluno::
                    insertGetId([
                        'cd_pessoa' => $idPessoa,
                        'cd_aluno_externo' => 0,
                        'nr_peso' => $data['nrPeso'],
                        'cm_altura' => $data['cmAltura'],
                        'ind_condicao_fisica' => $data['indCondicaoFisica'],
                        'ind_tipo_treino' => $data['indTipoTreino'],
                        'ind_nivel_experiencia' => 0
                    ]);

                $pais = DB::
                    table('pais')
                    ->where('ds_codigo', $data['dsCodigoPais'])
                    ->value('cd_pais');

                if (count($pais) == NULL)
                {
                    $pais = DB::
                        table('pais')
                        ->insertGetId([
                            'ds_codigo' => $data['dsCodigoPais'],
                            'ds_pais' => isset($data['dsPais']) ? $data['dsPais'] : $data['dsCodigoPais']
                        ]);
                }

                $estado = DB::
                    table('estados')
                    ->where('ds_estado', $data['dsUf'])
                    ->where('cd_pais', $pais)
                    ->value('cd_estado');

                if (count($estado) == NULL)
                {
                    $estado = DB::
                        table('estados')
                        ->insertGetId([
                            'cd_pais' => $pais,
                            'ds_estado' => $data['dsUf']
                        ]);
                }

                $cidade = DB::
                    table('cidades')
                    ->where('ds_cidade', $data['dsCidade'])
                    ->where('cd_estado', $estado)
                    ->value('cd_cidade');

                if (count($cidade) == NULL)
                {
                    $cidade = DB::
                        table('cidades')
                        ->insertGetId([
                            'cd_estado' => $estado,
                            'ds_cidade' => $data['dsCidade']
                        ]);
                }

                $endereco = DB::
                    table('enderecos')
                    ->insertGetId([
                        'cd_cidade' => $cidade,
                        'ds_ut' => $data['dsUf'],
                        'ds_cidade' => $data['dsCidade'],
                        'ds_bairro' => '',
                        'ds_endereco' => '',
                    ]);

                DB::
                    table('registros')
                    ->where('cd_registro', $cd_registro)
                    ->update(['cd_endereco' => $endereco]);
            }
        }
        catch(\Excetion $e)
        {
            return Response(
                [
                    'msg' =>'Erro ao cadastrar novos dados',
                    'erro' => $e
                ],
                400
            );
        }

        return Response(
            [ 
                'msg' =>'SUCCESS',
                'cd_aluno' => $studentId 
            ], 200);
    }

    public function getAluno($id)
    {
        try
        {
            $aluno = Registro::
                where('registros.cd_registro', $id)
                ->join('pessoas','pessoas.cd_registro','=','registros.cd_registro')
                ->join('alunos','alunos.cd_pessoa','=','pessoas.cd_pessoa')
                ->leftJoin('enderecos','enderecos.cd_endereco','=','registros.cd_endereco')
                ->leftJoin('cidades', 'cidades.cd_cidade', '=' ,'enderecos.cd_cidade')
                ->leftJoin('estados', 'estados.cd_estado', '=' ,'cidades.cd_estado')
                ->leftJoin('pais','estados.cd_pais','=','pais.cd_pais')
                ->select(
                    'registros.cd_registro as cdRegistroExterno',
                    'registros.ind_registro as indTipoRegistro',
                    'pessoas.ind_sexo as sexo',
                    'registros.ds_nome as dsNome',
                    'alunos.nr_peso as nrPeso',
                    'alunos.cm_altura as cmAltura',
                    'pessoas.dt_nascimento as sdtNascimento',
                    'alunos.ind_condicao_fisica as indCondicaoFisica',
                    'alunos.ind_tipo_treino as indTipoTreino',
                    'pessoas.ind_sexo as masculino',
                    'enderecos.ds_cidade as dsCidade',
                    'enderecos.ds_ut as dsUf',
                    'pais.ds_pais as dsPais',
                    'pais.ds_codigo as dsCodigoPais'
                )
                ->first();

            if (count($aluno) !== 0)
            {
                $aluno['sexo'] = ($aluno['sexo'] == 1) ? true : false;
                $aluno['masculino'] = ($aluno['masculino'] == 1) ? true : false;
                $aluno['endereco'] = [
                    'dsCidade' => $aluno['dsCidade'],
                    'dsUf' => $aluno['dsUf'],
                    'dsPais' => $aluno['dsPais'],
                    'dsCodigoPais' => $aluno['dsCodigoPais']
                ];
                return Response($aluno, 200);
            }
            return Response([ 'msg' =>'Aluno não encontrado' ], 404);
        }
        catch(\Exception $e)
        {
            return Response(
                [
                    'msg' =>'Erro ao consultar aluno',
                    'error' => $e
                ],
                400
            );
        }
    }

    public function getAlunoV2($cd_aluno)
    {
        try {

            $jwtLogin = Auth::user();
            $login = Login::
                with(
                    'registro',
                    'registro.pessoa'
                )
                ->where('cd_login', $jwtLogin->cd_login)
                ->first();

            $aluno = Aluno::
                where('alunos.cd_aluno', $cd_aluno)
                ->join('pessoas','alunos.cd_pessoa','=','pessoas.cd_pessoa')
                ->join('registros','pessoas.cd_registro','=','registros.cd_registro')
                ->leftJoin('enderecos','enderecos.cd_endereco','=','registros.cd_endereco')
                ->leftJoin('cidades', 'cidades.cd_cidade', '=' ,'enderecos.cd_cidade')
                ->leftJoin('estados', 'estados.cd_estado', '=' ,'cidades.cd_estado')
                ->leftJoin('pais','estados.cd_pais','=','pais.cd_pais')
                ->leftJoin('empresa','empresa.id','=','pessoas.cd_empresa')
                ->leftJoin('amizades', function($join) use ($login)
                {
                    return $join
                        ->on('amizades.cd_from', '=', 'pessoas.cd_pessoa')
                        ->on('amizades.cd_to', '=', \DB::raw($login->registro->pessoa->cd_pessoa))
                        ->orOn(function($join) use($login)
                        {
                            return $join
                                ->on('amizades.cd_from', '=', \DB::raw($login->registro->pessoa->cd_pessoa))
                                ->on('amizades.cd_to', '=', 'pessoas.cd_pessoa');
                        });
                })
                ->select(
                    'registros.cd_registro as cdRegistroExterno',
                    'registros.ind_registro as indTipoRegistro',
                    'amizades.cd_amizade',
                    \DB::raw('IF(amizades.cd_amizade IS NULL, 0, IF(amizades.fg_aceito = 1, 2, 1)) AS friendship_status'),
                    'registros.ds_avatar as dsAvatar',
                    'pessoas.ind_sexo as sexo',
                    'registros.ds_nome as dsNome',
                    'alunos.nr_peso as nrPeso',
                    'alunos.cm_altura as cmAltura',
                    'pessoas.dt_nascimento as sdtNascimento',
                    'alunos.ind_condicao_fisica as indCondicaoFisica',
                    'alunos.ind_tipo_treino as indTipoTreino',
                    'pessoas.ind_sexo as masculino',
                    'enderecos.ds_cidade as dsCidade',
                    'enderecos.ds_ut as dsUf',
                    'pais.ds_pais as dsPais',
                    'pais.ds_codigo as dsCodigoPais',
                    'empresa.id AS cdEmpresa',
                    'empresa.nome as dsNomeEmpresa'
                )
                ->first();

            if (count($aluno) !== 0)
            {
                $aluno['sexo'] = ($aluno['sexo'] == 1) ? true : false;
                $aluno['masculino'] = ($aluno['masculino'] == 1) ? true : false;
                $aluno['endereco'] = [
                    'dsCidade' => $aluno['dsCidade'],
                    'dsUf' => $aluno['dsUf'],
                    'dsPais' => $aluno['dsPais'],
                    'dsCodigoPais' => $aluno['dsCodigoPais']
                ];

                if ($aluno['cdEmpresa'])
                {
                    $aluno['gym'] = [
                        'id' => $aluno['cdEmpresa'],
                        'name' => $aluno['dsNomeEmpresa']
                    ];
                }

                // pegar os planos de treino do aluno
                $args = [
                    $cd_aluno
                ];

                $plans = DB::
                select("
                    SELECT 
                        p.cd_plano,
                        p.ind_nivel_treino,
                        p.ind_tipo_treino,
                        ap.dt_expiracao,
                        p.ds_nome as name, 
                        r.cd_registro as cd_registro_professor
                    FROM planos p
                    INNER JOIN aluno_plano ap ON ap.cd_plano  =  p.cd_plano 
                    INNER JOIN alunos a ON a.cd_aluno = ap.cd_aluno 
                    INNER JOIN professores pr ON pr.cd_professor = p.cd_professor 
                    INNER JOIN pessoas pe ON pe.cd_pessoa = pr.cd_pessoa 
                    INNER JOIN registros r ON r.cd_registro = pe.cd_registro 
                    WHERE a.cd_aluno = ?
                    ORDER BY p.cd_plano DESC
                ;",
                $args
                );
                $aluno["plans"] = $plans;
                
                $lastEvaluation = DB::select(
                    "SELECT created_at
                    FROM avaliacao
                    WHERE aluno_id = ?
                    ORDER BY created_at DESC
                    lIMIT 1
                ",
                [$cd_aluno]);
                
                if(isset($lastEvaluation) && isset($lastEvaluation[0]->created_at)){
                    $aluno["dateLastAssessment"] = date_create($lastEvaluation[0]->created_at)->format('d/m/y');
                }

                unset($aluno['cdEmpresa']);
                unset($aluno['dsNomeEmpresa']);


                return Response($aluno, 200);
            }

            return Response(['msg' =>'Aluno não encontrado'], 404);
        }
        catch(\Exception $e)
        {
            return Response(
                [
                    'msg' =>'Erro ao consultar aluno',
                    'error' => $e
                ],
                400
            );
        }
    }

    function linkingGymWithStudent($cd_aluno, $gymID){
        DB::
        table('pessoas')
        ->where('alunos.cd_aluno', $cd_aluno)
        ->join('alunos','alunos.cd_pessoa','=','pessoas.cd_pessoa')
        ->update(['cd_empresa' => $gymID]);

    }

    public function validarInvite(){
        $data = Input::all();
        $rules = [
            'uuid' => 'required|string',
            'cd_aluno' => 'required|integer'
        ];
        
        $args = [ 
            $data['uuid'],
            $data['cd_aluno']
        ];

        $gymID = DB::select(
            "
            SELECT id_empresa FROM aluno_empresa_invite
            WHERE uuid = ? 
            AND id_aluno IS NULL 
            AND id_professor IS NULL
            ",
            $args
        );
    
        if (count($gymID) != 0) {   
            DB::
            table('aluno_empresa_invite')
            ->where('uuid', $data['uuid'])
            ->where('id_aluno', null)
            ->where('id_professor', null)
            ->update(['id_aluno' =>  $data['cd_aluno']]);

            $this->linkingGymWithStudent($data['cd_aluno'], $gymID[0]->id_empresa);
            return Response([ 'gym_id' => $gymID[0]->id_empresa ] , 200);
        }

        $gymID = DB::select(
            "
            SELECT id as id_empresa FROM empresa
            WHERE uuid = ?
            ",
            $args
        );

        if (count($gymID) != 0) {
            DB::
            table('aluno_empresa_invite')
            ->insert(
                [
                    'uuid' =>  $data['uuid'],
                    'id_empresa' =>  $gymID[0]->id_empresa,
                    'id_aluno' => $data['cd_aluno']
                ]);

            $this->linkingGymWithStudent($data['cd_aluno'], $gymID[0]->id_empresa);
            return Response([ 'gym_id' => $gymID[0]->id_empresa ] , 200);
        }

        return Response([ 'msg' => 'Código inválido, Academia não encontrada' ], 404);
    }

    public function listAllStudents(Request $request)
    {
        $data = Input::all();
        $rules = [
            'pageIndex' => 'integer',
            'pageSize' => 'integer',
            'query' => 'string|max:24'
        ];
        $validation = Validator::make($data, $rules);

        // Busca por ID e NOME
        // Devolver imagem de perfil, nome, endereço e situação de amizade comigo.

        $jwtLogin = Auth::user();
        $login = Login::
            with(
                'registro',
                'registro.pessoa'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa)
        {
            return Response(
                [ 'msg' => 'Não autorizado' ],
                401
            );
        }

        $gym = $login->registro->pessoa->cd_empresa;
        if (!$gym)
        {
            return Response(
                [ 'msg' => 'Por favor se víncule a uma academia.' ],
                404
            );
        }

        $pageIndex = isset($data['pageIndex']) ? $data['pageIndex'] : 1;
        $pageSize = isset($data['pageSize']) ? $data['pageSize'] : 10;
        $query = isset($data['query']) ? $data['query'] : null;

        // All attributes come as string when passed as queryParams
        $pageIndex = (int) $pageIndex;
        $pageSize = (int) $pageSize;

        $from = ($pageIndex - 1) * $pageSize;

        $args = [
            $login->registro->pessoa->cd_pessoa,
            $login->registro->pessoa->cd_pessoa,
            $gym,
            $pageSize,
            $from
        ];
        $argsCount = [
            $login->registro->pessoa->cd_pessoa,
            $login->registro->pessoa->cd_pessoa,
            $gym
        ];

        $search = '';
        if ($query)
        {
            $search = ' AND (r.ds_nome LIKE ? OR a.cd_aluno LIKE ?) ';
            $args = [
                $login->registro->pessoa->cd_pessoa,
                $login->registro->pessoa->cd_pessoa,
                $gym,
                '%'.$query.'%',
                '%'.$query.'%',
                $pageSize,
                $from
            ];
            $argsCount = [
                $login->registro->pessoa->cd_pessoa,
                $login->registro->pessoa->cd_pessoa,
                $gym,
                '%'.$query.'%',
                '%'.$query.'%'
            ];
        }

        $alunos = DB::select("
            SELECT
                a.cd_aluno,
                cid.ds_cidade,
                est.ds_estado,
                pai.ds_pais,
                pai.ds_codigo,
                r.ds_nome,
                r.ds_avatar,
                r.dt_inclusao,
                pe.ind_sexo AS sexo,
                IF(am.cd_amizade IS NULL, 0, IF(am.fg_aceito = 1, 2, 1)) AS friendship_status
            FROM
                alunos AS a
            INNER JOIN
                pessoas AS pe
                    ON pe.cd_pessoa = a.cd_pessoa
            INNER JOIN
                registros AS r
                    ON r.cd_registro = pe.cd_registro
            LEFT JOIN
                enderecos AS e
                    ON e.cd_endereco = r.cd_endereco
            LEFT JOIN
                cidades AS cid
                    ON cid.cd_cidade = e.cd_cidade
            lEFT JOIN
                estados AS est
                    ON est.cd_estado = cid.cd_estado
            LEFT JOIN
                pais AS pai
                    ON pai.cd_pais = est.cd_pais
            LEFT JOIN
                amizades AS am ON
                    (am.cd_from = pe.cd_pessoa AND am.cd_to = ?) OR
                    (am.cd_from = ? AND am.cd_to = pe.cd_pessoa)
            WHERE
                IFNULL(r.fg_excluido, 0) <> 1
                AND IFNULL(pe.cd_empresa, 0) = ?
            "
            . $search .
            "
                ORDER BY r.ds_nome
                LIMIT ? OFFSET ?
            ;",
            $args
        );

        $count = DB::select("
            SELECT
                COUNT(1) AS count
            FROM
                alunos AS a
            INNER JOIN
                pessoas AS pe
                    ON pe.cd_pessoa = a.cd_pessoa
            INNER JOIN
                registros AS r
                    ON r.cd_registro = pe.cd_registro
            LEFT JOIN
                enderecos AS e
                    ON e.cd_endereco = r.cd_endereco
            LEFT JOIN
                cidades AS cid
                    ON cid.cd_cidade = e.cd_cidade
            lEFT JOIN
                estados AS est
                    ON est.cd_estado = cid.cd_estado
            LEFT JOIN
                pais AS pai
                    ON pai.cd_pais = est.cd_pais
            LEFT JOIN
                amizades AS am ON
                    (am.cd_from = pe.cd_pessoa AND am.cd_to = ?) OR
                    (am.cd_from = ? AND am.cd_to = pe.cd_pessoa)
            WHERE
                IFNULL(r.fg_excluido, 0) <> 1
                AND IFNULL(pe.cd_empresa, 0) = ?
            "
            . $search .
            ";",
            $argsCount
        );


        foreach($alunos as $a)
        {
            $a->endereco = [
                'dsCidade' => $a->ds_cidade ?? '',
                'dsUf' => $a->ds_estado ?? '',
                'dsPais' => $a->ds_pais ?? '',
                'dsCodigoPais' => $a->ds_codigo ?? ''
            ];
            unset($a->ds_cidade);
            unset($a->ds_estado);
            unset($a->ds_pais);
            unset($a->ds_codigo);

            $a->sexo = $a->sexo == 1 ? true : false;
        }

        $total = 0;
        $totalPages = 0;
        if ($count && count($count) > 0)
        {
            $total = $count[0]->count ?? 0;
            $totalPages = ceil($total / $pageSize);
        }

        return Response([
            'alunos' => $alunos,
            'pageCount' => $totalPages,
            'count' => $total
        ]);
    }
}
