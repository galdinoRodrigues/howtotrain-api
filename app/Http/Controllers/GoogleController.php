<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Validator,Log;

class GoogleController extends Controller {
    public function playlist() {
        $data = Input::all();
        $rules = [
            'channels' => 'required'
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return Response([
                'msg' => $validation->getMessageBag()->first()
            ], 400);
        }

        $channels = json_decode($data['channels']);

        $channelsResponse = [];
        foreach ($channels as $c) {
            $response = new \stdclass();
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => 'https://www.googleapis.com/youtube/v3/playlistItems?playlistId='.$c->channelId.'&part=snippet,contentDetails&key=AIzaSyDAu6QAGPYBntNmRpyIoQQEPi_jaHZSLME&maxResults=50',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => [
                    'Content-Type: application/json'
                ],
            ]);

            $responseCurl = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);

            if ($err) {
                return [
                    'status' => true,
                    'response' => $err
                ];
            }

            $responseCurl = json_decode($responseCurl);
            $videos = [];
            if (isset($responseCurl->items)) {
                foreach ($responseCurl->items as $i) {
                    $thumbnails = [];
                    if (!isset($i->snippet) || !isset($i->snippet->thumbnails)) {
                        continue;
                    }
                    foreach ($i->snippet->thumbnails as $key => $t) {
                        $thumbnails['url_'.$key] = $t->url;
                    }
                    $obj = [
                        'title' => $i->snippet->title,
                        'videoId' => $i->snippet->resourceId->videoId,
                        'thumbnails' => $thumbnails
                    ];

                    $videos[] = $obj;
                }
                $response->grupo_muscular = $c->grupo_muscular;
                $response->channelId = $c->channelId;
                $response->videos = $videos;

                $channelsResponse[] = $response;
            } else {
                $response->grupo_muscular = $c->grupo_muscular;
                $response->channelId = $c->channelId;
                $response->videos = NULL;
                $channelsResponse[] = $response;
            }
        }
        return Response([
            'channels'=> $channelsResponse
        ], 200);
    }
}
?>