<?php

namespace App\Http\Controllers;

use DB;


class GroupController extends Controller {
    public function listGroups() {

        try {
            $groups = DB::select("
                SELECT
                    cd_grupo,
                    ds_nome,
                    id_image,
                    id_image_info,
                    ds_url
                FROM
                    grupos
                ORDER BY
                    ds_nome
                ;"
            );

            return Response([ 'groups' => $groups ]);
        } catch(\Exception $e) {
            return Response([
                'msg' => 'Erro ao consultar os grupos',
                'error' => $e
            ], 400);
        }
    }
}
