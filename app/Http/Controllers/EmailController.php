<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Validator;

class EmailController extends Controller {
    public function enviarEmailContato() {
        $data = Input::all();
        $rules = [
            'email' => 'required|email',
            'message' => 'required'
        ];

        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            return Response([
                'msg' => $validation->getMessageBag()->first()
            ], 400);
        }

        try {
            $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
            $email = new \SendGrid\Mail\Mail();
            $email->setFrom($data['email'], 'Contato do Site');
            $email->setSubject('Quero o aplicativo em minha academia');
            $email->addTo('howtotrain.contato@gmail.com');
            $email->addContent('text/html', '<b>Enviado por:</b> '.$data['email'].'<br/><b>Mensagem:</b><br/><br/>'.$data['message']);
            $response = $sendgrid->send($email);
            return Response([ 'msg' => 'SUCCESS' ], 200);
        } catch (Exception $e) {
            return Response([
                'msg' => 'Problema ao enviar email',
                'error' => $e
            ], 200);
        }
    }
}
