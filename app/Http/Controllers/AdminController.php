<?php

namespace App\Http\Controllers;

use DB;
use Validator;

use App\Registro;
use App\Admin;
use App\Aluno;
use App\Professor;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller {
    public function showRegister() {
        return view('admin.auth.register');
    }

    public function createAdmin(Request $request) {
        $request->validate([
            'email'   => 'required|email',
            'name'    => 'required',
            'password' => 'required|min:4'
        ]);

        $admin = Admin::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);

        return redirect('login/admin');
    }

    public function loginScreen() {
        return view('admin.auth.login');
    }

    public function login(Request $request) {
        $request->validate([
            'email'   => 'required|email',
            'password' => 'required|min:4'
        ]);
        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];
        if (Auth::guard('admin')->attempt($data, true)) {
            return redirect()->intended(route('admin.home'));
        }
        return back()
            ->withInput($request->only('email', 'remember'))
            ->withErrors(['Usuário ou senha errados'], 'login');
    }

    public function logout() {
        Auth::guard('admin')->logout();
        return redirect(route('admin.login.screen'));
    }

    public function home() {
        return view('admin.home');
    }

    public function students(Request $request) {
        $page = $request->page ?? 1;
        $search = $request->search ?? null;
        $students = Aluno::
            with('pessoa', 'pessoa.registro', 'pessoa.registro.login')
            ->orderBy('cd_aluno', 'desc');
        // if ($search) {
        //     $students = $students
        //         ->where('registros.ds_nome', 'like' , '%'.$search.'%');
        // }
        $students = $students->paginate(12, ['*'], 'page', $page);
        return view('admin.students.list', [
            'students' => $students,
            'search' => $search
        ]);
    }

    public function admins(Request $request) {
        $page = $request->page ?? 1;
        $search = $request->search ?? null;
        $admins = Admin::orderBy('id', 'desc');

        $admins = $admins->paginate(12, ['*'], 'page', $page);
        return view('admin.admins.list', [
            'admins' => $admins,
            'search' => $search
        ]);
    }

    public function teachers(Request $request) {
        $page = $request->page ?? 1;
        $search = $request->search ?? null;
        $teachers = Professor::
            with('pessoa', 'pessoa.registro', 'pessoa.registro.login')
            ->orderBy('cd_professor', 'desc');
        $teachers = $teachers->paginate(12, ['*'], 'page', $page);
        return view('admin.teachers.list', [
            'teachers' => $teachers,
            'search' => $search
        ]);
    }

    public function gyms(Request $request) {
        $page = $request->page ?? 1;
        $search = $request->search ?? null;

        $gyms = [];

        return view('admin.gyms.list', [
            'gyms' => $gyms,
            'search' => $search
        ]);
    }

    public function createGymScreen(Request $request) {
        return view('admin.gyms.create');
    }

    public function createGym(Request $request) {
        return redirect(route('admin.gyms.list'));
    }
}
