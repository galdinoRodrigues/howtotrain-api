<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Registro;
use App\Login;
use JWTAuth;

class RegisterController extends Controller {
    public function createRegister() {
        $data = Input::all();
        $rules = [
            'email' => 'required|string',
            'senha' => 'required|string',
            'indTipoRegistro' => 'integer',
        ];
        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            return Response([
                'msg'=> $validation->getMessageBag()->first()
            ], 400);
        }

        $indTipoRegistro = isset($data['indTipoRegistro']) ? $data['indTipoRegistro'] : 1;
        if ($indTipoRegistro != 1 && $indTipoRegistro != 2) {
            $indTipoRegistro = 1;
        }

        //Verifica se o email informado já existe
        try {
            $fgEmail = Login::
                where('ds_email', $data['email'])
                ->first();
            $idRegistro = Registro::
                insertGetId([
                    'ind_registro' => $indTipoRegistro
                ]);

            if (isset($fgEmail) && count($fgEmail) !== 0) {
                return Response([
                    'msg'=>'O e-mail informado já existe!'
                ], 400);
            }

            $idLogin = Login::
                insertGetId([
                    'cd_registro'=> $idRegistro,
                    'ds_email'=> $data['email'],
                    'ds_senha'=> $data['senha']
                ]);

            $login = Login::
                where('cd_login', $idLogin)
                ->first();

            $token = JWTAuth::fromUser($login);

            return Response([
                'indTipoRegistro' => $indTipoRegistro,
                'cdRegistroExterno' => $idRegistro,
                'token' => $token
            ], 200);
        } catch(\Exception $e) {
            return Response([
                'msg'=>'Erro ao cadastrar usuário',
                'erro'=> $e->getMessage()
            ], 500);
        }
    }
}
