<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Validator;
use App\Login;
use App\Registro;
use App\Professor;
use App\Pessoa;
use DB;
use Auth;

class ProfessorController extends Controller
{
    public function completeDataV2($cd_registro)
    {
        $data = Input::all();
        $rules = [
            'sexo' => 'required|string',
            'dsNome' => 'required|string',
            'sdtNascimento' => 'required|string',
            'dsCidade' => 'required|string',
            'dsUf' => 'required|string',
            'dsPais' => 'string',
            'dsAvatar' => 'string',
            'dsCodigoPais' => 'required|string|max:6',
            'ds_cref' => 'required|string',
            'dt_cref' => 'required|string',
            'nr_valor_mensal' => 'required|numeric',
            'cd_empresa' => 'integer|exists:empresa,id'
        ];

        $validation = Validator::make($data, $rules);
        if ($validation->fails())
        {
            return Response(
                [ 'msg' => $validation->getMessageBag()->first() ],
                400
            );
        }
        else if (!$cd_registro)
        {
            return Response([ 'msg' => 'Id not informed.' ], 400);
        }

        try
        {
            Registro::
                where('cd_registro', $cd_registro)
                ->update([
                    'ds_nome' => $data['dsNome'],
                    'ds_avatar' => $data['dsAvatar'] ?? null,
                    'dt_alteracao' => date('Y-m-d H:i:s')
                ]);

            $fgPessoa = DB::
                table('pessoas')
                ->where('cd_registro', $cd_registro)
                ->first();

            if ($fgPessoa)
            {
                Pessoa::
                    where('cd_registro', $cd_registro)
                    ->update([
                        'dt_nascimento' => $data['sdtNascimento'],
                        'ind_sexo' => ($data['sexo'] == 'true') ? 1 : 0,
                        'fg_professor' => 1,
                        'cd_empresa' => $data['cd_empresa'] ?? null
                    ]);

                Professor::
                    where('cd_pessoa', $fgPessoa->cd_pessoa)
                    ->update([
                        'ds_cref' => $data['ds_cref'],
                        'dt_cref' => $data['dt_cref'],
                        'nr_valor_mensal' => $data['nr_valor_mensal']
                    ]);

                $pais = DB::
                    table('pais')
                    ->where('ds_codigo', $data['dsCodigoPais'])
                    ->value('cd_pais');

                if (count($pais) == NULL)
                {
                    $pais = DB::
                        table('pais')
                        ->insertGetId([
                            'ds_codigo' => $data['dsCodigoPais'],
                            'ds_pais' => isset($data['dsPais']) ? $data['dsPais'] : $data['dsCodigoPais']
                        ]);
                }

                $estado = DB::
                    table('estados')
                    ->where('ds_estado', $data['dsUf'])
                    ->where('cd_pais', $pais)
                    ->value('cd_estado');

                if (count($estado) == NULL)
                {
                    $estado = DB::
                        table('estados')
                        ->insertGetId([
                            'cd_pais' => $pais,
                            'ds_estado' => $data['dsUf']
                        ]);
                }

                $cidade = DB::
                    table('cidades')
                    ->where('ds_cidade', $data['dsCidade'])
                    ->where('cd_estado', $estado)
                    ->value('cd_cidade');

                if (count($cidade) == NULL)
                {
                    $cidade = DB::
                        table('cidades')
                        ->insertGetId([
                            'cd_estado' => $estado,
                            'ds_cidade' => $data['dsCidade']
                        ]);
                }

                $cd_endereco = DB::
                    table('registros')
                    ->where('cd_registro', $cd_registro)
                    ->value('cd_endereco');

                if ($cd_endereco)
                {
                    DB::
                        table('enderecos')
                        ->where('cd_endereco', $cd_endereco)
                        ->update([
                            'cd_cidade' => $cidade,
                            'ds_ut' => $data['dsUf'],
                            'ds_cidade' => $data['dsCidade'],
                            'ds_bairro' => '',
                            'ds_endereco' => '',
                        ]);
                }
                else
                {
                    $endereco = DB::
                        table('enderecos')
                        ->insertGetId([
                            'cd_cidade' => $cidade,
                            'ds_ut' => $data['dsUf'],
                            'ds_cidade' => $data['dsCidade'],
                            'ds_bairro' => '',
                            'ds_endereco' => '',
                        ]);

                    DB::
                        table('registros')
                        ->where('cd_registro', $cd_registro)
                        ->update(['cd_endereco' => $endereco]);
                }
            }
            else
            {
                $idPessoa = Pessoa::
                    insertGetId([
                        'cd_registro' => $cd_registro,
                        'dt_nascimento' => $data['sdtNascimento'],
                        'ind_sexo' => $data['sexo'] == 'true' ? 1 : 0,
                        'cd_empresa' => $data['cd_empresa'] ?? null,
                        'fg_professor' => 1
                    ]);

                $prof = Professor::
                    insert([
                        'cd_pessoa' => $idPessoa,
                        'ds_cref' => $data['ds_cref'],
                        'dt_cref' => $data['dt_cref'],
                        'nr_valor_mensal' => $data['nr_valor_mensal']
                    ]);

                $pais = DB::
                    table('pais')
                    ->where('ds_codigo', $data['dsCodigoPais'])
                    ->value('cd_pais');

                if (count($pais) == NULL)
                {
                    $pais = DB::
                        table('pais')
                        ->insertGetId([
                            'ds_codigo' => $data['dsCodigoPais'],
                            'ds_pais' => isset($data['dsPais']) ? $data['dsPais'] : $data['dsCodigoPais']
                        ]);
                }

                $estado = DB::
                    table('estados')
                    ->where('ds_estado', $data['dsUf'])
                    ->where('cd_pais', $pais)
                    ->value('cd_estado');

                if (count($estado) == NULL)
                {
                    $estado = DB::
                        table('estados')
                        ->insertGetId([
                            'cd_pais' => $pais,
                            'ds_estado' => $data['dsUf']
                        ]);
                }

                $cidade = DB::
                    table('cidades')
                    ->where('ds_cidade', $data['dsCidade'])
                    ->where('cd_estado', $estado)
                    ->value('cd_cidade');

                if (count($cidade) == NULL)
                {
                    $cidade = DB::
                        table('cidades')
                        ->insertGetId([
                            'cd_estado' => $estado,
                            'ds_cidade' => $data['dsCidade']
                        ]);
                }

                $endereco = DB::
                    table('enderecos')
                    ->insertGetId([
                        'cd_cidade' => $cidade,
                        'ds_ut' => $data['dsUf'],
                        'ds_cidade' => $data['dsCidade'],
                        'ds_bairro' => '',
                        'ds_endereco' => '',
                    ]);

                DB::
                    table('registros')
                    ->where('cd_registro', $cd_registro)
                    ->update(['cd_endereco' => $endereco]);
            }
        }
        catch(\Excetion $e)
        {
            return Response(
                ['msg' =>'Erro ao cadastrar novos dados','erro' => $e],
                400
            );
        }
        return Response(['msg' =>'SUCCESS'], 200);
    }

    public function getProfessorV2($cd_professor)
    {
        try
        {
            $jwtLogin = Auth::user();

            $login = Login::
                with(
                    'registro',
                    'registro.pessoa'
                )
                ->where('cd_login', $jwtLogin->cd_login)
                ->first();

            $professor = Registro::
                where('professores.cd_professor', $cd_professor)
                ->join('pessoas','pessoas.cd_registro','=','registros.cd_registro')
                ->join('professores','professores.cd_pessoa','=','pessoas.cd_pessoa')
                ->leftJoin('enderecos','enderecos.cd_endereco','=','registros.cd_endereco')
                ->leftJoin('cidades', 'cidades.cd_cidade', '=','enderecos.cd_cidade')
                ->leftJoin('estados', 'estados.cd_estado', '=','cidades.cd_estado')
                ->leftJoin('pais','estados.cd_pais','=','pais.cd_pais')
                ->leftJoin('empresa','empresa.id','=','pessoas.cd_empresa')
                ->leftJoin('amizades', function($join) use ($login) {
                    return $join
                        ->on('amizades.cd_from', '=', 'pessoas.cd_pessoa')
                        ->on('amizades.cd_to', '=', \DB::raw($login->registro->pessoa->cd_pessoa))
                        ->orOn(function($join) use($login) {
                            return $join
                                ->on('amizades.cd_from', '=', \DB::raw($login->registro->pessoa->cd_pessoa))
                                ->on('amizades.cd_to', '=', 'pessoas.cd_pessoa');
                        });
                })
                ->select(
                    'registros.cd_registro as cdRegistroExterno',
                    'registros.ind_registro as indTipoRegistro',
                    'amizades.cd_amizade',
                    \DB::raw('IF(amizades.cd_amizade IS NULL, 0, IF(amizades.fg_aceito = 1, 2, 1)) AS friendship_status'),
                    'registros.ds_avatar as dsAvatar',
                    'pessoas.ind_sexo as sexo',
                    'registros.ds_nome as dsNome',
                    'professores.ds_cref as dsCref',
                    'professores.dt_cref as dtCref',
                    'pessoas.dt_nascimento as sdtNascimento',
                    'professores.nr_valor_mensal as nrValorMensal',
                    'pessoas.ind_sexo as masculino',
                    'enderecos.ds_cidade as dsCidade',
                    'enderecos.ds_ut as dsUf',
                    'pais.ds_pais as dsPais',
                    'pais.ds_codigo as dsCodigoPais',
                    'empresa.id AS cdEmpresa',
                    'empresa.nome as dsNomeEmpresa'
                )
                ->first();

            if (count($professor) != 0)
            {
                $professor['sexo'] = ($professor['sexo'] == 1) ? true : false;
                $professor['masculino'] = ($professor['masculino'] == 1) ? true : false;
                $professor['endereco'] = [
                    'dsCidade' => $professor['dsCidade'],
                    'dsUf' => $professor['dsUf'],
                    'dsPais' => $professor['dsPais'],
                    'dsCodigoPais' => $professor['dsCodigoPais']
                ];
                if ($professor['cdEmpresa'])
                {
                    $professor['gym'] = [
                        'id' => $professor['cdEmpresa'],
                        'name' => $professor['dsNomeEmpresa']
                    ];
                }
                unset($professor['cdEmpresa']);
                unset($professor['dsNomeEmpresa']);
                return Response($professor, 200);
            }
            return Response([ 'msg' => 'Professor não encontrado' ], 404);
        }
        catch(\Exception $e)
        {
            return Response([
                'msg' => 'Erro ao consultar professor',
                'error' => $e
            ], 400);
        }
    }

    public function listarAlunosV2()
    {
        $data = Input::all();
        $rules = [
            'pageIndex' => 'integer',
            'pageSize' => 'integer',
            'query' => 'string|max:20'
        ];

        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            return Response([
                'msg' => $validation->getMessageBag()->first()
            ], 400);
        }

        $jwtLogin = Auth::user();
        $login = Login::
            with(
                'registro.pessoa.professor',
                'registro.pessoa.aluno'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa || !$login->registro->pessoa->professor) {
            return Response([
                'msg' => 'Não autorizado'
            ], 401);
        }

        $cd_professor = $login->registro->pessoa->professor->cd_professor;
        $pageIndex = $data['pageIndex'] ?? 1;
        $pageSize = $data['pageSize'] ?? 10;
        $query = $data['query'] ?? null;
        //
        $pageIndex = (int) $pageIndex;
        $pageSize = (int) $pageSize;

        $from = ($pageIndex - 1) * $pageSize;
        //

        $args = [
            $cd_professor,
            $cd_professor,
            $pageSize,
            $from
        ];

        $argsCount = [
            $cd_professor,
            $cd_professor
        ];

        $search = '';
        if ($query) {
            $search = " AND (r.ds_nome LIKE ? OR a.cd_aluno LIKE ?) ";
            $args = [
                $cd_professor,
                $cd_professor,
                '%'.$query.'%',
                '%'.$query.'%',
                $pageSize,
                $from
            ];
            $argsCount = [
                $cd_professor,
                $cd_professor,
                '%'.$query.'%',
                '%'.$query.'%'
            ];
        }

        //
        $alunos = DB::select("
            SELECT   
                a.cd_aluno,
                CASE WHEN pe.ind_sexo = 1 THEN 'TRUE' ELSE 'FALSE' END as sexo,
                r.ds_nome,
                r.ds_avatar,
                ap.dt_inclusao,
                ap.dt_expiracao 
            FROM planos p 
            INNER JOIN aluno_plano ap ON ap.cd_plano = p.cd_plano 
            INNER JOIN alunos a  ON a.cd_aluno  = ap.cd_aluno 
            INNER JOIN pessoas pe ON pe.cd_pessoa = a.cd_pessoa 
            INNER JOIN registros r ON r.cd_registro = pe.cd_registro
            where p.cd_professor  = ?
            AND ap.cd_plano = 
            (SELECT MAX(ap2.cd_plano) AS cd_plano 
            FROM aluno_plano ap2 
            INNER JOIN planos p2 ON p2.cd_plano = ap2.cd_plano 
            WHERE ap2.cd_aluno = a.cd_aluno AND p2.cd_professor = ? AND p2.active  = 1)
            AND IFNULL(r.fg_excluido, 0) <> 1
            AND p.active  = 1
            ". $search ." 
            ORDER BY ap.dt_expiracao ASC
            LIMIT ? 
            OFFSET ?
        ;", $args);


        $count = DB::select("
            SELECT
                count(1) AS count
            FROM planos p 
            INNER JOIN aluno_plano ap ON ap.cd_plano = p.cd_plano 
            INNER JOIN alunos a  ON a.cd_aluno  = ap.cd_aluno 
            INNER JOIN pessoas pe ON pe.cd_pessoa = a.cd_pessoa 
            INNER JOIN registros r ON r.cd_registro = pe.cd_registro
            where p.cd_professor  = ?
            AND ap.cd_plano = 
            (SELECT MAX(ap2.cd_plano) AS cd_plano 
            FROM aluno_plano ap2 
            INNER JOIN planos p2 ON p2.cd_plano = ap2.cd_plano 
            WHERE ap2.cd_aluno = a.cd_aluno AND p2.cd_professor = ? AND p2.active  = 1)
            AND IFNULL(r.fg_excluido, 0) <> 1
            AND p.active  = 1
            ". $search ."
        ;",$argsCount);

        $total = 0;
        $totalPages = 0;
        if ($count && count($count) > 0) {
            $total = $count[0]->count ?? 0;
            $totalPages = ceil($total / $pageSize);
        }

        return Response([
            'alunos' => $alunos,
            'pageCount' => $totalPages,
            'count' => $total
        ]);
    }


    public function listarAlunosV3() {
        $data = Input::all();
        $rules = [
            'tipo' => 'integer',
            'pageIndex' => 'integer',
            'pageSize' => 'integer',
            'query' => 'string|max:20',
            'sexo' => 'string',
            'expiration_close' => 'boolean',
            'expired' => 'boolean',
        ];

        $validation = Validator::make($data, $rules);
        if ($validation->fails())
        {
            return Response([
                'msg' => $validation->getMessageBag()->first()
            ], 400);
        }

        $jwtLogin = Auth::user();
        $login = Login::
            with(
                'registro.pessoa.professor',
                'registro.pessoa.aluno'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa || !$login->registro->pessoa->professor)
        {
            return Response([
                'msg' => 'Não autorizado'
            ], 401);
        }

        $cd_professor = $login->registro->pessoa->professor->cd_professor;

        $tipo = $data['tipo'] ?? 0;
        $pageIndex = $data['pageIndex'] ?? 1;
        $pageSize = $data['pageSize'] ?? 10;
        $query = $data['query'] ?? null;
        $sex = $data['sex'] ?? null;
        $expirationClose = $data['expiration_close'] ?? null;
        $expired = $data['expired'] ?? null;

        // All attributes come as string when passed as queryParams
        $tipo = (int) $tipo;
        $pageIndex = (int) $pageIndex;
        $pageSize = (int) $pageSize;

        $from = ($pageIndex - 1) * $pageSize;

        $args = [
            $cd_professor,
            $pageSize,
            $from
        ];
        $argsCount = [ $cd_professor ];

        $sameAcademy = '';
        if ($tipo === 0)
        {
            $sameAcademy = ' AND IFNULL(profpe.cd_empresa, 0) = IFNULL(pe.cd_empresa,0) ';
        }
        else if ($tipo === 1)
        {
            $sameAcademy = ' AND IFNULL(profpe.cd_empresa,0) <> IFNULL(pe.cd_empresa,0) ';
        }

        $search = '';
        if ($query)
        {
            $search = " AND (r.ds_nome LIKE ? OR a.cd_aluno LIKE ?) ";
            $args = [
                $cd_professor,
                '%'.$query.'%',
                '%'.$query.'%',
                $pageSize,
                $from
            ];
            $argsCount = [
                $cd_professor,
                '%'.$query.'%',
                '%'.$query.'%'
            ];
        }

        if ($sex)
        {
            $search.= " AND pe.ind_sexo = ". ($sex == 'M' ? '0 ' : '1 ');
        }

        $joinPlanAluno = '';
        if ($expirationClose)
        {
            $joinPlanAluno = "
                INNER JOIN
                    aluno_plano AS aplano
                        ON aplano.cd_aluno = a.cd_aluno
                INNER JOIN
                    planos AS pla
                        ON pla.cd_plano  = aplano.cd_plano AND pla.cd_professor = prof.cd_professor
            ";
            $search.= " AND DATE_ADD(aplano.dt_expiracao,INTERVAL -7 DAY) <= NOW() ";
        }
        if ($expired)
        {
            $joinPlanAluno = "
                INNER JOIN
                    aluno_plano AS aplano
                        ON aplano.cd_aluno = a.cd_aluno
                INNER JOIN
                    planos AS pla
                        ON pla.cd_plano  = aplano.cd_plano AND pla.cd_professor = prof.cd_professor
            ";
            $search.= " AND aplano.dt_expiracao <= NOW() ";
        }

        $alunos = DB::select("
            SELECT
                a.cd_aluno,
                pe.cd_pessoa,
                pe.ind_sexo as sexo,
                r.cd_registro,
                r.ds_nome,
                r.ds_avatar,
                r.dt_inclusao
            FROM
                professores AS prof
            INNER JOIN
                aluno_professor AS ap
                    ON ap.cd_professor = prof.cd_professor
            INNER JOIN
                alunos AS a
                    ON a.cd_aluno = ap.cd_aluno
            INNER JOIN
                pessoas AS pe
                    ON pe.cd_pessoa = a.cd_pessoa
            INNER JOIN
                pessoas AS profpe
                    ON profpe.cd_pessoa = prof.cd_pessoa
            INNER JOIN
                registros AS r
                    ON r.cd_registro = pe.cd_registro
            " . $joinPlanAluno . "
            WHERE
                prof.cd_professor = ?
                AND IFNULL(r.fg_excluido, 0) <> 1
            "
            . $sameAcademy . $search .
            " LIMIT ? OFFSET ?;",
            $args
        );

        foreach ($alunos as $a)
        {
            $a->sexo = $a->sexo == 1;
        }

        $count = DB::select("
            SELECT
                COUNT(1) AS count
            FROM
                professores AS prof
            INNER JOIN
                aluno_professor AS ap
                    ON ap.cd_professor = prof.cd_professor
            INNER JOIN
                alunos AS a
                    ON a.cd_aluno = ap.cd_aluno
            INNER JOIN
                pessoas AS pe
                    ON pe.cd_pessoa = a.cd_pessoa
            INNER JOIN
                pessoas AS profpe
                    ON profpe.cd_pessoa = prof.cd_pessoa
            INNER JOIN
                registros AS r
                    ON r.cd_registro = pe.cd_registro
            ".$joinPlanAluno . "
            WHERE
                prof.cd_professor = ?
                AND IFNULL(r.fg_excluido, 0) <> 1
            " . $sameAcademy . $search .";",
            $argsCount
        );

        $total = 0;
        $totalPages = 0;
        if ($count && count($count) > 0)
        {
            $total = $count[0]->count ?? 0;
            $totalPages = ceil($total / $pageSize);
        }

        return Response([
            'alunos' => $alunos,
            'pageCount' => $totalPages,
            'count' => $total
        ]);
    }

    public function homeData()
    {
        $data = Input::all();

        /** Faz a validação do token */

        $jwtLogin = Auth::user();

        $login = Login::
            with(
                'registro',
                'registro.pessoa'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();


        if (!$login || !$login->registro || !$login->registro->pessoa) {
            return Response([
                'msg' => 'Não autorizado'
            ], 401);
        }

        $homeData = DB::
            select("
                SELECT 

                (SELECT COUNT(1)
                FROM aluno_plano ap
                INNER JOIN planos p ON ap.cd_plano = p.cd_plano 
                AND p.active = 1
                WHERE p.cd_professor = ?
                AND DATEDIFF(ap.dt_expiracao, SYSDATE()) < 0) AS expired,
                
                (SELECT COUNT(1)
                FROM aluno_plano ap
                INNER JOIN planos p ON ap.cd_plano = p.cd_plano 
                AND p.active = 1
                WHERE p.cd_professor = ?
                AND DATEDIFF(ap.dt_expiracao, SYSDATE()) > 0 
                AND DATEDIFF(ap.dt_expiracao, SYSDATE()) < 10 ) AS near_expiration,

                (SELECT count(1)
                FROM aluno_plano ap
                INNER JOIN planos p ON p.cd_plano = ap.cd_plano 
                WHERE p.cd_professor = ?
                AND p.active = 1) as qtt_student,

                (SELECT
                    COUNT(1) AS count
                FROM
                    amizades
                WHERE
                    fg_aceito <> 1 AND
                    cd_to = ?) as qtt_notification
                ;",
                [
                    $login->registro->pessoa->professor->cd_professor,
                    $login->registro->pessoa->professor->cd_professor,
                    $login->registro->pessoa->professor->cd_professor,
                    $login->registro->pessoa->cd_pessoa
                ]
            );
        $response = [
            "homeData" => $homeData[0]
        ];

        return Response($response, 200);
    }

    public function listAllTeachers(Request $request)
    {
        $data = Input::all();
        $rules = [
            'pageIndex' => 'integer',
            'pageSize' => 'integer',
            'query' => 'string|max:24'
        ];
        $validation = Validator::make($data, $rules);

        // Busca por ID e NOME
        // Devolver imagem de perfil, nome, endereço e situação de amizade comigo.

        $jwtLogin = Auth::user();

        $login = Login::
            with(
                'registro',
                'registro.pessoa'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();


        if (!$login || !$login->registro || !$login->registro->pessoa)
        {
            return Response(
                [ 'msg' => 'Não autorizado' ],
                401
            );
        }

        $gym = $login->registro->pessoa->cd_empresa;
        if (!$gym)
        {
            return Response(
                [ 'msg' => 'Por favor se víncule a uma academia.' ],
                404
            );
        }

        $pageIndex = $data['pageIndex'] ?? 1;
        $pageSize = $data['pageSize'] ?? 10;
        $query = $data['query'] ?? null;

        // All attributes come as string when passed as queryParams
        $pageIndex = (int) $pageIndex;
        $pageSize = (int) $pageSize;

        $from = ($pageIndex - 1) * $pageSize;

        $args = [
            $login->registro->pessoa->cd_pessoa,
            $login->registro->pessoa->cd_pessoa,
            $gym,
            $pageSize,
            $from
        ];
        $argsCount = [
            $login->registro->pessoa->cd_pessoa,
            $login->registro->pessoa->cd_pessoa,
            $gym
        ];

        $search = '';
        if ($query)
        {
            $search = " AND (r.ds_nome LIKE ? OR p.cd_professor LIKE ?) ";
            $args = [
                $login->registro->pessoa->cd_pessoa,
                $login->registro->pessoa->cd_pessoa,
                $gym,
                '%'.$query.'%',
                '%'.$query.'%',
                $pageSize,
                $from
            ];
            $argsCount = [
                $login->registro->pessoa->cd_pessoa,
                $login->registro->pessoa->cd_pessoa,
                $gym,
                '%'.$query.'%',
                '%'.$query.'%'
            ];
        }

        $professores = DB::select("
            SELECT
                p.cd_professor,
                cid.ds_cidade,
                est.ds_estado,
                pai.ds_pais,
                pai.ds_codigo,
                r.ds_nome,
                r.ds_avatar,
                r.dt_inclusao,
                pe.ind_sexo AS sexo,
                pe.cd_pessoa,
                IF(am.cd_amizade IS NULL, 0, IF(am.fg_aceito = 1, 2, 1)) AS friendship_status
            FROM
                professores AS p
            INNER JOIN
                pessoas AS pe
                    ON pe.cd_pessoa = p.cd_pessoa
            INNER JOIN
                registros AS r
                    ON r.cd_registro = pe.cd_registro
            LEFT JOIN
                enderecos AS e
                    ON e.cd_endereco = r.cd_endereco
            LEFT JOIN
                cidades AS cid
                    ON cid.cd_cidade = e.cd_cidade
            lEFT JOIN
                estados AS est
                    ON est.cd_estado = cid.cd_estado
            LEFT JOIN
                pais AS pai
                    ON pai.cd_pais = est.cd_pais
            LEFT JOIN
                amizades AS am ON
                    (am.cd_from = pe.cd_pessoa AND am.cd_to = ?) OR
                    (am.cd_from = ? AND am.cd_to = pe.cd_pessoa)
            WHERE
                IFNULL(r.fg_excluido, 0) <> 1
                AND IFNULL(pe.cd_empresa, 0) = ?
            "
            . $search .
            "
                ORDER BY r.ds_nome
                LIMIT ? OFFSET ?
            ;",
            $args
        );

        $count = DB::select("
            SELECT
                COUNT(1) AS count
            FROM
                professores AS p
            INNER JOIN
                pessoas AS pe
                    ON pe.cd_pessoa = p.cd_pessoa
            INNER JOIN
                registros AS r
                    ON r.cd_registro = pe.cd_registro
            LEFT JOIN
                enderecos AS e
                    ON e.cd_endereco = r.cd_endereco
            LEFT JOIN
                cidades AS cid
                    ON cid.cd_cidade = e.cd_cidade
            lEFT JOIN
                estados AS est
                    ON est.cd_estado = cid.cd_estado
            LEFT JOIN
                pais AS pai
                    ON pai.cd_pais = est.cd_pais
            LEFT JOIN
                amizades AS am ON
                    (am.cd_from = pe.cd_pessoa AND am.cd_to = ?) OR
                    (am.cd_from = ? AND am.cd_to = pe.cd_pessoa)
            WHERE
                IFNULL(r.fg_excluido, 0) <> 1
                AND IFNULL(pe.cd_empresa, 0) = ?
            "
            . $search .
            ";",
            $argsCount
        );

        $total = 0;
        $totalPages = 0;
        if ($count && count($count) > 0)
        {
            $total = $count[0]->count ?? 0;
            $totalPages = ceil($total / $pageSize);
        }

        foreach($professores as $prof) {
            $prof->endereco = [
                'dsCidade' => $prof->ds_cidade ?? '',
                'dsUf' => $prof->ds_estado ?? '',
                'dsPais' => $prof->ds_pais ?? '',
                'dsCodigoPais' => $prof->ds_codigo ?? ''
            ];
            unset($prof->ds_cidade);
            unset($prof->ds_estado);
            unset($prof->ds_pais);
            unset($prof->ds_codigo);

            $prof->sexo = $prof->sexo == 1 ? true : false;

        }

        return Response([
            'professores' => $professores,
            'pageCount' => $totalPages,
            'count' => $total
        ]);
    }
}
