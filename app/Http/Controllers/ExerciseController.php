<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Input;
use DB;


class ExerciseController extends Controller {
    public function getExerciseV2($cd_exercise) {
        try {
            $exercises = DB::select("
                SELECT
                    e.cd_exercicio,
                    e.ds_nome,
                    e.ds_nome_logico,
                    e.ds_nome_logico_gif_exercicio,
                    e.cd_video,
                    e.ds_exercicio,
                    e.url_thumbnails_default,
                    e.url_thumbnails_medium,
                    e.url_thumbnails_high,
                    e.cd_grupo,
                    g.ds_nome as ds_nome_grupo,
                    g.id_image as id_image_grupo,
                    g.id_image_info as id_image_info_grupo,
                    g.ds_url as ds_url_grupo,
                    m.cd_musculo,
                    m.ds_musculo
                FROM
                    exercicios AS e
                INNER JOIN
                    grupos AS g
                        ON g.cd_grupo = e.cd_grupo
                LEFT JOIN
                    musculo_exercicio AS me
                        ON me.cd_exercicio = e.cd_exercicio
                LEFT JOIN
                    musculos AS m
                        ON m.cd_musculo = me.cd_musculo
                WHERE
                    e.cd_exercicio = ?
                ;",
                [$cd_exercise]
            );
            if (count($exercises) != 0) {
                $resp = [
                    'cd_exercicio' => $exercises[0]->cd_exercicio,
                    'ds_nome' => $exercises[0]->ds_nome,
                    'ds_nome_logico' => $exercises[0]->ds_nome_logico,
                    'ds_nome_logico_gif_exercicio' => $exercises[0]->ds_nome_logico_gif_exercicio,
                    'cd_video' => $exercises[0]->cd_video,
                    'ds_exercicio' => $exercises[0]->ds_exercicio,
                    'url_thumbnails_default' => $exercises[0]->url_thumbnails_default,
                    'url_thumbnails_medium' => $exercises[0]->url_thumbnails_medium,
                    'url_thumbnails_high' => $exercises[0]->url_thumbnails_high,
                    'grupo' => [
                        'cd_grupo' =>   $exercises[0]->cd_grupo,
                        'ds_nome' =>  $exercises[0]->ds_nome_grupo,
                        'id_image' =>  $exercises[0]->id_image_grupo,
                        'id_image_info' =>  $exercises[0]->id_image_info_grupo,
                        'ds_url' =>  $exercises[0]->ds_url_grupo
                    ],
                    'musculos' => []
                ];

                foreach ($exercises as $ex) {
                    if ($ex->cd_musculo) {
                        $resp['musculos'][] = [
                            'cd_musculo' => $ex->cd_musculo,
                            'ds_musculo' => $ex->ds_musculo
                        ];
                    }
                }
                if (count($resp['musculos']) == 0) {
                    unset($resp['musculos']);
                }


                return Response($resp, 200);
            }
            return Response([ 'msg' => 'Exercício não encontrado' ], 404);
        } catch(\Exception $e) {
            return Response([
                'msg' => 'Erro ao consultar o exercicio',
                'error' => $e
            ], 400);
        }

    }
    public function listExercises(Request $request) {
        $data = Input::all();
        $rules = [
            'group' => 'integer|exists:grupos,cd_grupo',
            'query' => 'string|max:24'
        ];
        $validation = Validator::make($data, $rules);

        $group = $data['group'] ?? null;
        $query = $data['query'] ?? null;
        $args = [];

        $whereGroup = '';
        if ($group) {
            $whereGroup = ' WHERE e.cd_grupo = ? ';
            $args[] = (int) $group;
        }

        $whereSearch = '';
        if ($query) {
            $whereSearch = ' e.ds_nome LIKE ? ';
            if ($group) {
                $whereSearch = 'AND '.$whereSearch;
            } else {
                $whereSearch = 'WHERE '.$whereSearch;
            }
            $args[] = '%'.$query.'%';
        }

        try {
            $exercises = DB::select("
                SELECT
                    e.cd_exercicio,
                    e.ds_nome,
                    e.ds_nome_logico,
                    e.ds_nome_logico_gif_exercicio,
                    e.cd_video,
                    e.ds_exercicio,
                    e.url_thumbnails_default,
                    e.url_thumbnails_medium,
                    e.url_thumbnails_high,
                    e.cd_grupo,
                    g.ds_url AS ds_url_grupo,
                    me.ind_primario_secundario,
                    m.cd_musculo,
                    m.ds_musculo
                FROM
                    exercicios AS e
                INNER JOIN
                    grupos AS g
                        ON g.cd_grupo = e.cd_grupo
                LEFT JOIN
                    musculo_exercicio AS me
                        ON me.cd_exercicio = e.cd_exercicio
                LEFT JOIN
                    musculos AS m
                        ON m.cd_musculo = me.cd_musculo
                " . $whereGroup . $whereSearch .
                "
                    ORDER BY e.ds_nome
                ;",
                $args
            );

            $resp = [];

            $exercise_set = [];

            foreach($exercises as $e) {
                if (!array_key_exists($e->cd_exercicio, $exercise_set)) {
                    $exercise_set[$e->cd_exercicio] = [
                        'cd_exercicio' => $e->cd_exercicio,
                        'ds_nome' => $e->ds_nome,
                        'ds_nome_logico' => $e->ds_nome_logico,
                        'ds_nome_logico_gif_exercicio' => $e->ds_nome_logico_gif_exercicio,
                        'cd_video' => $e->cd_video,
                        'ds_exercicio' => $e->ds_exercicio,
                        'url_thumbnails_default' => $e->url_thumbnails_default,
                        'url_thumbnails_medium' => $e->url_thumbnails_medium,
                        'url_thumbnails_high' => $e->url_thumbnails_high,
                        'cd_grupo' => $e->cd_grupo,
                        'ds_url_grupo' => $e->ds_url_grupo,
                        'muscles' => [
                            'primary' => [],
                            'secondary' => []
                        ]
                    ];
                }
                if ($e->ind_primario_secundario == 1) {
                    $exercise_set[$e->cd_exercicio]['muscles']['primary'][] = [
                        "cd_musculo" => $e->cd_musculo,
                        "ds_musculo" => $e->ds_musculo
                    ];
                } else if ($e->ind_primario_secundario == 2) {
                    $exercise_set[$e->cd_exercicio]['muscles']['secondary'][] = [
                        "cd_musculo" => $e->cd_musculo,
                        "ds_musculo" => $e->ds_musculo
                    ];
                }
            }

            return Response([
                'exercises' => array_values($exercise_set),
            ]);
        } catch(\Exception $e) {
            return Response([
                'msg' => 'Erro ao consultar os exercicios',
                'error' => $e
            ], 400);
        }
    }
}
