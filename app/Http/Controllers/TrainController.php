<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Input;
use App\Login;
use App\Treino;
use App\Plano;
use App\Exercicio;
use DB;
use Auth;

class TrainController extends Controller {
    public function create() {
        $data = Input::all();
        $rules = [
            'description' => 'nullable|string',
            'ind_tipo_treino' => 'integer',
            'ind_nivel_treino' => 'integer',
            'cd_grupo' => 'required|integer|exists:grupos',
            'exercises' => 'array',
            'cd_plano' => 'required|integer'
        ];
        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            return Response([
                'msg'=> $validation->getMessageBag()->first()
            ], 400);
        }

        $jwtLogin = Auth::user();
        $login = Login::
            with(
                'registro.pessoa.professor',
                'registro.pessoa.aluno'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa || !$login->registro->pessoa->professor) {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }

        $cd_professor = $login->registro->pessoa->professor->cd_professor;

        $plano = null;
        if (array_key_exists('cd_plano', $data) && isset($data['cd_plano'])) {
            $plano = Plano::
                select('cd_plano')
                ->where('cd_plano', $data['cd_plano'])
                ->first();

            if (!$plano || !$plano->cd_plano) {
                return Response([ 'msg' => 'Plano informado não existe' ], 404);
            }
        }

        // verificar se já existe um treino desse plano para esse grupo
        $args = [ $data['cd_plano'],$data['cd_grupo'] ];

        $oldTrain = DB::select("
            SELECT t.cd_treino FROM plano_treino pt 
            INNER JOIN treinos t ON pt.cd_treino = t.cd_treino 
            WHERE pt.cd_plano = ? AND t.cd_grupo = ?
        ",
        $args);

        if(!isset($oldTraind) && count($oldTrain) > 0){
            
            foreach($oldTrain as $t) {
                // se já existe o treino, deleta o plano treino, treino exercicio, e treinos
                DB::
                table('plano_treino')
                ->where('cd_plano',  $data['cd_plano'])
                ->where('cd_treino', $t->cd_treino)
                ->delete();

                DB::
                table('treino_exercicio')
                ->where('cd_treino',  $t->cd_treino)
                ->delete();

                DB::
                table('treinos')
                ->where('cd_treino',  $t->cd_treino)
                ->delete();
            }
        }

        if(empty($data['description'])){
            $data['description'] = null;
        }

        $cdTreino = Treino::
        insertGetId([
            'description' => $data['description'] ?? null,
            'ind_tipo_treino' => $data['ind_tipo_treino'] ?? null,
            'ind_nivel_treino' => $data['ind_nivel_treino'] ?? null,
            'cd_grupo' => $data['cd_grupo'],
            'cd_professor' => $cd_professor
        ]);

        $train = Treino::find($cdTreino);
    
        if(isset($data['exercises'])){
            if (count($data['exercises'])) {

                $exercise_ids = array_map(function($e) {
                    return $e['cd_exercicio'];
                }, $data['exercises']);
    
                $count = Exercicio::
                    whereIn('cd_exercicio', $exercise_ids)
                    ->count();
                if ($count != count($exercise_ids)) {
                    return Response([ 'msg' => 'Lista de exercícios inválida' ], 400);
                }
            }

            foreach($data['exercises'] AS $exercise) {
                $repeticoes = '';
                $ordem = 0;

                if (array_key_exists('nr_ordem', $exercise)) {
                    $ordem = $exercise['nr_ordem'];
                }

                $repetitionLabel = "";

                if (array_key_exists('nr_repeticoes', $exercise)) {
                    $repetitionLabel = $exercise['nr_repeticoes'];
                }

                $train->exercises()->attach($exercise['cd_exercicio'], 
                    [
                        'nr_ordem' => $exercise['nr_ordem'],
                        'nr_repeticoes' => $repetitionLabel,
                        'divider' => $exercise['divider'] ?? 'A',
                        'ds_exercicio_professor' => $exercise['ds_exercicio_professor']  ?? ' '
                    ]
                );
            }
        }

        if (isset($plano)) {
            $plano->trains()->attach($cdTreino);
        }

        return Response([ 'cd_treino' => $cdTreino ], 200);
    }

    public function listFromTeacher() {
        $data = Input::all();
        $rules = [
            'query' => 'string|max:24'
        ];
        $validation = Validator::make($data, $rules);

        $jwtLogin = Auth::user();
        $login = Login::
            with(
                'registro.pessoa.professor',
                'registro.pessoa.aluno'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa || !$login->registro->pessoa->professor) {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }

        $cd_professor = $login->registro->pessoa->professor->cd_professor;

        $where = '';
        $args = [ $cd_professor ];
        if(array_key_exists('query', $data) && $data['query']) {
            $args = [ '%'.$data['query'].'%', $cd_professor ];
            $where = "ds_nome LIKE ? AND";
        }

        try {
            $trains = DB::select("
                SELECT
                    cd_treino,
                    ds_nome,
                    ds_picture,
                    fg_carga,
                    ind_tipo_treino,
                    ind_nivel_treino,
                    cd_grupo
                FROM
                    treinos
                WHERE
                    " . $where . "
                    cd_professor = ?
                ORDER BY
                    cd_treino
                ",
                $args
            );
            return Response([ 'trains' =>  $trains ]);
        } catch(\Exception $e) {
            return Response([
                'msg' => 'Erro ao consultar os treinos',
                'error' => $e
            ], 400);
        }
    }

    public function getTrainingExercises($cd_treino){
        $jwtLogin = Auth::user();
        $login = Login::
            with(
                'registro.pessoa.professor',
                'registro.pessoa.aluno'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa || (!$login->registro->pessoa->professor && 
        !$login->registro->pessoa->aluno)) {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }
        $exercises = DB::select("
        SELECT
            te.nr_ordem,
            te.nr_repeticoes,
            te.ds_exercicio_professor,
            e.cd_exercicio,
            e.ds_nome,
            e.ds_nome_logico,
            e.ds_exercicio,
            e.cd_grupo AS exercicio_cd_grupo,
            e.cd_video,
            e.url_thumbnails_default,
            e.url_thumbnails_medium,
            e.url_thumbnails_high,
            e.ds_nome_logico_gif_exercicio,
            me.ind_primario_secundario,
            m.cd_musculo,
            m.ds_musculo,
            te.divider
        FROM
            treinos AS t
        INNER JOIN
            plano_treino AS pt
                ON pt.cd_treino  = t.cd_treino
        INNER JOIN
            treino_exercicio  AS te
                ON te.cd_treino  = t.cd_treino
        INNER JOIN
            exercicios AS e
                ON e.cd_exercicio  = te.cd_exercicio
        LEFT JOIN
            musculo_exercicio AS me
                ON me.cd_exercicio = e.cd_exercicio
        LEFT JOIN
            musculos AS m
                ON m.cd_musculo = me.cd_musculo
        WHERE
            t.cd_treino  = ?
        ORDER BY te.nr_ordem ASC;
        ",
        [$cd_treino]);

        $resp = [];

        $exercise_set = [];

        foreach($exercises as $e) {
            if (!array_key_exists($e->cd_exercicio, $exercise_set)) {
                $exercise_set[$e->cd_exercicio] = [
                    'nr_ordem' => $e->nr_ordem,
                    'nr_repeticoes' => $e->nr_repeticoes,
                    'ds_exercicio_professor' => $e->ds_exercicio_professor,
                    'cd_exercicio' => $e->cd_exercicio,
                    'ds_nome' => $e->ds_nome,
                    'ds_nome_logico' => $e->ds_nome_logico,
                    'ds_exercicio' => $e->ds_exercicio,
                    'exercicio_cd_grupo' => $e->exercicio_cd_grupo,
                    'cd_video' => $e->cd_video,
                    'url_thumbnails_default' => $e->url_thumbnails_default,
                    'url_thumbnails_medium' => $e->url_thumbnails_medium,
                    'url_thumbnails_high' => $e->url_thumbnails_high,
                    'ds_nome_logico_gif_exercicio' => $e->ds_nome_logico_gif_exercicio,
                    'muscles' => [
                        'primary' => [],
                        'secondary' => []
                    ],
                    'divider' => $e->divider
                ];
            }
            if ($e->ind_primario_secundario == 1) {
                $exercise_set[$e->cd_exercicio]['muscles']['primary'][] = [
                    "cd_musculo" => $e->cd_musculo,
                    "ds_musculo" => $e->ds_musculo
                ];
            } else if ($e->ind_primario_secundario == 2) {
                $exercise_set[$e->cd_exercicio]['muscles']['secondary'][] = [
                    "cd_musculo" => $e->cd_musculo,
                    "ds_musculo" => $e->ds_musculo
                ];
            }
        }

        return Response([
            'exercises' => array_values($exercise_set),
        ]);

        // return Response([
        //     'exercises' => $result
        // ]);
    }
}
