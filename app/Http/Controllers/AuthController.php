<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Validator;
use App\Login;
use App\Registro;
use App\Aluno;
use App\Pessoa;
use DB;
use JWTAuth;
use Auth;

class AuthController extends Controller
{
    public function login()
    {
        $data = Input::all();
        $rules = [
            'email' => 'required|string',
            'senha' => 'required|string',
            'indTipoRegistro' => 'integer',
        ];

        $validation = Validator::make($data, $rules);
        if ($validation->fails())
        {
            return Response(
                [ 'msg' => $validation->getMessageBag()->first() ], 400
            );
        }

        $indTipoRegistro = isset($data['indTipoRegistro']) ? $data['indTipoRegistro'] : 1;
        if ($indTipoRegistro !== 1 && $indTipoRegistro !== 2)
        {
            $indTipoRegistro = 1;
        }

        try
        {
            $login = Login::
                where('ds_email', $data['email'])
                ->first();

            if (!$login)
            {
                return Response(['msg' =>'Email não cadastrado'], 404);
            }
            else if ($login['ds_senha'] != $data['senha'])
            {
                return Response(['msg' =>'Senha incorreta'], 401);
            }

            $token = JWTAuth::fromUser($login);
            if ($indTipoRegistro === 1)
            {
                $aluno = Registro::
                    where('registros.cd_registro', $login['cd_registro'])
                    ->where('registros.ind_registro', $indTipoRegistro)
                    ->leftJoin('pessoas','pessoas.cd_registro','=','registros.cd_registro')
                    ->leftJoin('alunos','alunos.cd_pessoa','=','pessoas.cd_pessoa')
                    ->leftJoin('enderecos','enderecos.cd_endereco','=','registros.cd_endereco')
                    ->leftJoin('cidades', 'cidades.cd_cidade', '=','enderecos.cd_cidade')
                    ->leftJoin('estados', 'estados.cd_estado', '=','cidades.cd_estado')
                    ->leftJoin('pais','estados.cd_pais','=','pais.cd_pais')
                    ->select(
                        'registros.cd_registro as cdRegistroExterno',
                        'registros.ind_registro as indTipoRegistro',
                        'pessoas.ind_sexo as sexo',
                        'registros.ds_nome as dsNome',
                        'alunos.cd_aluno',
                        'alunos.nr_peso as nrPeso',
                        'alunos.cm_altura as cmAltura',
                        'pessoas.dt_nascimento as sdtNascimento',
                        'alunos.ind_condicao_fisica as indCondicaoFisica',
                        'alunos.ind_tipo_treino as indTipoTreino',
                        'pessoas.ind_sexo as masculino',
                        'enderecos.ds_cidade as dsCidade',
                        'enderecos.ds_ut as dsEstado',
                        'pais.ds_pais as dsPais',
                        'pais.ds_codigo as dsCodigoPais'
                    )
                    ->first();

                if ($aluno)
                {
                    $aluno['sexo'] = $aluno['sexo'] == 1 ? true : false;
                    $aluno['masculino'] = $aluno['masculino'] === 1 ? true : false;

                    $aluno['endereco'] = [
                        'dsCidade' => $aluno['dsCidade'],
                        'dsUf' => $aluno['dsEstado'],
                        'dsPais' => $aluno['dsPais'],
                        'dsCodigoPais' => $aluno['dsCodigoPais']
                    ];

                    if (isset($aluno['cmAltura']) && !empty($aluno['cmAltura']))
                    {
                        $aluno['fgCadastroFinalizado'] = true;
                    }
                    else {
                        $aluno['fgCadastroFinalizado'] = false;
                    }

                    $aluno['token'] = $token;
                    return Response($aluno, 200);
                }

                return Response(['msg' =>'Aluno não encontrado'], 404);
            }
            else
            {
                $professor = Registro::
                    where('registros.cd_registro', $login['cd_registro'])
                    ->where('registros.ind_registro', $indTipoRegistro)
                    ->leftJoin('pessoas','pessoas.cd_registro','=','registros.cd_registro')
                    ->leftJoin('professores','professores.cd_pessoa','=','pessoas.cd_pessoa')
                    ->leftJoin('enderecos','enderecos.cd_endereco','=','registros.cd_endereco')
                    ->leftJoin('cidades', 'cidades.cd_cidade', '=','enderecos.cd_cidade')
                    ->leftJoin('estados', 'estados.cd_estado', '=','cidades.cd_estado')
                    ->leftJoin('pais','estados.cd_pais','=','pais.cd_pais')
                    ->select(
                        'registros.cd_registro as cdRegistroExterno',
                        'registros.ind_registro as indTipoRegistro',
                        'pessoas.ind_sexo as sexo',
                        'registros.ds_nome as dsNome',
                        'professores.cd_professor',
                        'professores.nr_valor_mensal as nrValorMensal',
                        'professores.ds_cref as dsCref',
                        'professores.dt_cref as dtCref',
                        'pessoas.dt_nascimento as sdtNascimento',
                        'pessoas.ind_sexo as masculino',
                        'enderecos.ds_cidade as dsCidade',
                        'enderecos.ds_ut as dsEstado',
                        'pais.ds_pais as dsPais',
                        'pais.ds_codigo as dsCodigoPais'
                    )
                    ->first();

                if ($professor)
                {
                    $professor['sexo'] = ($professor['sexo'] === 'true') ? true : false;
                    $professor['masculino'] = ($professor['masculino'] === '1') ? true : false;
                    $professor['endereco'] = [
                        'dsCidade' => $professor['dsCidade'],
                        'dsUf' => $professor['dsEstado'],
                        'dsPais' => $professor['dsPais'],
                        'dsCodigoPais' => $professor['dsCodigoPais']
                    ];

                    if (isset($professor['dsCref']) && !empty($professor['dsCref']))
                    {
                        $professor['fgCadastroFinalizado'] = true;
                    }
                    else
                    {
                        $professor['fgCadastroFinalizado'] = false;
                    }

                    $professor['token'] = $token;
                    return Response($professor, 200);
                }
                else
                {
                    return Response(['msg' => 'Professor não encontrado'], 404);
                }
            }
        }
        catch(\Exception $e)
        {
            return Response(
                [
                    'msg' => 'Erro ao fazer login',
                    'erro' => $e->getMessage()
                ],
                500
            );
        }
    }

    public function loginV2()
    {
        $data = Input::all();
        $rules = [
            'email' => 'required|string',
            'senha' => 'required|string',
            'indTipoRegistro' => 'integer',
        ];

        $validation = Validator::make($data, $rules);
        if ($validation->fails())
        {
            return Response(
                [ 'msg' => $validation->getMessageBag()->first() ],
                400
            );
        }

        $indTipoRegistro = isset($data['indTipoRegistro']) ? $data['indTipoRegistro'] : 1;
        if ($indTipoRegistro !== 1 && $indTipoRegistro !== 2)
        {
            $indTipoRegistro = 1;
        }

        try
        {
            $login = Login::
                where('ds_email', $data['email'])
                ->first();

            if (!$login)
            {
                return Response(['msg' =>'Email não cadastrado'], 404);
            }
            else if ($login['ds_senha'] != $data['senha'])
            {
                return Response(['msg' =>'Senha incorreta'], 401);
            }

            $token = JWTAuth::fromUser($login);
            $last_date_update_exercise = DB::
                table('config')
                ->value('last_date_update_exercise');

            $response = null;

            if ($indTipoRegistro === 1)
            {
                $aluno = Registro::
                    where('registros.cd_registro', $login['cd_registro'])
                    ->where('registros.ind_registro', $indTipoRegistro)
                    ->leftJoin('pessoas','pessoas.cd_registro','=','registros.cd_registro')
                    ->leftJoin('alunos','alunos.cd_pessoa','=','pessoas.cd_pessoa')
                    ->leftJoin('enderecos','enderecos.cd_endereco','=','registros.cd_endereco')
                    ->leftJoin('cidades', 'cidades.cd_cidade', '=','enderecos.cd_cidade')
                    ->leftJoin('estados', 'estados.cd_estado', '=','cidades.cd_estado')
                    ->leftJoin('pais','estados.cd_pais','=','pais.cd_pais')
                    ->leftJoin('empresa','empresa.id','=','pessoas.cd_empresa')
                    ->select(
                        'registros.cd_registro as cdRegistroExterno',
                        'registros.ind_registro as indTipoRegistro',
                        'registros.ds_avatar as dsAvatar',
                        'pessoas.ind_sexo as sexo',
                        'pessoas.cd_pessoa as cdPessoa',
                        'registros.ds_nome as dsNome',
                        'alunos.cd_aluno',
                        'alunos.nr_peso as nrPeso',
                        'alunos.cm_altura as cmAltura',
                        'pessoas.dt_nascimento as sdtNascimento',
                        'alunos.ind_condicao_fisica as indCondicaoFisica',
                        'alunos.ind_tipo_treino as indTipoTreino',
                        'pessoas.ind_sexo as masculino',
                        'enderecos.ds_cidade as dsCidade',
                        'enderecos.ds_ut as dsEstado',
                        'pais.ds_pais as dsPais',
                        'pais.ds_codigo as dsCodigoPais',
                        'empresa.id AS cdEmpresa',
                        'empresa.nome as dsNomeEmpresa'
                    )
                    ->first();

                if (!$aluno)
                {
                    return Response(['msg' =>'Aluno não encontrado'], 404);
                }

                $aluno['sexo'] = ($aluno['sexo'] == 1) ? true : false;
                $aluno['masculino'] = $aluno['masculino'] === 1 ? true : false;
                $aluno['endereco'] = [
                    'dsCidade' => $aluno['dsCidade'],
                    'dsUf' => $aluno['dsEstado'],
                    'dsPais' => $aluno['dsPais'],
                    'dsCodigoPais' => $aluno['dsCodigoPais']
                ];

                if (isset($aluno['cmAltura']) && !empty($aluno['cmAltura']))
                {
                    $aluno['fgCadastroFinalizado'] = true;
                }
                else {
                    $aluno['fgCadastroFinalizado'] = false;
                }

                if ($aluno['cdEmpresa'])
                {
                    $aluno['gym'] = [
                        'id' => $aluno['cdEmpresa'],
                        'name' => $aluno['dsNomeEmpresa']
                    ];
                }

                unset($aluno['cdEmpresa']);
                unset($aluno['dsNomeEmpresa']);

                $aluno['token'] = $token;
                $response = [
                    'user' => $aluno,
                    'last_date_update_exercise' => $last_date_update_exercise
                ];

            }
            else
            {
                $professor = Registro::
                    where('registros.cd_registro', $login['cd_registro'])
                    ->where('registros.ind_registro', $indTipoRegistro)
                    ->leftJoin('pessoas','pessoas.cd_registro','=','registros.cd_registro')
                    ->leftJoin('professores','professores.cd_pessoa','=','pessoas.cd_pessoa')
                    ->leftJoin('enderecos','enderecos.cd_endereco','=','registros.cd_endereco')
                    ->leftJoin('cidades', 'cidades.cd_cidade', '=','enderecos.cd_cidade')
                    ->leftJoin('estados', 'estados.cd_estado', '=','cidades.cd_estado')
                    ->leftJoin('pais','estados.cd_pais','=','pais.cd_pais')
                    ->leftJoin('empresa','empresa.id','=','pessoas.cd_empresa')
                    ->select(
                        'registros.cd_registro as cdRegistroExterno',
                        'registros.ind_registro as indTipoRegistro',
                        'registros.ds_avatar as dsAvatar',
                        'pessoas.ind_sexo as sexo',
                        'pessoas.cd_pessoa as cdPessoa',
                        'registros.ds_nome as dsNome',
                        'professores.cd_professor',
                        'professores.nr_valor_mensal as nrValorMensal',
                        'professores.ds_cref as dsCref',
                        'professores.dt_cref as dtCref',
                        'pessoas.dt_nascimento as sdtNascimento',
                        'pessoas.ind_sexo as masculino',
                        'enderecos.ds_cidade as dsCidade',
                        'enderecos.ds_ut as dsEstado',
                        'pais.ds_pais as dsPais',
                        'pais.ds_codigo as dsCodigoPais',
                        'empresa.id AS cdEmpresa',
                        'empresa.nome as dsNomeEmpresa'
                    )
                    ->first();


                if (!$professor)
                {
                    return Response(['msg' => 'Professor não encontrado'], 404);
                }

                $professor['sexo'] = ($professor['sexo'] === 'true') ? true : false;
                $professor['masculino'] = ($professor['masculino'] === '1') ? true : false;
                $professor['endereco'] = [
                    'dsCidade' => $professor['dsCidade'],
                    'dsUf' => $professor['dsEstado'],
                    'dsPais' => $professor['dsPais'],
                    'dsCodigoPais' => $professor['dsCodigoPais']
                ];

                if (isset($professor['dsCref']) && !empty($professor['dsCref']))
                {
                    $professor['fgCadastroFinalizado'] = true;
                }
                else
                {
                    $professor['fgCadastroFinalizado'] = false;
                }

                if ($professor['cdEmpresa'])
                {
                    $professor['gym'] = [
                        'id' => $professor['cdEmpresa'],
                        'name' => $professor['dsNomeEmpresa']
                    ];
                }
                unset($professor['cdEmpresa']);
                unset($professor['dsNomeEmpresa']);

                $professor['token'] = $token;
                $response = [
                    'user' => $professor,
                    'last_date_update_exercise' => $last_date_update_exercise
                ];
            }

            if (!isset($response)) {
                return Response([ 'msg' => 'Error', 500 ]);
            }

            $cd_pessoa = $response['user']['cdPessoa'];
            $countNewFriends = DB::
                select("
                        SELECT
                            COUNT(1) AS count
                        FROM
                            amizades
                        WHERE
                            fg_aceito <> 1 AND
                            cd_to = ?
                    ",
                    [ $cd_pessoa ]
                );

            $response['user']['friendship_notifications'] = $countNewFriends[0]->count;

            return Response($response, 200);

        }
        catch(\Exception $e) {
            return Response(
                [
                    'msg' => 'Erro ao fazer login',
                    'erro' => $e->getMessage()
                ],
                500
            );
        }
    }
}

