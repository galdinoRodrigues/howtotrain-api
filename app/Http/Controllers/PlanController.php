<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Input;
use App\Login;
use App\Plano;
use App\Aluno;
use App\Treino;
use DB;
use Auth;
use Carbon\Carbon;

class PlanController extends Controller {
    public function create() {
        $data = Input::all();
        $rules = [
            'cd_plano' => 'integer',
            'name' => 'required|string',
            'description' => 'string',
            'nr_validade_dias' => 'integer',
            'ind_tipo_treino' => 'integer',
            'ind_nivel_treino' => 'integer',
            'ind_sexo' => 'integer',
            'cd_aluno' => 'integer'
        ];
        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            return Response([
                'msg'=> $validation->getMessageBag()->first()
            ], 400);
        }

        $jwtLogin = Auth::user();
        $login = Login::
            with(
                'registro.pessoa.professor',
                'registro.pessoa.aluno'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa || !$login->registro->pessoa->professor) {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }

        $cd_professor = $login->registro->pessoa->professor->cd_professor;
        $cdPlano = 0;
        if(isset($data['cd_plano'])){
            $cdPlano = $data['cd_plano'];
            Plano::
                where('cd_plano', $cdPlano)
                ->update([
                    'ds_nome' => $data['name'],
                    'ds_plano' => $data['description'] ?? null,
                    'nr_validade_dias' => $data['nr_validade_dias'] ?? null,
                    'ind_tipo_treino' => $data['ind_tipo_treino'] ?? null,
                    'ind_nivel_treino' => $data['ind_nivel_treino'] ?? null,
                    'ind_sexo' => $data['ind_sexo'] ?? null,
                    'cd_professor' => $cd_professor
                ]);
        }
        else{
            $cdPlano = Plano::
            insertGetId([
                'ds_nome' => $data['name'],
                'ds_plano' => $data['description'] ?? null,
                'nr_validade_dias' => $data['nr_validade_dias'] ?? null,
                'ind_tipo_treino' => $data['ind_tipo_treino'] ?? null,
                'ind_nivel_treino' => $data['ind_nivel_treino'] ?? null,
                'ind_sexo' => $data['ind_sexo'] ?? null,
                'cd_professor' => $cd_professor
            ]);
        }

        if(isset($data['cd_aluno']) && isset($cdPlano) && $data['cd_aluno'] != 0){
            $cd_aluno = $data['cd_aluno'];
            
            $cdPlano = $this->attachPlanToStudent2($cdPlano, $cd_aluno);
        }

        return Response([ 'cd_plano' => $cdPlano ], 200);
    }

    public function attachTrain() {
        $data = Input::all();
        $rules = [
            'cd_plano' => 'required|integer|exists:planos',
            'cd_treino' => 'required|integer|exists:treinos'
        ];

        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            return Response([
                'msg'=> $validation->getMessageBag()->first()
            ], 400);
        }

        $jwtLogin = Auth::user();
        $login = Login::
            with(
                'registro.pessoa.professor',
                'registro.pessoa.aluno'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa || !$login->registro->pessoa->professor) {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }

        $train = Treino::
            where([
                'cd_treino' => $data['cd_treino'],
                'cd_professor' => $login->registro->pessoa->professor->cd_professor
            ])
            ->first();
        $plan = Plano::
            where([
                'cd_plano' => $data['cd_plano'],
                'cd_professor' => $login->registro->pessoa->professor->cd_professor
            ])
            ->first();

        if (!$plan || !$plan->cd_plano) {
            return Response([ 'msg' => 'Plano informado não é seu' ], 401);
        }
        if (!$train || !$train->cd_treino) {
            return Response([ 'msg' => 'Treino informado não é seu' ], 401);
        }

        $rel = DB::
            table('plano_treino')
            ->where('cd_plano', $data['cd_plano'])
            ->where('cd_treino', $data['cd_treino'])
            ->value('cd_plano');

        if (count($rel) > 0) {
            return Response([ 'msg' => 'Treino já atrelado ao plano' ], 401);
        }

        $plan->trains()->attach($data['cd_treino']);

        return Response([ 'msg' => 'SUCCESS' ], 200);
    }

    public function listFromStudent2(){
        $data = Input::all();
        $rules = [
            'query' => 'string|max:24'
        ];
        $validation = Validator::make($data, $rules);

        $jwtLogin = Auth::user();
        $login = Login::
            with(
                'registro.pessoa.professor',
                'registro.pessoa.aluno'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa || !$login->registro->pessoa->aluno) {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }

        try {
            $args = [
                $login->registro->pessoa->aluno->cd_aluno
            ];

            $plans = DB::
            select("
                SELECT 
                    p.cd_plano,
                    p.ind_nivel_treino,
                    p.ind_tipo_treino,
                    ap.dt_expiracao,
                    p.ds_nome as name,
                    p.nr_validade_dias
                FROM planos p
                INNER JOIN aluno_plano ap ON ap.cd_plano  =  p.cd_plano 
                INNER JOIN alunos a ON a.cd_aluno = ap.cd_aluno 
                WHERE a.cd_aluno = ?
                ORDER BY p.cd_plano DESC
            ;",
            $args
            );
            $aluno["plans"] = $plans;
            return Response([ 'plans' =>  $plans ]);
        } catch(\Exception $e) {
            return Response([
                'msg' => 'Erro ao consultar os planos',
                'error' => $e
            ], 400);
        }
    }

    public function listFromTeacher() {
        $data = Input::all();
        $rules = [
            'query' => 'string|max:24'
        ];
        $validation = Validator::make($data, $rules);

        $jwtLogin = Auth::user();
        $login = Login::
            with(
                'registro.pessoa.professor',
                'registro.pessoa.aluno'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa || !$login->registro->pessoa->professor) {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }

        $cd_professor = $login->registro->pessoa->professor->cd_professor;

        $where = '';
        $args = [ $cd_professor ];
        if(array_key_exists('query', $data) && $data['query']) {
            $args = [ '%'.$data['query'].'%', '%'.$data['query'].'%', $cd_professor ];
            $where = '( ds_nome LIKE ? OR ds_plano LIKE ?) AND';
        }

        try {
            $plans = DB::select("
                SELECT
                    cd_plano,
                    ds_nome AS name,
                    ds_plano AS description,
                    ind_tipo_treino,
                    ind_nivel_treino,
                    nr_validade_dias
                FROM
                    planos
                WHERE
                    " . $where . "
                    cd_professor = ?
                    AND cd_plano_original IS NULL
                ORDER BY
                    cd_plano DESC
                ",
                $args
            );
            return Response([ 'plans' =>  $plans ]);
        } catch(\Exception $e) {
            return Response([
                'msg' => 'Erro ao consultar os planos',
                'error' => $e
            ], 400);
        }
    }

    public function detailFromStudent($cd_plano) {
        if (!$cd_plano) {
            return Response([ 'msg' => 'Informe o plano!' ], 404);
        }

        $jwtLogin = Auth::user();
        $login = Login::
            with(
                'registro.pessoa.professor',
                'registro.pessoa.aluno'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa) {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }

        try {
            $plan = Plano::find($cd_plano);
            if (!isset($plan)) {
                return Response([ 'msg' => 'Plano não encontrado!' ], 404);
            }

            $result = DB::select("
                SELECT
                    g.cd_grupo AS cd_grupo,
                    g.ds_nome AS group_name,
                    t.cd_treino,
                    t.ds_nome,
                    t.description AS description,
                    t.ind_tipo_treino,
                    t.ind_nivel_treino

                FROM planos p 
                INNER JOIN aluno_plano ap ON ap.cd_plano = p.cd_plano 
                INNER JOIN plano_treino pt ON pt.cd_plano = p.cd_plano 
                INNER JOIN treinos t ON t.cd_treino  = pt.cd_treino 
                INNER JOIN grupos g ON g.cd_grupo = t.cd_grupo
                WHERE p.cd_plano = ?
                ORDER BY g.ds_nome ASC
            ",
            [$cd_plano]);

            if (count($result) > 0) {
                $train_list = [];
                foreach ($result as $t) {
                    $train_list[] = $t;
                }

                $exercisesResult = DB::select("
                SELECT
                    g.cd_grupo,
                    te.cd_exercicio,
                    te.divider
                FROM
                    grupos g
                INNER JOIN
                    treinos AS t ON g.cd_grupo = t.cd_grupo
                        AND t.cd_treino in (
                            SELECT pt.cd_treino FROM plano_treino AS pt
                            where  pt.cd_plano  = ?)
                INNER JOIN treino_exercicio te ON te.cd_treino = t.cd_treino
                ",
                [$cd_plano]);

                if (count($exercisesResult) > 0) {
                    foreach($train_list as $t){
                        $exercise_list = [];
                        foreach ($exercisesResult as $e) {
                            if ($t->cd_grupo == $e->cd_grupo) {
                                $exercise_list[] = [
                                    'cd_exercicio' => $e->cd_exercicio,
                                    'divider' => $e->divider
                                ];
                            }
                        }
                        if (count($exercise_list) > 0) {
                            $t->exercises =$exercise_list;
                        }
                    }
                }


                $plan->training_by_groups = $train_list;
            }

            return Response([ 'plan' =>  $plan ]);
        } catch(\Exception $e) {
            return Response([
                'msg' => 'Erro ao consultar o plano',
                'error' => $e
            ], 400);
        }
    }

    public function detailFromTeacher($cd_plano) {
        if (!$cd_plano) {
            return Response([ 'msg' => 'Informe o plano!' ], 404);
        }

        $jwtLogin = Auth::user();
        $login = Login::
            with(
                'registro.pessoa.professor',
                'registro.pessoa.aluno'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa || !$login->registro->pessoa->professor) {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }
        $cd_professor = $login->registro->pessoa->professor->cd_professor;

        $plan = Plano::find($cd_plano);
        if (!isset($plan)) {
            return Response([ 'msg' => 'Plano não encontrado!' ], 404);
        }

        // if ($plan->cd_professor != $cd_professor) {
        //     return Response([ 'msg' => 'Este plano é de outro usuário!' ], 401);
        // }

        $result = DB::select("
            SELECT
                g.cd_grupo AS cd_grupo,
                g.ds_nome AS group_name,
                t.cd_treino,
                t.ds_nome,
                t.description AS description,
                t.ind_tipo_treino,
                t.ind_nivel_treino

            FROM
                grupos g
            LEFT JOIN
                treinos AS t
                    ON g.cd_grupo = t.cd_grupo
                    and t.cd_treino in (
                        SELECT pt.cd_treino FROM plano_treino AS pt
                        where  pt.cd_plano  = ?)
        ",
        [$cd_plano]);

        if (count($result) > 0) {
            $train_list = [];
            foreach ($result as $t) {
                $train_list[] = $t;
            }

            $exercisesResult = DB::select("
            SELECT
                e.ds_nome,
                g.cd_grupo,
                te.cd_exercicio,
                te.divider,
                te.nr_ordem,
                te.nr_repeticoes,
                te.ds_exercicio_professor
            FROM
                grupos g
            INNER JOIN
                treinos AS t ON g.cd_grupo = t.cd_grupo
                    AND t.cd_treino in (
                        SELECT pt.cd_treino FROM plano_treino AS pt
                        where  pt.cd_plano  = ?)
            INNER JOIN treino_exercicio te ON te.cd_treino = t.cd_treino
            INNER JOIN exercicios e ON e.cd_exercicio = te.cd_exercicio 
            ",
            [$cd_plano]);

            if (count($exercisesResult) > 0) {
                foreach($train_list as $t){
                    $exercise_list = [];
                    foreach ($exercisesResult as $e) {
                        if ($t->cd_grupo == $e->cd_grupo) {
                            $exercise_list[] = [
                                'cd_exercicio' => $e->cd_exercicio,
                                'divider' => $e->divider,
                                'ds_nome' => $e->ds_nome,
                                'nr_ordem' => $e->nr_ordem,
                                'nr_repeticoes' => $e->nr_repeticoes,
                                'ds_exercicio_professor' => $e->ds_exercicio_professor
                            ];
                        }
                    }
                    if (count($exercise_list) > 0) {
                        $t->exercises =$exercise_list;
                    }
                }
            }

            $plan->training_by_groups = $train_list;
        }

        return Response([ 'plan' =>  $plan ]);
    }

    public function attachPlanToStudent() {
        $data = Input::all();
        $rules = [
            'cd_plano' => 'required|integer|exists:planos',
            'cd_aluno' => 'required|integer|exists:alunos'
        ];

        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            return Response([
                'msg'=> $validation->getMessageBag()->first()
            ], 400);
        }

        $jwtLogin = Auth::user();
        $login = Login::where('cd_login', $jwtLogin->cd_login)->first();
        if (!$login || !$login->registro || !$login->registro->pessoa || !$login->registro->pessoa->professor) {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }
        $cd_aluno = $data['cd_aluno'];
        $cd_plano = $data['cd_plano'];
        $this->attachPlanToStudent2($cd_plano, $cd_aluno);
        
        return Response([ 'msg' => 'SUCCESS' ], 200);
    }

    function attachPlanToStudent2($cd_plano, $cd_aluno){
        $jwtLogin = Auth::user();
        $login = Login::where('cd_login', $jwtLogin->cd_login)->first();
        $cd_professor = $login->registro->pessoa->professor->cd_professor;
        

        $aluno = Aluno::find($cd_aluno);
        $plan = Plano::
            where([
                'cd_plano' => $cd_plano,
                'cd_professor' => $cd_professor
            ])
            ->first();

        if (!$plan || !$plan->cd_plano) {
            return Response([ 'msg' => 'Plano informado não é seu' ], 401);
        }
        

        // Is he my friend
        // $areFriends = DB::select("
        //         SELECT
        //             1
        //         FROM
        //             amizades
        //         WHERE
        //             fg_aceito = 1 AND (
        //                 (cd_from = ? AND cd_to = ?) OR
        //                 (cd_from = ? AND cd_to = ?)
        //             )
        //         ;",
        //         [
        //             $login->registro->pessoa->cd_pessoa,
        //             $aluno->pessoa->cd_pessoa,
        //             $aluno->pessoa->cd_pessoa,
        //             $login->registro->pessoa->cd_pessoa
        //         ]
        //     );

        // if (!isset($areFriends) || count($areFriends) == 0) {
        //     return Response([ 'msg' => 'Você não é amigo deste aluno!' ], 401);
        // }

        // tirado temporariamente
        // foreach ($aluno->planos as $plano) {
        //     if ($plano->cd_professor == $cd_professor) {
        //         return Response([ 'msg' => 'Este aluno já possuí um plano seu!' ], 400);
        //     }
        // }

        // Desabilita todos os treinos já existentes
        DB::
        table('planos')
        ->join('aluno_plano','aluno_plano.cd_plano','=','planos.cd_plano')
        ->where('aluno_plano.cd_aluno', $aluno->cd_aluno)
        ->update([
            'active' =>0
        ]);

        $new_plan = $plan->replicate();
        $new_plan->active = 1;
        $new_plan->save();
        $new_plan->planoOriginal()->associate($plan);
        $new_plan->save();

        $plan->load('trains');
        foreach ($plan->trains as $train) {
            $new_train = $train->replicate();
            $new_train->save();
            $new_plan->trains()->attach($new_train->cd_treino);

            // carrega a tabela: treino_exercicio
            $exercises_training = DB::select("
                SELECT
                    nr_ordem,
                    cd_exercicio,
                    nr_repeticoes,
                    ds_exercicio_professor,
                    divider
                FROM
                    treino_exercicio
                WHERE cd_treino  = ?
                    ;",
                    [
                        $train->cd_treino,
                    ]
                );
            
            foreach ($exercises_training as $exercise_training) {
                DB::
                    table('treino_exercicio')
                    ->insert([
                        'nr_ordem' => $exercise_training->nr_ordem,
                        'cd_exercicio' => $exercise_training->cd_exercicio,
                        'nr_repeticoes' => $exercise_training->nr_repeticoes,
                        'ds_exercicio_professor' => $exercise_training->ds_exercicio_professor,
                        'cd_treino' => $new_train->cd_treino,
                        'divider' => $exercise_training->divider
                    ]);
            }
        }

        DB::
            table('aluno_plano')
            ->insert([
                'dt_inclusao' => Carbon::now(),
                'dt_expiracao' => Carbon::now()->addDays($new_plan->nr_validade_dias ?? 30),
                'cd_plano' => $new_plan->cd_plano,
                'cd_aluno' => $aluno->cd_aluno
            ]);

        return $new_plan->cd_plano;
    }
}
