<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Login;

class LoginController extends Controller
{
    public function gerarCodigo()
    {
        $data = Input::all();
        $rules = [
            'email' => 'required|email'
        ];

        $validation = Validator::make($data,$rules);
        if ($validation->fails())
        {
            return Response(
                [ 'msg' => $validation->getMessageBag()->first() ],
                400
            );
        }

        //Verify if email exists
        try
        {
            $user = Login::
                where('ds_email', $data['email'])
                ->first();
        }
        catch (\Exception $e)
        {
            return Response(
                [
                    'msg' => 'Erro interno',
                    'error' => $e
                ],
                400
            );
        }

        if (!$user)
        {
            return Response([ 'msg' => 'Usuário não existe' ], 404);
        }

        $upper = implode('', range('A', 'Z'));
        $lower = implode('', range('a', 'z'));
        $nums = implode('', range(0, 9));

        $alphaNumeric = $upper . $lower . $nums;
        $string = '';
        $len = 6;
        for ($i = 0; $i < $len; $i++)
        {
            $string .= $alphaNumeric[ rand(0, strlen($alphaNumeric) - 1) ];
        }
        $codigo = $string;

        try
        {
            Login::
                where('ds_email', $data['email'])
                ->update(['ds_codigo' => $codigo]);
        }
        catch (\Exception $e)
        {
            return Response([ 'msg' => 'Não foi possivel gerar o codigo' ], 400);
        }

        try
        {
            $email = new \SendGrid\Mail\Mail();
            $email->setFrom('senha@howtotrain.com.br', 'How To Train');
            $email->setSubject('Renovar Senha');
            $email->addTo($data['email']);
            $email->addContent(
                'text/plain',
                'Codigo para Mudar Senha ' . $codigo
            );
            $email->addContent(
                'text/html',
                '<html><center>Codigo para Mudar Senha ' . $codigo . ' </center></html>'
            );
            $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
            $response = $sendgrid->send($email);
            return Response(['msg' => 'SUCCESS'], 200);
        }
        catch (Exception $e)
        {
            return Response(
                [
                    'msg' => 'Não foi possível enviar o e-mail! Tente novamente.',
                    'error' => $e
                ],
                200
            );
        }
    }

    public function validarSenha()
    {
        $data = Input::all();
        $rules = [
            'email' => 'required|string',
            'codigo' => 'required|string',
            'senha' => 'required|string'
        ];

        $validation = Validator::make($data,$rules);
        if ($validation->fails())
        {
            return Response(
                [ 'msg' => $validation->getMessageBag()->first() ],
                400
            );
        }

        try
        {
            $login = Login::
                where('ds_codigo', $data['codigo'])
                ->first();

            if (count($login) !== 0)
            {
                Login::
                    where('ds_codigo',$data['codigo'])
                    ->update([
                        'ds_senha' => $data['senha'],
                        'ds_codigo' => NULL
                    ]);

                return Response([ 'msg' => 'SUCCESS' ], 200);
            }

            return Response([ 'msg' => 'Codigo é invalido' ], 403);
        }
        catch(\Exception $e)
        {
            return Response(
                [
                    'msg' => 'Problema ao verificar token',
                    'erro' => $e->getMessage()
                ],
                500
            );
        }
    }
}
