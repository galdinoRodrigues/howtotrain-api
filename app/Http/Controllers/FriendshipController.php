<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Pessoa;
use App\Professor;
use App\Aluno;
use App\Login;
use JWTAuth;
use DB;
use Auth;
use Carbon\Carbon;

class FriendshipController extends Controller
{
    function listSentRequests()
    {
        $jwtLogin = Auth::user();

        $login = Login::
            with(
                'registro',
                'registro.pessoa'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa)
        {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }

        $friendRequests = DB::
            select("
                SELECT
                    am.cd_amizade,
                    r.ds_nome,
                    r.ds_avatar,
                    a.cd_aluno,
                    pr.cd_professor,
                    am.dt_inclusao as dt_requested,
                    IF(pr.cd_professor IS NULL, 1, 2) AS indTipoRegistro
                FROM
                    amizades AS am
                INNER JOIN
                    pessoas AS p
                        ON p.cd_pessoa = am.cd_to
                INNER JOIN
                    registros AS r
                        ON r.cd_registro = p.cd_registro
                LEFT JOIN
                    alunos AS a
                        ON a.cd_pessoa = p.cd_pessoa
                LEFT JOIN
                    professores AS pr
                        ON pr.cd_pessoa = p.cd_pessoa
                WHERE
                    am.fg_aceito <> 1 AND
                    am.cd_from = ?
                ORDER BY
                    dt_requested DESC
                LIMIT 50;
                ",
                [ $login->registro->pessoa->cd_pessoa ]
            );
        return Response([
            'sent' => $friendRequests
        ]);
    }

    function listFriends()
    {
        $data = Input::all();
        $rules = [
            'pageIndex' => 'integer',
            'pageSize' => 'integer'
        ];
        $validation = Validator::make($data, $rules);

        $jwtLogin = Auth::user();

        $login = Login::
            with(
                'registro',
                'registro.pessoa'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa)
        {
            return Response(
                [ 'msg' => 'Não autorizado' ],
                401
            );
        }

        $pageIndex = $data['pageIndex'] ?? 1;
        $pageSize = $data['pageSize'] ?? 10;

        // All attributes come as string when passed as queryParams
        $pageIndex = (int) $pageIndex;
        $pageSize = (int) $pageSize;

        $from = ($pageIndex - 1) * $pageSize;
        // validação para nao trazer quando ja tem plano de treino, comentado por hora, talvez nao faça sentido ter
        // pois se um prof ja enviou treino, e depois eles desfizerem a amizade, vai continuar trazendo na apa de alunos, 
        // mas ele não vai conseguir enviar treinos mais
        // AND ( 
        //     (
        //            SELECT distinct 1
        //         FROM aluno_plano ap2 
        //         INNER JOIN planos p2 ON p2.cd_plano = ap2.cd_plano 
        //         INNER JOIN alunos a2 ON a2.cd_aluno = ap2.cd_aluno 
        //         WHERE a2.cd_pessoa = ? AND ap2.cd_aluno = a.cd_aluno
        //     ) IS NULL
        //     AND
        //     (
        //            SELECT distinct 1
        //         FROM aluno_plano ap2 
        //         INNER JOIN planos p2 ON p2.cd_plano = ap2.cd_plano 
        //         INNER JOIN professores p3 ON p3.cd_professor = p2.cd_professor 
        //         WHERE p3.cd_pessoa = ? AND ap2.cd_aluno = a.cd_aluno
        //     ) IS NULL
        // )
        $friends = DB::
            select("
                SELECT
                    am.cd_amizade,
                    r.ds_nome,
                    r.ds_avatar,
                    a.cd_aluno,
                    pr.cd_professor,
                    am.dt_inclusao as dt_requested,
                    am.dt_alteracao as dt_accepted,
                    IF(pr.cd_professor IS NULL, 1, 2) AS indTipoRegistro
                FROM
                    amizades AS am
                INNER JOIN
                    pessoas AS p
                        ON p.cd_pessoa = IF(am.cd_from = ?, am.cd_to, am.cd_from)
                INNER JOIN
                    registros AS r
                        ON r.cd_registro = p.cd_registro
                LEFT JOIN
                    alunos AS a
                        ON a.cd_pessoa = p.cd_pessoa
                LEFT JOIN
                    professores AS pr
                        ON pr.cd_pessoa = p.cd_pessoa
                WHERE
                    am.fg_aceito = 1 AND
                    (
                        am.cd_to = ? OR
                        am.cd_from = ?
                    )
                ORDER BY
                    dt_accepted DESC
                LIMIT ? OFFSET ?;
                ",
                [
                    $login->registro->pessoa->cd_pessoa,
                    $login->registro->pessoa->cd_pessoa,
                    $login->registro->pessoa->cd_pessoa,
                    $pageSize,
                    $from
                ]
            );

        $count = DB::
            select("
                SELECT
                   COUNT(1)
                FROM
                    amizades AS am
                INNER JOIN
                    pessoas AS p
                        ON p.cd_pessoa = IF(am.cd_from = ?, am.cd_to, am.cd_from)
                INNER JOIN
                    registros AS r
                        ON r.cd_registro = p.cd_registro
                LEFT JOIN
                    alunos AS a
                        ON a.cd_pessoa = p.cd_pessoa
                LEFT JOIN
                    professores AS pr
                        ON pr.cd_pessoa = p.cd_pessoa
                WHERE
                    am.fg_aceito = 1 AND
                    (
                        am.cd_to = ? OR
                        am.cd_from = ?
                    )
                    ;
                ",
                [
                    $login->registro->pessoa->cd_pessoa,
                    $login->registro->pessoa->cd_pessoa,
                    $login->registro->pessoa->cd_pessoa
                ]
            );

        $total = 0;
        $totalPages = 0;
        if ($count && count($count) > 0)
        {
            $total = $count[0]->count ?? 0;
            $totalPages = ceil($total / $pageSize);
        }
        return Response([
            'friends' => $friends,
            'pageCount' => $totalPages,
            'count' => $total
        ]);
    }

    function create()
    {
        $data = Input::all();
        $rules = [
            'id' => 'required|integer',
            'indTipoRegistro' => 'required|integer',
        ];
        $validation = Validator::make($data, $rules);
        if ($validation->fails())
        {
            return Response(
                [ 'msg'=> $validation->getMessageBag()->first() ],
                400
            );
        }

        if ($data['indTipoRegistro'] !== 1 && $data['indTipoRegistro'] !== 2)
        {
            return Response(
                [ 'msg'=> 'Tipo de registro inválido!' ],
                400
            );
        }

        $jwtLogin = Auth::user();
        $login = Login::
            with(
                'registro.pessoa.professor',
                'registro.pessoa.aluno'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa)
        {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }

        if (
            (
                $login->registro->pessoa->professor && $data['indTipoRegistro'] == 2 &&
                $login->registro->pessoa->professor->cd_professor == $data['id']
            ) ||
            (
                $login->registro->pessoa->aluno && $data['indTipoRegistro'] == 1 &&
                $login->registro->pessoa->aluno->cd_aluno == $data['id']
            )
        ) {
            return Response(
                [ 'msg'=> 'Você não pode ser seu próprio amigo!' ],
                400
            );
        }

        $pessoa = null;
        if ($data['indTipoRegistro'] == 1)
        {
            $pessoa = DB::
                table('alunos')
                ->where('cd_aluno', $data['id'])
                ->value('cd_pessoa');
        }
        else
        {
            $pessoa = DB::
                table('professores')
                ->where('cd_professor', $data['id'])
                ->value('cd_pessoa');
        }

        if (!$pessoa)
        {
            return Response([ 'msg'=> 'Usuário não encontrado!' ], 404);
        }

        $args = [
            $login->registro->pessoa->cd_pessoa,
            $pessoa,
            $pessoa,
            $login->registro->pessoa->cd_pessoa
        ];
        $areFriends = DB::select("
            SELECT
                1
            FROM
                amizades
            WHERE
                (cd_from = ? AND cd_to = ?) OR
                (cd_from = ? AND cd_to = ?)
            ;",
            $args
        );

        if (isset($areFriends) && count($areFriends) > 0)
        {
            return Response(
                [ 'msg'=> 'Você já possuem uma amizade ou requisição de amizade com este usuário!' ],
                400
            );
        }

        DB::
            table('amizades')
            ->insert([
                'cd_from' => $login->registro->pessoa->cd_pessoa,
                'cd_to' => $pessoa
            ]);

        return Response([ 'msg' =>'SUCCESS' ], 200);
    }

    function accept($cd_amizade)
    {
        if (!$cd_amizade)
        {
            return Response(
                [ 'msg' => 'Informe a solicitação de amizade!' ],
                404
            );
        }

        $jwtLogin = Auth::user();
        $login = Login::
            with('registro.pessoa')
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa)
        {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }

        $amizade = DB::
            table('amizades')
            ->where('cd_amizade', $cd_amizade)
            ->first();

        if (!$amizade)
        {
            return Response(
                [ 'msg'=> 'Requisição de amizade não encontrada!' ],
                404
            );
        }
        if ($amizade->cd_to != $login->registro->pessoa->cd_pessoa)
        {
            return Response([
                'msg'=> 'Requisição de amizade não é sua!'
            ], 401);
        }


        if ($amizade->fg_aceito)
        {
            return Response([
                'msg'=> 'Requisição de amizade já aceita!'
            ], 404);
        }

        DB::
            table('amizades')
            ->where('cd_amizade', $cd_amizade)
            ->update([
                'fg_aceito' => true,
                'dt_alteracao' => Carbon::now()
            ]);

        return Response(['msg' =>'SUCCESS'], 200);
    }

    function deleteFriendship($cd_amizade)
    {
        if (!$cd_amizade)
        {
            return Response([ 'msg' => 'Informe o id da amizade!' ], 404);
        }
        $jwtLogin = Auth::user();
        $login = Login::
            with('registro.pessoa')
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa)
        {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }

        $amizade = DB::
            table('amizades')
            ->where('cd_amizade', $cd_amizade)
            ->first();

        if (!$amizade)
        {
            return Response([ 'msg'=> 'Amizade não encontrada!' ], 404);
        }

        if (
            $amizade->cd_to != $login->registro->pessoa->cd_pessoa &&
            $amizade->cd_from != $login->registro->pessoa->cd_pessoa
        )
        {
            return Response([ 'msg'=> 'Amizade não é sua!' ], 401);
        }

        if ($amizade->fg_aceito != true)
        {
            return Response([ 'msg'=> 'Amizade não foi aceita!' ], 404);
        }

        DB::
            table('amizades')
            ->where('cd_amizade', $cd_amizade)
            ->delete();

        return Response(['msg' =>'SUCCESS'], 200);
    }

    function reject($cd_amizade)
    {
        if (!$cd_amizade)
        {
            return Response(
                [ 'msg' => 'Informe a solicitação de amizade!' ],
                404
            );
        }
        $jwtLogin = Auth::user();
        $login = Login::
            with('registro.pessoa')
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa)
        {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }

        $amizade = DB::
            table('amizades')
            ->where('cd_amizade', $cd_amizade)
            ->first();

        if (!$amizade)
        {
            return Response([ 'msg'=> 'Requisição de amizade não encontrada!' ], 404);
        }
        if ($amizade->cd_to != $login->registro->pessoa->cd_pessoa)
        {
            return Response([ 'msg'=> 'Requisição de amizade não é sua!' ], 401);
        }

        DB::
            table('amizades')
            ->where('cd_amizade', $cd_amizade)
            ->delete();

        return Response(['msg' =>'SUCCESS'], 200);
    }

    function cancel($cd_amizade)
    {
        if (!$cd_amizade)
        {
            return Response([ 'msg' => 'Informe a solicitação de amizade!' ], 404);
        }
        $jwtLogin = Auth::user();
        $login = Login::
            with('registro.pessoa')
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa)
        {
            return Response([ 'msg' => 'Não autorizado' ], 401);
        }

        $amizade = DB::
            table('amizades')
            ->where('cd_amizade', $cd_amizade)
            ->first();

        if (!$amizade)
        {
            return Response([ 'msg'=> 'Requisição de amizade não encontrada!' ], 404);
        }
        if ($amizade->cd_from != $login->registro->pessoa->cd_pessoa)
        {
            return Response([ 'msg'=> 'Requisição de amizade não é sua!' ], 401);
        }

        DB::
            table('amizades')
            ->where('cd_amizade', $cd_amizade)
            ->delete();

        return Response(['msg' =>'SUCCESS'], 200);
    }
}
