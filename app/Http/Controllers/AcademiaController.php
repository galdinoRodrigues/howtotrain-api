<?php

namespace App\Http\Controllers;

use DB;
use Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Empresa;

class AcademiaController extends Controller
{
    public function validarId(){
        $data = Input::all();
        $rules = [
            'uuid' => 'required|string'
        ];
        
        $args = [ 
            $data['uuid']
        ];

        $gymID = DB::select(
            "
            SELECT id_empresa FROM aluno_empresa_invite
            WHERE uuid = ? 
            AND id_aluno IS NULL 
            AND id_professor IS NULL
            ",
            $args
        );

        if (count($gymID) != 0) {    
            return Response([ 'gym_id' => $gymID[0]->id_empresa ] , 200);
        }
        
        $gymID = DB::select(
            "
            SELECT id as id_empresa FROM empresa
            WHERE uuid = ?
            ",
            $args
        );

        if (count($gymID) != 0) {    
            return Response([ 'gym_id' => $gymID[0]->id_empresa ] , 200);
        }

        return Response([ 'msg' => 'Código inválido, Academia não encontrada' ], 404);
    }


    public function listGyms()
    {
        $data = Input::all();
        $rules = [ 'query' => 'string' ];
        $validation = Validator::make($data, $rules);
        if ($validation->fails())
        {
            return Response(
                [ 'msg' => $validation->getMessageBag()->first() ],
                400
            );
        }

        $args = [];

        $search = '';
        if (isset($data['query']))
        {
            $search = ' AND nome LIKE ? ';
            $args[] = '%'.$data['query'].'%';
        }

        $gyms = DB::select(
            "
                SELECT
                    id,
                    nome AS name,
                    url_imagem AS url
                FROM
                    empresa
                WHERE
                    id_tipo_empresa = 3 AND
                    IFNULL(fg_excluido, 0) = 0 AND
                    IFNULL(fg_ativo, 0) = 1
            " . $search,
            $args
        );

        return Response([ 'gyms' => $gyms ]);
    }
}
