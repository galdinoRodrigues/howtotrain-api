<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Pessoa;
use App\Professor;
use App\Aluno;
use App\Login;
use JWTAuth;
use DB;
use Auth;
use Carbon\Carbon;

class NotificationController extends Controller {
    function list() {
        $jwtLogin = Auth::user();

        $login = Login::
            with(
                'registro',
                'registro.pessoa'
            )
            ->where('cd_login', $jwtLogin->cd_login)
            ->first();

        if (!$login || !$login->registro || !$login->registro->pessoa) {
            return Response([
                'msg' => 'Não autorizado'
            ], 401);
        }

        $friendNotifications = DB::
            select("
                SELECT
                    am.cd_amizade,
                    r.ds_nome,
                    r.ds_avatar,
                    a.cd_aluno,
                    pr.cd_professor,
                    am.dt_inclusao as dt_requested,
                    IF(pr.cd_professor IS NULL, 1, 2) AS indTipoRegistro
                FROM
                    amizades AS am
                INNER JOIN
                    pessoas AS p
                        ON p.cd_pessoa = am.cd_from
                INNER JOIN
                    registros AS r
                        ON r.cd_registro = p.cd_registro
                LEFT JOIN
                    alunos AS a
                        ON a.cd_pessoa = p.cd_pessoa
                LEFT JOIN
                    professores AS pr
                        ON pr.cd_pessoa = p.cd_pessoa
                WHERE
                    am.fg_aceito <> 1 AND
                    am.cd_to = ?
                ORDER BY
                    dt_requested DESC
                LIMIT 50;
                ",
                [ $login->registro->pessoa->cd_pessoa ]
            );
        return Response([
            'notifications' => $friendNotifications
        ]);
    }

}
