<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Login extends Authenticatable implements JWTSubject {
    use Notifiable;

    protected $table = 'logins';
    protected $primaryKey = 'cd_login';
    public $timestamps = false;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function registro() {
        return $this->hasOne('App\Registro', 'cd_registro', 'cd_registro');
    }
}