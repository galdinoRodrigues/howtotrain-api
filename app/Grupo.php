<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model {
    protected $table = 'grupos';
    protected $primaryKey = 'cd_grupo';
    // Relationships
    public function pessoa() {
        return $this->belongsTo('App\Exercicio', 'cd_grupo', 'cd_grupo');
    }

}
