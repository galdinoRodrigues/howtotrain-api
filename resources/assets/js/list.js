window.addEventListener('DOMContentLoaded', () => {
    document.querySelector('#search').addEventListener('keyup', event => {
        if (event.keyCode === 13) {
            search();
        }
    });
    document.querySelector('#search-glass').addEventListener('click', () => search());
});

function search() {
    return;
    const search = document.querySelector('#search').value;
    if (!search || !search.trim()) {
        return;
    }
    const url = document.querySelector('.page.active').getAttribute('href').split('&search');
    window.location.href = url + '&search=' + search
}