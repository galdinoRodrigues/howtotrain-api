<!doctype html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

		<title>How To Train</title>

		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
		<link rel="stylesheet" href="{{ URL::asset('css/home.css?4') }} ">
		<link rel="stylesheet" href="{{ URL::asset('css/home-queries.css?4') }} ">
	</head>
	<body>
		<section id="intro">
			<div id="logo">HOW TO<span>TRAIN</span></div>
			<object id="background-ellipse" data="{{URL::asset('images/ellipse.svg')}}" type="image/svg+xml"></object>
			<div id="background-ellipse-fake">
				<span>
					<span>
						<span>
							<span>
								<span>
									<span>
										<span>
											<span>
												<span class="fill"></span>
											</span>
										</span>
									</span>
								</span>
							</span>
						</span>
					</span>
				</span>
			</div>
			<div class="info">
				<h2>Seu Treino Em Suas Mãos</h2>
				<span>O aplicativo que vai revolucionar seu treino e sua academia</span>
				<div class="phone">
					<img class="front" src="{{URL::asset('images/Samsung-Galaxy-S8-Front-Mockup-372x800-compressed.png')}}" alt="">
					<div class="back">
						<img class="slide-item" src="{{URL::asset('images/model_home-compressed.jpeg')}}" alt="">
					</div>
				</div>
				<a
					class="download"
					href='https://play.google.com/store/apps/details?id=com.galdinoLabs.howToTrain&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'
				>
					<img
						alt='Disponível no Google Play'
						src="{{URL::asset('images/playbadge-compressed.png')}}"
					/>
				</a>
			</div>
			<div class="phone">
				<img class="front" src="{{URL::asset('images/Samsung-Galaxy-S8-Front-Mockup-372x800-compressed.png')}}" alt="">
				<div class="back">
					<img class="slide-item" src="{{URL::asset('images/model_home-compressed.jpeg')}}" alt="">
				</div>
			</div>
		</section>
		<div class="division"></div>
		<section id="about">
			<h3>Nós ajudamos <span>você</span> a atingir seus objetivos</h3>
			<h5>Com informações dos exercícios, será possível montar e acompanhar treinos te ajudando a:</h5>
			<div class="carousel">
				<button class="left arrow-button small"><span></span></button>
				<button class="right arrow-button small"><span></span></button>
				<div class="fade-left"></div>
				<div class="fade-right"></div>
				<div class="bar" current="0"></div>
				<div class="row">
					<div class="item">
							<div class="box">
								<object data="{{URL::asset('images/ic_svg_definition_black.svg')}}" type="image/svg+xml"></object>
								<span>Definir</span>
								<span>Sempre temos aquela parte do corpo que queremos melhorar, para isso é preciso saber como</span>
							</div>
						</div>
					<div class="item active">
						<div class="box">
							<object data="{{URL::asset('images/ic_svg_get_muscle_black.svg')}}" type="image/svg+xml"></object>
							<span>Ganhar Músculo</span>
							<span>Realmente um desafio para muitos, ao atingir um nível físico, é preciso de novos estímulos para continuar evoluindo</span>
						</div>
					</div>
					<div class="item">
						<div class="box">
							<object data="{{URL::asset('images/ic_svg_get_in_shape_black.svg')}}" type="image/svg+xml"></object>
							<span>Entrar em forma</span>
							<span>Muitas vezes abandonamos os cuidados com nossa saúde, mas quando decidimos melhorar, ter um bom auxílio é primordial</span>
						</div>
					</div>
					<div class="item">
						<div class="box">
							<object data="{{URL::asset('images/ic_svg_get_power_black.svg')}}" type="image/svg+xml"></object>
							<span>Ganhar força</span>
							<span>Tem exercícios que achamos mais difíceis de fazer, e é esses que mais precisamos, mas é possível melhorar de outras formas</span>
						</div>
					</div>
				</div>
			</div>
		</section >
		<section id="pricing">
			<h3>Faça já a sua Assinatura</h3>
			<object class="ball" data="{{URL::asset('images/ellipse.svg')}}" type="image/svg+xml"></object>
			<object class="ball second" data="{{URL::asset('images/ellipse.svg')}}" type="image/svg+xml"></object>
			<object class="ball third" data="{{URL::asset('images/ellipse.svg')}}" type="image/svg+xml"></object>
			<div class="carousel">
				<div class="card">
					<div class="title">Básico</div>
					<div class="price">
						R$ <span>14</span>,70 /mês
					</div>
					<div class="small-price">R$ 0,49/aluno</div>
					<div class="info">Arraste para fazer uma simulação</div>
					<input type="range" value="30" min="30" max="350">
					<div class="range-text">Até <input class="num-students" max="350" min="30" value="30"/> alunos</div>
					<div class="features">
						<div><b>•Um App exclusivo para os Alunos</b></div>
						<div><b>•Um App exclusivo para os Professores</b></div>
						<div>• Base de exercícios pré-cadastrados para <br/> montagem de treinos</div>
						<div>• Canal de comunicação com os alunos</div>
						<div>• Ambos os aplicativos mostram <br/> za logo do HowToTrain</div>
					</div>
					<button class="subscribe basic">Assine</button>
				</div>
				<div class="card premium">
					<div class="title">
					Enterprise <div class="diamond right"></div>
					</div>
					<div class="price">
						R$ <span>29</span>,70 /mês
					</div>
					<div class="small-price">R$ 0,99/aluno</div>
					<div class="info">Arraste para fazer uma simulação</div>
					<input type="range" value="30" min="30" max="350">
					<div class="range-text">Até <input class="num-students" max="350" min="30" value="30"/> alunos</div>

					<div class="features">
						<div><b>• App personalizado para a sua academia <br/>(cores, nome e logo do App)</b></div>
						<div><b>•Um App exclusivo para os Alunos</b></div>
						<div><b>•Um App exclusivo para os Professores</b></div>
						<div>• Base de exercícios pré-cadastrados para <br/> montagem de treinos</div>
						<div>• Canal de comunicação com os alunos</div>
					</div>

					<button class="subscribe premium">Assine</button>
				</div>
			</div>
			<div class="subscribe-overlay">
				<div class="box">
					<div class="close-overlay">
						<div class="cross"></div>
					</div>
					<div class="intro">
						Plano Básico <br> <span>30 alunos por R$ 14,70 /mês</span>
					</div>
					<div class="logo-container">
						<div class="logo">
							HOW TO<span>TRAIN</span>
						</div>
					</div>
					<div class="form-group">
						<input class="name" type="text" placeholder="Nome">
					</div>
					<div class="form-group">
						<input class="email" type="email" placeholder="E-mail para Contato">
					</div>
					<div class="form-group">
						<input
							class="phone"
							type="tel"
							pattern="[0-9]{10}"
							value=""
							placeholder="Telefone Para Contato"
							autocomplete="off"
							maxlength="15"
							onkeyup="mascara( this, mtel );"
						>
					</div>
					<div class="form-group">
						<button>Solicitar Assinatura</button>
					</div>
				</div>
			</div>
		</section>
		<section id="contact">
			<h2>Quer o Aplicativo Em Sua Academia?</h2>
			<h4>Se você treina e recebe o treino em papel mas gostaria de recebe-lo de uma forma mais inteligente, de ter mais informações sobre seu treino e os exercícios dele, entre em contato conosco, iremos até sua academia</h4>
			<h4>Se você é dono de academia e gostou da ideia, entre em contato conosco, faremos uma apresentação completa sobre nosso serviço</h4>
			<div class="email-box">
				<object id="mail-icon" data="{{URL::asset('images/ic_svg_letter.svg')}}" type="image/svg+xml"></object>
				<div class="form-group">
					<input type="email" placeholder="seu@email.com.br" name="from" id="email-from">
					<input type="text" placeholder="Mensagem aqui..." name="message" id="email-message">
				</div>
				<button id="send-mail">Enviar</button>
			</div>
			<div id="social-media">
				<div>Nos acompanhe também nas redes sociais!</div>
				<a href="https://www.facebook.com/How-to-Train-234565457420864">
					<img id="facebook" class="btn-h"  src="{{URL::asset('images/ic_vt_facebook.svg')}}" type="image/svg+xml"></img>
				</a>
				<a href="https://www.instagram.com/_howtotrain/">
					<img id="instagram" class="btn-h"  src="{{URL::asset('images/insta-svg.svg')}}" type="image/svg+xml"></img>
				</a>
			</div>
			<div class="division"> </div>
			<a href="https://github.com/GaldinoJr" id="created-by">Created By Galdino Rodrigues</a>
			<a href="https://github.com/PauloGasparSv" id="made-by">Made by Paulo Gaspar</a>
			<object id="background-contact-ellipse" data="{{URL::asset('images/ellipse_gray.svg')}}" type="image/svg+xml"></object>
		</section>
		<div class="overlay"><div class="lds-dual-ring"></div></div>
		<div class="toast">Email Enviado</div>
		<button onclick="scrollToTop()" id="scrollTop" class="arrow-button up" title="Ir para o topo"><span></span></button>
	</body>

	<script>
		var lastScroll = null;
		window.onscroll = () => scrollFunction();
		function scrollFunction() {
			const button = document.querySelector("#scrollTop");
			if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
				button.style.cursor = 'pointer';
				return button.style.opacity = "0.6";
			}
			button.style.cursor = 'default';
			document.querySelector("#scrollTop").style.opacity = "0";
		}
		function scrollToTop() {
			if (document.querySelector("#scrollTop").style.opacity == '0') {
				lastScroll = null;
				return;
			}
			var dist = (document.body.scrollToTop || document.documentElement.scrollTop) || 0;

			if (lastScroll !== null && lastScroll < dist) {
				lastScroll = null;
				return;
			}
			lastScroll = dist;
			if (dist % 5 != 0 ) {
				dist = parseInt(dist / 5) * 5;
			}
			dist -=  dist > 100 ? dist / 20 : dist / 10;
			if (dist < 10) {
				dist = 0;
			}
			document.body.scrollTop = dist;
			document.documentElement.scrollTop = dist;
			if (dist > 0) setTimeout(() =>  scrollToTop(), 10);
			else lastScroll = null;
		}

		document.querySelector('#about>.carousel>.arrow-button.right').addEventListener('click', () => {
			const row = document.querySelector('#about>.carousel>.row');
			const bar = document.querySelector('#about>.carousel>.bar');

			let position =  (isNaN(row.style.left) && row.style.left.indexOf('-') < -1) ? '50%' : (row.style.left || '50%');
			position = parseInt(position.substring(0, position.indexOf('%'))) - 50;

			if (bar.hasAttribute('wait')) return;
			bar.setAttribute('wait','1.7s');

			setTimeout(() => bar.removeAttribute('wait'), 1700);

			let current = bar.getAttribute('current');
			let grid = window.getComputedStyle(bar).content;

			if (grid == '"1"' && position == -300) return;
			if (grid == '"2"' && position == -100) return;
			if (grid == '"3"' && position  == -100) return;

			if (grid == '"3"') {
				row.style.left = '20%';
				document.querySelector('#about>.carousel .item:nth-child(2)').classList.remove('active');
				document.querySelector('#about>.carousel .item:nth-child(3)').classList.add('active');
			} else row.style.left = (position - 50) + '%';
		});
		document.querySelector('#about>.carousel>.arrow-button.left').addEventListener('click', () => {
			const row = document.querySelector('#about>.carousel>.row');
			const bar = document.querySelector('#about>.carousel>.bar');

			let position =  (isNaN(row.style.left) && row.style.left.indexOf('-') < -1) ? '50%' : (row.style.left || '50%');
			position = parseInt(position.substring(0, position.indexOf('%'))) + 50;

			if (bar.hasAttribute('wait')) return;
			bar.setAttribute('wait','1.7s');
			setTimeout(() => bar.removeAttribute('wait'), 1700);

			let current = bar.getAttribute('current');
			let grid = window.getComputedStyle(bar).content;

			if (position == 100) {
				bar.setAttribute('current', '0');
				bar.style.left = '50%';
				return;
			}

			if (grid == '"3"') {
				document.querySelector('#about>.carousel .item:nth-child(3)').classList.remove('active');
				document.querySelector('#about>.carousel .item:nth-child(2)').classList.add('active');
			}

			bar.style.opacity = '1';

			row.style.left = ((position + 50) > 50 ? 50 : (position + 50)) + '%';
		});

		function validateEmail(email) {
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(String(email).toLowerCase());
		}

		function toast(message, color) {
			const toast = document.querySelector('.toast');
			toast.style.background = color;
			toast.innerHTML = message;
			toast.style.top = '32px';
			setTimeout(() => toast.style.top = '-72px', 1800);
		}

		function sendEmail(from, message, callback) {
			var params = {
				email: from || document.querySelector('#email-from').value,
				message: message || document.querySelector('#email-message').value,
			};

			if (!validateEmail(params.email)) {
				toast("Email inválido!", "red");
				return;
			}
			if (!params.email || !params.message || params.email.trim() == '' || params.message.trim() == '') return;

			var formData = new FormData();

			for (var k in params) {
				formData.append(k, params[k]);
			}

			var request = {
				method: 'POST',
				body: formData
			};

			document.querySelector('.overlay').style.display = 'block';
			setTimeout(() => {
				fetch('/api/emailContato', request)
				.then(response => {
					document.querySelector('.overlay').style.display = 'none';
					if (response.status == 200) {
						document.querySelector('#email-from').value = '';
						document.querySelector('#email-message').value = '';
						toast('Email enviado!', '#02da02');
						if (callback) {
							callback();
						}
					} else{
						toast('Erro ao enviar!', 'red');
					}
				})
				.catch(error => {
					document.querySelector('.overlay').style.display = 'none';
					const toast = document.querySelector('.toast');
					toast('Erro ao enviar!', 'red');
				});
			}, 800);
		}

		document.querySelector('#send-mail').addEventListener('click', () => {sendEmail();})
		document.querySelector('#email-from').onkeypress = function(e) {
			if (!e) e = window.event;
			var keyCode = e.keyCode || e.which;
			if (keyCode == '13') {
				sendEmail();
				return false;
			}
		}
		document.querySelector('#email-message').onkeypress = function(e) {
			if (!e) e = window.event;
			var keyCode = e.keyCode || e.which;
			if (keyCode == '13') {
				sendEmail();
				return false;
			}
		}

		const cardBasic = document.querySelector('#pricing .carousel .card:not(.premium)')
		if (cardBasic) {
			const smallPriceDiv = cardBasic.querySelector('.small-price');
			const priceDiv = cardBasic.querySelector('.price');
			const priceInput = cardBasic.querySelector('.num-students');
			const priceRange = cardBasic.querySelector('input[type="range"]');
			const max = parseInt(priceRange.getAttribute('max'));
			const min = parseInt(priceRange.getAttribute('min'));

			priceInput.addEventListener('change', function() {
				if (priceInput.value > max) {
					priceInput.value = max;
				}
				if (priceInput.value < min) {
					priceInput.value = min;
				}

				const numAlunos = parseInt(priceInput.value);
				let preco = 14.70;
				if (numAlunos < 100) {
					smallPriceDiv.textContent = 'R$ 0,49/aluno';
					preco = numAlunos * 0.49;
				} else if (numAlunos >= 120) {
					smallPriceDiv.textContent = 'R$ 0,42/aluno';
					preco = numAlunos * 0.42;
				} else {
					smallPriceDiv.textContent = 'R$ 0,42/aluno';
					preco = 120 * 0.42;
				}

				priceRange.value = numAlunos;
				priceInput.value = numAlunos;

				const splitted = preco.toFixed(2).replace(',', '.').split('.');
				const dezenas = splitted[0];
				const decimals = splitted[1];

				priceDiv.innerHTML = 'R$ <span>' + dezenas.padStart(2, '0') +  '</span>,' + decimals.padStart(2, '0') +' /mês';
			});

			priceRange.addEventListener('input', function() {
				const numAlunos = parseInt(priceRange.value);
				let preco = 14.70;
				if (numAlunos < 100) {
					smallPriceDiv.textContent = 'R$ 0,49/aluno';
					preco = numAlunos * 0.49;
				} else if (numAlunos >= 120) {
					smallPriceDiv.textContent = 'R$ 0,42/aluno';
					preco = numAlunos * 0.42;
				} else {
					smallPriceDiv.textContent = 'R$ 0,42/aluno';
					preco = 120 * 0.42;
				}

				priceInput.value = numAlunos;

				const splitted = preco.toFixed(2).replace(',', '.').split('.');
				const dezenas = splitted[0];
				const decimals = splitted[1];

				priceDiv.innerHTML = 'R$ <span>' + dezenas.padStart(2, '0') +  '</span>,' + decimals.padStart(2, '0') +' /mês';
			});
		}

		const cardBusiness = document.querySelector('#pricing .carousel .card.premium')
		if (cardBusiness) {
			const smallPriceDiv = cardBusiness.querySelector('.small-price');
			const priceDiv = cardBusiness.querySelector('.price');
			const priceInput = cardBusiness.querySelector('.num-students');
			const priceRange = cardBusiness.querySelector('input[type="range"]');
			const max = parseInt(priceRange.getAttribute('max'));
			const min = parseInt(priceRange.getAttribute('min'));

			priceInput.addEventListener('change', function() {
				if (priceInput.value > max) {
					priceInput.value = max;
				}
				if (priceInput.value < min) {
					priceInput.value = min;
				}

				const numAlunos = parseInt(priceInput.value);
				let preco = 29.70;
				if (numAlunos < 100) {
					smallPriceDiv.textContent = 'R$ 0,99/aluno';
					preco = numAlunos * 0.99;
				} else if (numAlunos >= 120) {
					smallPriceDiv.textContent = 'R$ 0,89/aluno';
					preco = numAlunos * 0.89;
				} else {
					smallPriceDiv.textContent = 'R$ 0,89/aluno';
					preco = 120 * 0.89;
				}

				priceRange.value = numAlunos;
				priceInput.value = numAlunos;

				const splitted = preco.toFixed(2).replace(',', '.').split('.');
				const dezenas = splitted[0];
				const decimals = splitted[1];

				priceDiv.innerHTML = 'R$ <span>' + dezenas.padStart(2, '0') +  '</span>,' + decimals.padStart(2, '0') +' /mês';
			});

			priceRange.addEventListener('input', function() {
				const numAlunos = parseInt(priceRange.value);
				let preco = 29.70;
				if (numAlunos < 100) {
					smallPriceDiv.textContent = 'R$ 0,99/aluno';
					preco = numAlunos * 0.99;
				} else if (numAlunos >= 120) {
					smallPriceDiv.textContent = 'R$ 0,89/aluno';
					preco = numAlunos * 0.89;
				} else {
					smallPriceDiv.textContent = 'R$ 0,89/aluno';
					preco = 120 * 0.89;
				}

				priceInput.value = numAlunos;

				const splitted = preco.toFixed(2).replace(',', '.').split('.');
				const dezenas = splitted[0];
				const decimals = splitted[1];

				priceDiv.innerHTML = 'R$ <span>' + dezenas.padStart(2, '0') +  '</span>,' + decimals.padStart(2, '0') +' /mês';
			});
		}

		let subscribeButtons = document.querySelectorAll('.subscribe');
		if (subscribeButtons) {
			const subscribeOverlay = document.querySelector('.subscribe-overlay');
			const closeOverlay = document.querySelector('.close-overlay');
			const submitButton = document.querySelector('.subscribe-overlay .box button');
			const headerIntro = document.querySelector('.subscribe-overlay .box .intro');

			closeOverlay.addEventListener('click', () => {
				subscribeOverlay.classList.remove('active');
			});

			submitButton.addEventListener('click', () => {
				let message = '';
				const from = document.querySelector('.subscribe-overlay .email').value;

				const phone = document.querySelector('.subscribe-overlay .phone').value;
				const name = document.querySelector('.subscribe-overlay .name').value;

				if (!name.trim()) {
					return toast("Nome é obrigatório!", "red");
				}
				if (!from.trim()) {
					return toast("E-mail é obrigatório!", "red");
				}
				if (!phone.trim()) {
					return toast("Telefone é obrigatório!", "red");
				}

				message = name + ' de telefone ' + phone + ' solicitou uma estimativa hoje ' + new Date().toLocaleDateString() + ' as ' + new Date().getHours().toString().padStart(2, '0') + ':' + new Date().getMinutes().toString().padStart(2, '0') + ' solicitando ' + headerIntro.textContent;

				sendEmail(from, message, () => {
					subscribeOverlay.classList.remove('active');
				});
			});

			subscribeButtons.forEach(subBtn => {
				subBtn.addEventListener('click', () => {
					if (subscribeOverlay.classList.contains('active')) {
						subscribeOverlay.classList.remove('active');
						return;
					}

					subscribeOverlay.classList.add('active');
					if (subBtn.classList.contains('basic')) {
						submitButton.classList.remove('premium');
						headerIntro.classList.remove('premium');

						const price = document.querySelector('.card:not(.premium) .price').textContent.trim();
						const numStudents = document.querySelector('.card:not(.premium) .num-students').value;

						headerIntro.innerHTML = 'Plano Premium <br> <span>' + numStudents + ' alunos por ' + price + '</span>';

					} else if (subBtn.classList.contains('premium')) {
						submitButton.classList.add('premium');
						headerIntro.classList.add('premium');

						const price = document.querySelector('.card.premium .price').textContent.trim();
						const numStudents = document.querySelector('.card.premium .num-students').value;

						headerIntro.innerHTML = 'Plano Premium <br> <span>' + numStudents + ' alunos por ' + price + '</span>';
					}
				});
			});
		}
		function mascara(o,f){
			v_obj=o
			v_fun=f
			setTimeout("execmascara()",1)
		}
		function execmascara(){
			v_obj.value=v_fun(v_obj.value)
		}
		function mtel(v){
			v=v.replace(/\D/g,"");
			v=v.replace(/^(\d{2})(\d)/g,"($1) $2");
			v=v.replace(/(\d)(\d{4})$/,"$1-$2");
			return v;
		}

	</script>
</html>
