<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title>How To Train - Termos</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
</head>

<body style="padding: 0; margin: 0;">

    <div style="width: 80%; margin: auto;">
        <p style="margin-top:32px; margin-bottom:8pt; text-align:center; line-height:108%; font-size:16pt"><span style="font-family:Arial; text-align:center;;">Termos
                de Serviço – How To Train</span><span style="font-family:Calibri; font-size:11pt"> </span><span style="font-family:Arial">©</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; line-height:108%; font-size:14pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">&#xa0;</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; line-height:108%; font-size:14pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="width:36pt; display:inline-block">&#xa0;</span><span
                style="font-family:Arial">Introdução</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-indent:35.4pt; text-align:justify; line-height:150%; font-size:12pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">Obrigado
                por utilizar o serviço disponibilizado pelo How To Train©, esperamos que o aplicativo atenda às
                expectativas e necessidades de seu público alvo. Logo, ressalte-se, sua utilização do serviço requer o
                aceite dos seguintes termos e, portanto, pedimos que leia calmamente e, no caso de discordância ou mau
                entendimento da finalidade do aplicativo, recomenda-se o não uso deste.</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-indent:35.4pt; text-align:justify; line-height:150%; font-size:12pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">Este
                aplicativo foi desenvolvido a partir da intenção de organizar e otimizar os treinos criados por
                professores de centros de treinamento/academia, a fim de que seja possível que acessem diretamente de
                seus dispositivos eletrônicos o melhor treinamento para cada aluno. Sendo assim, cabe ressaltar que,
                não é de responsabilidade do desenvolvedor do aplicativo ou da criadora dos exercícios disponíveis no
                serviço quaisquer resultados negativos oriundos do mau uso dos treinos assim selecionados, tendo em
                vista o caráter informativo do presente aplicativo, não havendo relações diretas com o treinamento em
                si.</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-indent:35.4pt; text-align:justify; line-height:150%; font-size:12pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">&#xa0;</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-indent:35.4pt; text-align:justify; line-height:150%; font-size:14pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">Desenvolvimento
                e criação</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-indent:35.4pt; text-align:justify; line-height:150%; font-size:12pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">How
                To Train© foi desenvolvido pelo analista e desenvolvedor de sistemas Galdino Rodrigues dos Ouros Junior
                e a criação dos exercícios foi feita pela bacharela em educação física Verônica Louise dos Santos, de
                modo que prezamos pelo profissionalismo e transparência no que tange à utilização e divulgação do
                serviço prestado.</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-indent:35.4pt; text-align:justify; line-height:150%; font-size:12pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">&#xa0;</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-indent:35.4pt; text-align:justify; line-height:108%; font-size:14pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">Legislação</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-indent:35.4pt; text-align:justify; line-height:150%; font-size:12pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">Conforme
                a Lei nº 9.609/1998, que regula a propriedade intelectual de programa de computador, tem-se</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-left:113.4pt; margin-bottom:8pt; text-align:justify; font-size:10pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">Art.
                1º Programa de computador é a expressão de um conjunto organizado de instruções em linguagem natural ou
                codificada, contida em suporte físico de qualquer natureza, de emprego necessário em máquinas
                automáticas de tratamento da informação, dispositivos, instrumentos ou equipamentos periféricos,
                baseados em técnica digital ou análoga, para fazê-los funcionar de modo e para fins determinados.</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-left:113.4pt; margin-bottom:8pt; text-align:justify; font-size:10pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">Art.
                2º O regime de proteção à propriedade intelectual de programa de computador é o conferido às obras
                literárias pela legislação de direitos autorais e conexos vigentes no País, observado o disposto nesta
                Lei.</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-left:113.4pt; margin-bottom:8pt; text-align:justify; font-size:10pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">§
                1º Não se aplicam ao programa de computador as disposições relativas aos direitos morais, ressalvado, a
                qualquer tempo, o direito do autor de reivindicar a paternidade do programa de computador e o direito
                do autor de opor-se a alterações não-autorizadas, quando estas impliquem deformação, mutilação ou outra
                modificação do programa de computador, que prejudiquem a sua honra ou a sua reputação.</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-left:113.4pt; margin-bottom:8pt; text-align:justify; font-size:10pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">§
                2º Fica assegurada a tutela dos direitos relativos a programa de computador pelo prazo de cinqüenta
                anos, contados a partir de 1º de janeiro do ano subseqüente ao da sua publicação ou, na ausência desta,
                da sua criação.</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-left:113.4pt; margin-bottom:8pt; text-align:justify; font-size:10pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">§
                3º A proteção aos direitos de que trata esta Lei independe de registro.</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-align:justify; line-height:150%; font-size:12pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="width:36pt; display:inline-block">&#xa0;</span><span
                style="font-family:Arial">Portanto, aqueles que fizerem mau uso, infringindo a Lei acima, responderão,
                conforme disposto em Lei, pelos prejuízos causados ao desenvolvedor a partir de uso indevido e violação
                dos direitos do autor.</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-align:justify; line-height:150%; font-size:12pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="width:36pt; display:inline-block">&#xa0;</span><span
                style="font-family:Arial">Ainda, conforme a Lei 9.610/1998, que dispõe acerca dos direitos autorais,</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-left:113.4pt; margin-bottom:8pt; text-align:justify; font-size:10pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">Art.
                7º São obras intelectuais protegidas as criações do espírito, expressas por qualquer meio ou fixadas em
                qualquer suporte, tangível ou intangível, conhecido ou que se invente no futuro, tais como:</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-left:113.4pt; margin-bottom:8pt; text-align:justify; font-size:10pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">XII
                - os programas de computador;</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-left:113.4pt; margin-bottom:8pt; text-align:justify; font-size:10pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial; background-color:#ffffff">Art.
                18. A proteção aos direitos de que trata esta Lei independe de registro.</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-left:113.4pt; margin-bottom:8pt; text-align:justify; font-size:10pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial; background-color:#ffffff">Art.
                22. Pertencem ao autor os direitos morais e patrimoniais sobre a obra que criou.</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-left:113.4pt; margin-bottom:8pt; text-align:justify; font-size:10pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial; background-color:#ffffff">Art.
                27. Os direitos morais do autor são inalienáveis e irrenunciáveis.</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-align:justify; line-height:150%; font-size:12pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="width:36pt; display:inline-block">&#xa0;</span><span
                style="font-family:Arial">Recomenda-se a leitura das leis citadas acima para obter total compreensão
                sobre a propriedade intelectual e, por conseguinte, dos direitos autorais oriundos, de modo a se evitar
                futuros constrangimentos em relação a este aplicativo, além de obter o completo teor das legislações e
                estar ciente acerca de quaisquer violações que possam ser feitas em relação a este aplicativo.</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-align:justify; line-height:150%; font-size:12pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="width:36pt; display:inline-block">&#xa0;</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-align:justify; line-height:150%; font-size:14pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="width:36pt; display:inline-block">&#xa0;</span><a
                name="politicaPrivacidade"></a><span
                style="font-family:Arial">Cadastro de usuários</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-align:justify; line-height:150%; font-size:12pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="width:36pt; display:inline-block">&#xa0;</span><span style="font-family:Arial">As informações coletadas para a
                utilização do serviço prestado pelo aplicativo How To Train© são compostas de nome completo, idade,
                peso, condição física, intenção em relação ao treino (emagrecimento, tonificação muscular etc.), cidade
                e estado, sendo estas informações preenchidas pelo próprio usuário quando no momento da instalação e
                concordância a estes termos de serviço. Garante-se que o uso destas se restringe à finalidade e
                funcionalidade do serviço, sendo confidenciais e de acesso apenas ao fabricante e usuário.</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-align:justify; line-height:150%; font-size:12pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">&#xa0;</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-align:justify; line-height:150%; font-size:14pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="width:36pt; display:inline-block">&#xa0;</span><span
                style="font-family:Arial">Cancelamento do serviço</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-align:justify; line-height:150%; font-size:12pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="width:36pt; display:inline-block">&#xa0;</span><span
                style="font-family:Arial">Havendo insatisfação com o uso ou alguma outra funcionalidade do aplicativo,
                a desinstalação do How To Train© será suficiente para findar o vínculo entre usuário e fabricante,
                ressaltando que How To Train© é um aplicativo gratuito e que as informações coletadas durante seu uso
                restringem-se ao âmbito do serviço, não sendo utilizadas para fins escusos e que ultrapassem as
                finalidades propostas pelo desenvolvedor.</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-align:justify; line-height:150%; font-size:12pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">&#xa0;</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-align:right; line-height:150%; font-size:12pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">Galdino
                Rodrigues dos Ouros Junior,</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-align:right; line-height:150%; font-size:12pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a name="_gjdgxs"></a><span
                style="font-family:Arial">desenvolvedor do aplicativo How To Train©,</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="margin-top:0pt; margin-bottom:8pt; text-align:right; line-height:150%; font-size:12pt">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Arial">Mogi
                das Cruzes, 12 de outubro de 2018.</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <button style="font-size:22px; height: 64px; cursor: pointer;width: 100%; color: white; background: #ffa100; margin: 0; outline: none; border: none;">Entendi</button>

</body>


</html>