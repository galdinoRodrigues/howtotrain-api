<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <title>@yield('title')</title>
</head>
<body>
    @section('sidebar')
        <nav id="sidebar">
            <a href="/admin/home" class="item home">
                <div class="icon">
                    <i class="zmdi zmdi-home"></i>
                </div>
                <span>Dashboard</span>
            </a>
            <a href="/admin/students" class="item students">
                <div class="icon">
                    <i class="zmdi zmdi-account"></i>
                </div>
                <span>Alunos</span>
            </a>
            <a href="/admin/teachers" class="item teachers">
                <div class="icon">
                    <i class="zmdi zmdi-graduation-cap"></i>
                </div>
                <span>Professores</span>
            </a>
            <a href="/admin/gyms" class="item gyms">
                <div class="icon">
                    <i class="zmdi zmdi-pin"></i>
                </div>
                <span>Academias</span>
            </a>
            <a href="/admin/admins" class="item admins">
                <div class="icon">
                    <i class="zmdi zmdi-star"></i>
                </div>
                <span>Administradores</span>
            </a>
            <a href="{{ route('admin.logout') }}" class="item">
                <div class="icon">
                    <i class="zmdi zmdi-sign-in sign-out"></i>
                </div>
                <span>Sair</span>
            </a>
        </nav>
    @show
    @yield('content')
    <script>
        if (window.location.href.includes('/home')) {
            document.querySelector('#sidebar .item.home').classList.add('active');
        } else if (window.location.href.includes('/students')) {
            document.querySelector('#sidebar .item.students').classList.add('active');
        } else if (window.location.href.includes('/teachers')) {
            document.querySelector('#sidebar .item.teachers').classList.add('active');
        } else if (window.location.href.includes('/gyms')) {
            document.querySelector('#sidebar .item.gyms').classList.add('active');
        } else if (window.location.href.includes('/admins')) {
            document.querySelector('#sidebar .item.admins').classList.add('active');
        }
    </script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    @yield('scripts')
</body>
</html>