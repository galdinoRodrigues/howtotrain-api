@extends('admin.base')
@section('title', 'Admin Login')

@section('sidebar')
@endsection
@section('header')
@endsection

@section('content')
<section id="login-form">
    <form method="POST" action="{{ route('admin.login') }}">
        <div id="logo">HOW TO<span>TRAIN</span></div>
        <div class="form-group">
            <label for="E-mail">E-mail</label>
            <input class="form-control" type="email" name="email" placeholder="E-mail" autofocus>
        </div>
        <div class="form-group">
            <label for="password">Senha</label>
            <input class="form-control" type="password" name="password" placeholder="Senha">
        </div>
        <span class="error">{{ $errors->login->first() }}</span>
        <div class="action-group">
            <button class="btn primary big" type="submit">Login</button>
        </div>

        {{ csrf_field() }}
    </form>
</section>

@endsection