@extends('admin.base')

@section('content')
<main>
    <h1>Administradores do Sistema</h1>

    <div id="admin-list">
        <div class="list-header">
            <div class="form-group">
                <input id="search" placeholder="Pesquisar..." class="form-control">
                <i id="search-glass" class="zmdi zmdi-search"></i>
            </div>
        </div>
        <ul class="list">
            <li class="flex header">
                <div class="flex-col-1">#</div>
                <div class="flex-col-3">Nome</div>
                <div class="flex-col-3">E-mail</div>
            </li>
            @forelse ($admins as $admin)
                <li class="flex">
                    <div class="flex-col-1">{{ $admin->id }}</div>
                    <div class="flex-col-3">{{ $admin->name }}</div>
                    <div class="flex-col-3">{{ $admin->email }}</div>
                </li>
            @empty
                <p>Nenhum Administrador</p>
            @endforelse
        </ul>
        <div class="pagination">
                @php
                    $from = $admins->currentPage();
                    if ($from < 4) {
                        $from  = 1;
                    } else {
                        $from = $from - 2;
                    }
                    $diff = $admins->currentPage();
                    $diff = $admins->lastPage() - $diff;
                    if ($diff > 4) {
                        $diff = 4;
                    } else if ($diff < 0) {
                        $diff = 0;
                    }
                    $to = $from + $diff;
                    $searchUrl = "";
                    if  ($search){
                        $searchUrl = "&search=".$search;
                    }
                @endphp
            @if ($admins->previousPageUrl())
                <a href="{{ $admins->previousPageUrl() }}{{ $searchUrl }}"class="previous"></a>
            @endif
            @for ($i = $from; $i <= $to; $i++)
                @if ($admins->currentPage() == $i)
                    <a href="/admin/students?page={{ $i }}{{ $searchUrl }}" class="page active {{ $i == $from ? 'first' : ''}}">{{$i}}</a>
                @else
                    <a href="/admin/students?page={{ $i }}{{ $searchUrl }}" class="page {{ $i == $from ? 'first' : ''}}">{{$i}}</a>
                @endif
            @endfor
            @if ($admins->nextPageUrl())
                <a href="{{ $admins->nextPageUrl() }}{{ $searchUrl }}"class="next"></a>
            @endif
        </div>
    </div>

</main>
@endsection
@section('scripts')
<script src="{{ asset('js/app.js') }}"></script>
@endsection