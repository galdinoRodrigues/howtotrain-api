@extends('admin.base')

@section('content')
<main>
    <h1>Dashboard</h1>

    <div id="dashboard">
        <div class="card">
            <div id="count-students" class="value">-</div>
            <div class="legend">Alunos Cadastrados</div>
        </div>
        <div class="card">
            <div id="count-teachers" class="value">-</div>
            <div class="legend">Professores Cadastrados</div>
        </div>
        <div class="card">
            <div id="count-gyms" class="value">-</div>
            <div class="legend">Academias Cadastradas</div>
        </div>
        <div class="card">
            <div id="count-exercises" class="value">-</div>
            <div class="legend">Exercícios Cadastrados</div>
        </div>
        <div class="card">
            <div id="count-trains" class="value">-</div>
            <div class="legend">Treinos Cadastrados</div>
        </div>
    </div>

</main>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="{{ asset('js/home.js') }}"></script>
@endsection