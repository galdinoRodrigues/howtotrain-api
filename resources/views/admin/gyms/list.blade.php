@extends('admin.base')

@section('content')
<main>
    <h1>Academias</h1>

    <div id="gym-list">
        <div class="list-header">
            <div class="form-group search-bar">
                <input id="search" placeholder="Pesquisar..." class="form-control">
                <i id="search-glass" class="zmdi zmdi-search"></i>
            </div>
            <div class="buttons">
                <a href="/admin/gyms/new" class="btn primary" type="submit">Criar Academia</a>
            </div>
        </div>
        <ul class="list">
            <li class="flex header">
                <div class="flex-col-1">#Academia</div>
                <div class="flex-col-4">Nome</div>
                <div class="flex-col-2">Ativa?</div>
            </li>
            @forelse ($gyms as $gym)
                <li class="flex">
                    <div class="flex-col-1">{{ $gym->cd_academia }}</div>
                    <div class="flex-col-4">{{ $gym->registro->ds_nome }}</div>
                    <div class="flex-col-2">{{ $gym->fg_ativo ? 'Sim' : 'Não' }}</div>
                </li>
            @empty
                <p>Nenhuma Academia</p>
            @endforelse
        </ul>
        <div class="pagination">
                @php
                    $from = $gyms->currentPage();
                    if ($from < 4) {
                        $from  = 1;
                    } else {
                        $from = $from - 2;
                    }
                    $diff = $gyms->currentPage();
                    $diff = $gyms->lastPage() - $diff;
                    if ($diff > 4) {
                        $diff = 4;
                    } else if ($diff < 0) {
                        $diff = 0;
                    }
                    $to = $from + $diff;
                    $searchUrl = "";
                    if  ($search){
                        $searchUrl = "&search=".$search;
                    }
                @endphp
            @if ($gyms->previousPageUrl())
                <a href="{{ $gyms->previousPageUrl() }}{{ $searchUrl }}"class="previous"></a>
            @endif
            @for ($i = $from; $i <= $to; $i++)
                @if ($gyms->currentPage() == $i)
                    <a href="/admin/gyms?page={{ $i }}{{ $searchUrl }}" class="page active {{ $i == $from ? 'first' : ''}}">{{$i}}</a>
                @else
                    <a href="/admin/gyms?page={{ $i }}{{ $searchUrl }}" class="page {{ $i == $from ? 'first' : ''}}">{{$i}}</a>
                @endif
            @endfor
            @if ($gyms->nextPageUrl())
                <a href="{{ $gyms->nextPageUrl() }}{{ $searchUrl }}"class="next"></a>
            @endif
        </div>
    </div>

</main>
@endsection
@section('scripts')
<script src="{{ asset('js/app.js') }}"></script>
@endsection