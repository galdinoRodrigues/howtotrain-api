@extends('admin.base')

@section('content')
<main>
    <h1>Academias</h1>
    <form id="create-gym" action="/admin/gyms/new/teste"  method="POST">
        <div class="form-group avatar-picture">
            <div class="avatar big">
                <img src="https://d1a3v8txm37nbo.cloudfront.net/image/filename/2086222/md_Ye_fZmMe2MSkHdMvXgswqWw_dWUoXsFx.jpg" alt="">
            </div>
            <span>Foto da Academia. Máximo de 5mb, formatos aceitos: JPEG, JPG, GIF, PNG</span>
        </div>
        <h4>Informações da Academia</h4>
        <div class="fields">
            <div class="form-group">
                <input
                    class="form-control"
                    type="text"
                    name="name"
                    id="name"
                    required
                    placeholder="Nome da Academia"
                >
            </div>
            <div class="form-group">
                <input
                    class="form-control"
                    type="number"
                    name="subscription-value"
                    id="subscription-value"
                    step="0.01"
                    required
                    placeholder="Valor Por Pessoa"
                >
            </div>
        </div>
        <h4>Endereço</h4>
        <div class="fields">
            <div class="form-group">
                <input
                    class="form-control"
                    type="text"
                    name="street"
                    id="street"
                    required
                    disabled
                    placeholder="Rua"
                >
            </div>
            <div class="form-group">
                <input
                    class="form-control"
                    type="number"
                    name="neighborhood"
                    disabled
                    id="neighborhood"
                    step="0.01"
                    required
                    placeholder="Bairro"
                >
            </div>
        </div>
        <div class="action-group">
            <button class="btn primary md" type="submit">Criar</button>
        </div>
        {{ csrf_field() }}
    </form>
</main>
@endsection
@section('scripts')
<script src="{{ asset('js/app.js') }}"></script>
@endsection