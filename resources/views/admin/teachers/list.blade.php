@extends('admin.base')

@section('content')
<main>
    <h1>Professores</h1>

    <div id="teacher-list">
        <div class="list-header">
            <div class="form-group">
                <input id="search" placeholder="Pesquisar..." class="form-control">
                <i id="search-glass" class="zmdi zmdi-search"></i>
            </div>
        </div>
        <ul class="list">
            <li class="flex header">
                <div class="flex-col-1">#Professor</div>
                <div class="flex-col-1">#Registro</div>
                <div class="flex-col-6">Nome</div>
                <div class="flex-col-6">E-mail</div>
            </li>
            @forelse ($teachers as $teacher)
                <li class="flex">
                    <div class="flex-col-1">{{ $teacher->cd_professor }}</div>
                    <div class="flex-col-1">{{ $teacher->pessoa->registro->cd_registro }}</div>
                    <div class="flex-col-6">{{ $teacher->pessoa->registro->ds_nome }}</div>
                    <div class="flex-col-6">{{ $teacher->pessoa->registro->login->ds_email }}</div>
                </li>
            @empty
                <p>Nenhum Professor</p>
            @endforelse
        </ul>
        <div class="pagination">
                @php
                    $from = $teachers->currentPage();
                    if ($from < 4) {
                        $from  = 1;
                    } else {
                        $from = $from - 2;
                    }
                    $diff = $teachers->currentPage();
                    $diff = $teachers->lastPage() - $diff;
                    if ($diff > 4) {
                        $diff = 4;
                    } else if ($diff < 0) {
                        $diff = 0;
                    }
                    $to = $from + $diff;
                    $searchUrl = "";
                    if  ($search){
                        $searchUrl = "&search=".$search;
                    }
                @endphp
            @if ($teachers->previousPageUrl())
                <a href="{{ $teachers->previousPageUrl() }}{{ $searchUrl }}"class="previous"></a>
            @endif
            @for ($i = $from; $i <= $to; $i++)
                @if ($teachers->currentPage() == $i)
                    <a href="/admin/teachers?page={{ $i }}{{ $searchUrl }}" class="page active {{ $i == $from ? 'first' : ''}}">{{$i}}</a>
                @else
                    <a href="/admin/teachers?page={{ $i }}{{ $searchUrl }}" class="page {{ $i == $from ? 'first' : ''}}">{{$i}}</a>
                @endif
            @endfor
            @if ($teachers->nextPageUrl())
                <a href="{{ $teachers->nextPageUrl() }}{{ $searchUrl }}"class="next"></a>
            @endif
        </div>
    </div>

</main>
@endsection
@section('scripts')
<script src="{{ asset('js/app.js') }}"></script>
@endsection