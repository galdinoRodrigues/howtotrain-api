@extends('admin.base')

@section('content')
<main>
    <h1>Alunos</h1>

    <div id="student-list">
        <div class="list-header">
            <div class="form-group">
                <input id="search" placeholder="Pesquisar..." class="form-control">
                <i id="search-glass" class="zmdi zmdi-search"></i>
            </div>
        </div>
        <ul class="list">
            <li class="flex header">
                <div class="flex-col-1">#Aluno</div>
                <div class="flex-col-1">#Registro</div>
                <div class="flex-col-6">Nome</div>
                <div class="flex-col-6">E-mail</div>
            </li>
            @forelse ($students as $student)
                <li class="flex">
                    <div class="flex-col-1">{{ $student->cd_aluno }}</div>
                    <div class="flex-col-1">{{ $student->pessoa->registro->cd_registro }}</div>
                    <div class="flex-col-6">{{ $student->pessoa->registro->ds_nome }}</div>
                    <div class="flex-col-6">{{ $student->pessoa->registro->login->ds_email }}</div>
                </li>
            @empty
                <p>Nenhum Aluno</p>
            @endforelse
        </ul>
        <div class="pagination">
                @php
                    $from = $students->currentPage();
                    if ($from < 4) {
                        $from  = 1;
                    } else {
                        $from = $from - 2;
                    }
                    $diff = $students->currentPage();
                    $diff = $students->lastPage() - $diff;
                    if ($diff > 4) {
                        $diff = 4;
                    } else if ($diff < 0) {
                        $diff = 0;
                    }
                    $to = $from + $diff;
                    $searchUrl = "";
                    if  ($search){
                        $searchUrl = "&search=".$search;
                    }
                @endphp
            @if ($students->previousPageUrl())
                <a href="{{ $students->previousPageUrl() }}{{ $searchUrl }}"class="previous"></a>
            @endif
            @for ($i = $from; $i <= $to; $i++)
                @if ($students->currentPage() == $i)
                    <a href="/admin/students?page={{ $i }}{{ $searchUrl }}" class="page active {{ $i == $from ? 'first' : ''}}">{{$i}}</a>
                @else
                    <a href="/admin/students?page={{ $i }}{{ $searchUrl }}" class="page {{ $i == $from ? 'first' : ''}}">{{$i}}</a>
                @endif
            @endfor
            @if ($students->nextPageUrl())
                <a href="{{ $students->nextPageUrl() }}{{ $searchUrl }}"class="next"></a>
            @endif
        </div>
    </div>

</main>
@endsection
@section('scripts')
<script src="{{ asset('js/app.js') }}"></script>
@endsection