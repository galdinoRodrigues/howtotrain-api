# Libs
import datetime
import os

# Vars and Constants
PATH = '/var/www/dumps/'
DB_BACKUP_COMMAND = f'mysqldump -u root -h localhost --password=Pq4BqnndWm2l9qgKrdW9A7xXVZBZYhLTIlQYKBbbLwgKhiO287FzBmkB3mGiCMdc howtotrain > {PATH}####.sql'

# Code
def remove_old_files():
    global PATH

    today = datetime.date.today()

    dump_files = os.listdir(PATH)
    for file in dump_files:
        parts = file.replace('.sql', '').split('-')

        if len(parts) != 3:
            continue

        file_day = int(parts[2])
        file_month = int(parts[1])
        file_year = int(parts[0])
        file_date = datetime.date(file_year, file_month, file_day)
        file_days_old = (today - file_date).days

        if file_days_old > 7 or file_days_old < -7:
            os.remove(PATH + file)

    print('Ok Old Files')

def create_dump():
    global DB_BACKUP_COMMAND

    today = datetime.date.today()
    day = today.day
    month = today.month
    year = today.year

    current_dump_file_name = f'{year}-{month}-{day}.sql'
    DB_BACKUP_COMMAND = DB_BACKUP_COMMAND.replace('####.sql', current_dump_file_name)

    os.system(DB_BACKUP_COMMAND)

    print('Ok backup')

if __name__ == '__main__':
    remove_old_files()
    create_dump()
