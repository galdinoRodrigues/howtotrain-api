#!/bin/sh
# db_backup.sh
# 0 0,12 * * * /bin/sh /var/www/html/howToTrain-API/db_backup.sh

DATE=`/bin/date +%d-%m-%Y-%H-%M`
FILEPATH="/var/www/dumps/htt-$DATE.sql"

mysqldump howtotrain > $FILEPATH
python /var/www/html/howToTrain-API/db_backup_organizer.py