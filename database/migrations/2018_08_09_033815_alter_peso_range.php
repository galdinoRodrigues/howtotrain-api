<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPesoRange extends Migration {
    public function up(){
        Schema::table('alunos', function ($table) {
            DB::statement('ALTER TABLE alunos MODIFY nr_peso double(8,4);');
        });
    }
    public function down(){
        Schema::table('alunos', function ($table) {
            DB::statement('ALTER TABLE alunos MODIFY nr_peso double(3,2);');
        });
    }
}
