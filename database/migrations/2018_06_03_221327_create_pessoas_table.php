<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePessoasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoas', function (Blueprint $table) {
            $table->increments('cd_pessoa');
            $table->unsignedInteger('cd_registro');
            $table->foreign('cd_registro')->
            references('cd_registro')->
            on('registros');
            $table->dateTime('dt_nascimento');
            $table->integer('ind_sexo');
            $table->boolean('fg_professor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pessoas');
    }
}
