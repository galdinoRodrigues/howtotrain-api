<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewFieldsExercices extends Migration {
    public function up() {
        Schema::table('grupos', function (Blueprint $table) {
            $table->integer('id_image');
            $table->integer('id_image_info');
        });
        Schema::table('exercicios', function (Blueprint $table) {
            $table->string('cd_video');
            $table->string('url_thumbnails_default');
            $table->string('url_thumbnails_medium');
            $table->string('url_thumbnails_high');
        });
    }
    public function down() {
        Schema::table('grupos', function (Blueprint $table) {
            $table->dropColumn('id_image');
            $table->dropColumn('id_image_info');
        });
        Schema::table('exercicios', function (Blueprint $table) {
            $table->dropColumn('cd_video');
            $table->dropColumn('url_thumbnails_default');
            $table->dropColumn('url_thumbnails_medium');
            $table->dropColumn('url_thumbnails_high');
        });
    }
}
