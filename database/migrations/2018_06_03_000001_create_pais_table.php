<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaisTable extends Migration {
    public function up() {
        Schema::create('pais', function (Blueprint $table) {
            $table->increments('cd_pais');
            $table->string('ds_pais', 100);
        });
    }
    public function down() {
        Schema::dropIfExists('pais');
    }
}
