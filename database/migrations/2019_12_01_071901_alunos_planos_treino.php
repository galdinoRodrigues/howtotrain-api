<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlunosPlanosTreino extends Migration {
    public function up() {
        Schema::create('aluno_plano', function (Blueprint $table) {
            $table->dateTime('dt_inclusao');
            $table->dateTime('dt_expiracao');
            $table->unsignedInteger('cd_plano');
            $table->foreign('cd_plano')->
                    references('cd_plano')->
                    on('planos');
            $table->unsignedInteger('cd_aluno');
            $table->foreign('cd_aluno')->
                    references('cd_aluno')->
                    on('alunos');
        });
    }
    public function down() {
        Schema::dropIfExists('plano_treino');
    }
}