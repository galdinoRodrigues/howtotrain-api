<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TreinoProfessor extends Migration {
    public function up() {
        Schema::table('treinos', function (Blueprint $table) {
            $table->unsignedInteger('cd_professor')->nullable();
            $table->foreign('cd_professor')->
                    references('cd_professor')->
                    on('professores');
        });
    }
    public function down() {
        Schema::table('treinos', function (Blueprint $table) {
            $table->dropForeign('treinos_cd_professor_foreign');
            $table->dropColumn('cd_professor');
        });
    }
}
