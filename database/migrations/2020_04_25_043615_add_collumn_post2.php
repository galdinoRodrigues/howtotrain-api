<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnPost2 extends Migration
{
    public function up()
    {
        Schema::table('post', function (Blueprint $table) {
            $table->unsignedInteger('holder_id');

            $table->unsignedInteger('type_id');
            $table->foreign('type_id')->
                    references('id')->
                    on('post_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post', function (Blueprint $table) {
            $table->dropForeign('post_type_id_foreign');
            $table->dropColumn('type_id');
            $table->dropColumn('holder_id');
        });
    }
}
