<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlunoEmpresaInvite2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aluno_empresa_invite', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('id_empresa');
            $table->foreign('id_empresa')->
            references('id')->
            on('empresa');
            
            $table->unsignedInteger('id_aluno');
            $table->foreign('id_aluno')->
            references('cd_aluno')->
            on('alunos');

            $table->string('uuid',40);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aluno_empresa_invite');
    }
}
