<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GroupPic extends Migration {
    public function up() {
        Schema::table('grupos', function (Blueprint $table) {
            $table->text('ds_url')->nullable();
        });
    }
    public function down() {
        Schema::table('grupos', function (Blueprint $table) {
            $table->dropColumn('ds_url');
        });
    }
}
