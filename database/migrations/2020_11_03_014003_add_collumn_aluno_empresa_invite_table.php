<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnAlunoEmpresaInviteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aluno_empresa_invite', function (Blueprint $table) {
            $table->unsignedInteger('id_professor')->nullable();
            $table->foreign('id_professor')->
            references('cd_professor')->
            on('professores');

            $table->unsignedInteger('id_aluno')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
