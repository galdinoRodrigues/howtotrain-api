<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTrainingTableCollumns3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('treinos', function (Blueprint $table) {
            $table->integer('ind_tipo_treino')->nullable()->change();
            $table->integer('ind_nivel_treino')->nullable()->change();
            $table->string('ds_nome', 220)->nullable()->change();
            $table->string('ds_picture')->nullable()->change();

            $table->dropColumn('fg_carga');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
