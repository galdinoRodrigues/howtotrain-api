<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TreinoExercicio extends Migration {
    public function up() {
        Schema::create('treino_exercicio', function (Blueprint $table) {
            $table->increments('cd_treino_exercicio');
            $table->unsignedInteger('nr_ordem');
            $table->unsignedInteger('cd_treino');
            $table->foreign('cd_treino')->
                    references('cd_treino')->
                    on('treinos');
            $table->unsignedInteger('cd_exercicio');
            $table->foreign('cd_exercicio')->
                    references('cd_exercicio')->
                    on('exercicios');
        });
    }
    public function down() {
        Schema::dropIfExists('treino_exercicio');
    }
}