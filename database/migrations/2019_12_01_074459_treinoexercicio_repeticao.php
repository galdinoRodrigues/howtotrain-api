<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TreinoexercicioRepeticao extends Migration {
    public function up() {
        Schema::create('treino_exercicio_repeticao', function (Blueprint $table) {
            $table->unsignedInteger('cd_treino_exercicio');
            $table->foreign('cd_treino_exercicio')->
                    references('cd_treino_exercicio')->
                    on('treino_exercicio');
            $table->unsignedInteger('nr_repeticoes');
        });
    }
    public function down() {
        Schema::dropIfExists('treino_exercicio_repeticao');
    }
}

