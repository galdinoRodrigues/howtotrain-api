<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcademiasTable extends Migration {
    public function up() {
        Schema::create('academias', function (Blueprint $table) {
            $table->increments('cd_academia');
            $table->unsignedInteger('cd_registro');
            $table->foreign('cd_registro')->
                    references('cd_registro')->
                    on('registros');
            $table->integer('cd_academia_externo');
            $table->boolean('fg_ativo');
        });
    }
    public function down() {
        Schema::dropIfExists('academias');
    }
}
