<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExerciciosFields extends Migration {
    public function up() {
        Schema::table('exercicios', function (Blueprint $table) {
            $table->string('cd_video')->nullable()->change();
            $table->string('ds_nome_logico')->nullable()->change();
            $table->text('ds_exercicio')->nullable()->change();

            $table->string('ds_nome_logico_gif_exercicio')->nullable();
            $table->string('ds_nome_logico_foto_exercicio')->nullable();
            $table->string('url_thumbnails_default')->nullable()->change();
            $table->string('url_thumbnails_medium')->nullable()->change();
            $table->string('url_thumbnails_high')->nullable()->change();
        });
    }
    public function down() {
        Schema::table('exercicios', function (Blueprint $table) {
            $table->dropColumn('ds_nome_logico_gif_exercicio');
            $table->dropColumn('ds_nome_logico_foto_exercicio');
        });
    }
}
