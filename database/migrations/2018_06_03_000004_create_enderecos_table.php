<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnderecosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enderecos', function (Blueprint $table) {
            $table->increments('cd_endereco');
            $table->unsignedInteger('cd_cidade');
            $table->foreign('cd_cidade')->references('cd_cidade')->on('cidades');
            $table->string('ds_ut',2);
            $table->string('ds_cidade',45);
            $table->string('ds_bairro', 45);
            $table->string('ds_endereco', 45);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enderecos');
    }
}
