<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TreinoExercicioRepeticoes extends Migration {
    public function up() {
        Schema::table('treino_exercicio', function (Blueprint $table) {
            $table->string('nr_repeticoes')->nullable();
        });
    }
    public function down() {
        Schema::table('treino_exercicio', function (Blueprint $table) {
            $table->dropColumn('nr_repeticoes');
        });
    }
}
