<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Professor extends Migration {
    public function up() {
        Schema::create('professores', function (Blueprint $table) {
            $table->increments('cd_professor');
            $table->unsignedInteger('cd_pessoa');
            $table->foreign('cd_pessoa')->
                    references('cd_pessoa')->
                    on('pessoas');

            $table->string('ds_cref');
            $table->string('dt_cref');
            $table->double('nr_valor_mensal', 8,2);

            $table->unsignedInteger('cd_academia')->nullable();
            $table->foreign('cd_academia')->
                    references('cd_academia')->
                    on('academias');

        });
    }

    public function down() {
        Schema::dropIfExists('professores');
    }
}
