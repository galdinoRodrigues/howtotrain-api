<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveExternalGym extends Migration {
    public function up() {
        Schema::table('academias', function (Blueprint $table) {
            $table->dropColumn('cd_academia_externo');
        });
    }
    public function down() {
        Schema::table('academias', function (Blueprint $table) {
            $table->integer('cd_academia_externo');
        });
    }
}