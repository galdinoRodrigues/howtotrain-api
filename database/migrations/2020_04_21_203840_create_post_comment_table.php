<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_comment', function (Blueprint $table) {
            $table->increments('id');
            $table->text('comment');
            $table->timestamps();

            $table->unsignedInteger('cd_pessoa');
            $table->foreign('cd_pessoa')->
                    references('cd_pessoa')->
                    on('pessoas');

            $table->unsignedInteger('post_comment_id')->nullable();
            $table->foreign('post_comment_id')->
                    references('id')->
                    on('post_comment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_comment');
    }
}
