<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Avatar extends Migration {
    public function up() {
        Schema::table('registros', function (Blueprint $table) {
            $table->string('ds_avatar')->nullable();
        });
    }
    public function down() {
        Schema::table('registros', function (Blueprint $table) {
            $table->dropColumn('ds_avatar');
        });
    }
}
