<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePostTable extends Migration
{
    public function up() {
        Schema::table('post', function (Blueprint $table) {
            $table->unsignedInteger('product_id')->nullable();
            $table->foreign('product_id')->
                    references('id')->
                    on('product');

            $table->unsignedInteger('post_comment_id')->nullable();
            $table->foreign('post_comment_id')->
                    references('id')->
                    on('post_comment');

            $table->unsignedInteger('post_like_id')->nullable();
            $table->foreign('post_like_id')->
                    references('id')->
                    on('post_like');
        });
    }
    public function down() {
        Schema::table('post', function (Blueprint $table) {
            $table->dropColumn('product_id');
            $table->dropColumn('post_comment_id');
            $table->dropColumn('post_like_id');
        });
    }
}
