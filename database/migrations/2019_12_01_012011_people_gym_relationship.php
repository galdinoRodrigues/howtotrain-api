<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PeopleGymRelationship extends Migration {
    public function up() {
        Schema::table('pessoas', function (Blueprint $table) {
            $table
                ->unsignedInteger('cd_academia')
                ->nullable();
            $table
                ->foreign('cd_academia')
                ->references('cd_academia')
                ->on('academias');
        });
    }
    public function down() {
        Schema::table('pessoas', function (Blueprint $table) {
            $table->dropForeign('pessoas_cd_academia_foreign');
            $table->dropColumn('cd_academia');
        });
    }
}
