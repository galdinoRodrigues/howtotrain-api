<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PessoaEmpresa extends Migration
{
    public function up()
    {
        Schema::table('pessoas', function (Blueprint $table)
        {
            $table
                    ->unsignedInteger('cd_empresa')
                    ->nullable();
            $table
                ->foreign('cd_empresa')
                ->references('id')
                ->on('empresa');
        });
    }

    public function down()
    {
        Schema::table('pessoas', function (Blueprint $table)
        {
            $table->dropForeign('pessoas_cd_empresa_foreign');
            $table->dropColumn('cd_empresa');
        });
    }
}
