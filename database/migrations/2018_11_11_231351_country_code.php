<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CountryCode extends Migration {
    public function up() {
        Schema::table('pais', function (Blueprint $table) {
            $table->string('ds_codigo', 6);
        });
    }
    public function down() {
        Schema::table('pais', function (Blueprint $table) {
            $table->dropColumn('ds_codigo');
        });
    }
}
