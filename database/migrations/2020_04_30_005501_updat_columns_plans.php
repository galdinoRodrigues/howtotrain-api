<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatColumnsPlans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planos', function($table)
        {
            $table->text('ds_plano')->nullable()->change();
            $table->unsignedInteger('ind_tipo_treino')->nullable()->change();
            $table->unsignedInteger('ind_nivel_treino')->nullable()->change();
            $table->unsignedInteger('nr_validade_dias')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
  
    }
}
