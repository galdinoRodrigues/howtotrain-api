<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registros', function (Blueprint $table) {
            $table->increments('cd_registro');
            $table->unsignedInteger('cd_endereco');
            $table->foreign('cd_endereco')->references('cd_endereco')->on('enderecos');
            $table->string('ds_nome',220);
            $table->integer('ind_registro');
            $table->integer('fg_excluido');
            $table->dateTime('dt_inclusao');
            $table->dateTime('dt_alteracao');
            $table->dateTime('dt_ultima_utilizacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registros');
    }
}
