<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Friends extends Migration {
    public function up() {
        Schema::create('amizades', function (Blueprint $table) {
            $table->increments('cd_amizade');
            $table->unsignedInteger('cd_from');
            $table->foreign('cd_from')->
                    references('cd_pessoa')->
                    on('pessoas');
            $table->unsignedInteger('cd_to');
            $table->foreign('cd_to')->
                references('cd_pessoa')->
                on('pessoas');
            $table->boolean('fg_aceito')->default(false);
            $table->dateTime('dt_inclusao')->default(DB::raw('NOW()'));
            $table->dateTime('dt_alteracao')->nullable();
        });
    }
    public function down() {
        Schema::dropIfExists('amizades');
    }
}
