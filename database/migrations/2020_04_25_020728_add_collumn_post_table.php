<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnPostTable extends Migration
{
    public function up() {
        Schema::table('post', function (Blueprint $table) {
            $table->unsignedInteger('empresa_id')->nullable();
            $table->foreign('empresa_id')->
                    references('id')->
                    on('empresa');
        });
    }
    public function down() {
        Schema::table('post', function (Blueprint $table) {
            $table->dropColumn('empresa_id');
        });
    }
}
