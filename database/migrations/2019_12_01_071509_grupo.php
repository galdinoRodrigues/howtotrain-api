<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Grupo extends Migration {
    public function up() {
        Schema::create('grupos', function (Blueprint $table) {
            $table->increments('cd_grupo');
            $table->string('ds_nome', 45);
        });
    }
    public function down() {
        Schema::dropIfExists('grupos');
    }
}
