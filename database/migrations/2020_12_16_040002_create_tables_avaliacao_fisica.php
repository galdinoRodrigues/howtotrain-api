<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesAvaliacaoFisica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliacao', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('idade');
            $table->unsignedInteger('masculino');

            $table->double('peso',5,2);
            $table->double('gordura',5,2);
            $table->double('massa_magra',5,2);
            $table->double('massa_gorda',5,2);

            $table->unsignedInteger('percentual_medio_minimo');
            $table->unsignedInteger('percentual_medio_maximo');
            $table->timestamps(); 
            
            $table->unsignedInteger('aluno_id');
            $table->foreign('aluno_id')->
                    references('cd_aluno')->
                    on('alunos');

            $table->unsignedInteger('professor_id');
            $table->foreign('professor_id')->
                    references('cd_professor')->
                    on('professores');
        });

        Schema::create('dobras_cultaneas', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->double('value',5,2);

            $table->unsignedInteger('id_avaliacao');
            $table->foreign('id_avaliacao')->
                    references('id')->
                    on('avaliacao');
        });

        Schema::create('perimetros', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->double('valueA',5,2);
            $table->double('valueB',5,2);

            $table->unsignedInteger('id_avaliacao');
            $table->foreign('id_avaliacao')->
                    references('id')->
                    on('avaliacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dobras_cultaneas');
        Schema::dropIfExists('perimetros');
        Schema::dropIfExists('avaliacao');
    }
}
