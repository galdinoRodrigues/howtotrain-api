<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLikeCommenTable extends Migration
{
    public function up() {
        Schema::table('post_comment', function (Blueprint $table) {
            $table->unsignedInteger('post_id')->nullable();
            $table->foreign('post_id')->
                    references('id')->
                    on('post');

        });

        Schema::table('post_like', function (Blueprint $table) {
            $table->unsignedInteger('post_id')->nullable();
            $table->foreign('post_id')->
                    references('id')->
                    on('post');

        });
    }
    
    public function down() {
        Schema::table('post_comment', function (Blueprint $table) {
            $table->dropColumn('post_id');
        });

        Schema::table('post_like', function (Blueprint $table) {
            $table->dropColumn('post_id');
        });
    }
}
