<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveGymRelationships extends Migration {
    public function up() {
        Schema::table('alunos', function (Blueprint $table) {
            $table->dropForeign('alunos_cd_academia_foreign');
            $table->dropColumn('cd_academia');
        });
        Schema::table('professores', function (Blueprint $table) {
            $table->dropForeign('professores_cd_academia_foreign');
            $table->dropColumn('cd_academia');
        });
    }
    public function down() {
        Schema::table('alunos', function (Blueprint $table) {
            $table->unsignedInteger('cd_academia')->nullable();
            $table->foreign('cd_academia')->
                    references('cd_academia')->
                    on('academias');
        });
        Schema::table('professores', function (Blueprint $table) {
            $table->unsignedInteger('cd_academia')->nullable();
            $table->foreign('cd_academia')->
                    references('cd_academia')->
                    on('academias');
        });
    }
}
