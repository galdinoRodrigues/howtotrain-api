<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlunosTable extends Migration {
    public function up() {
        Schema::create('alunos', function (Blueprint $table) {
            $table->increments('cd_aluno');
            $table->unsignedInteger('cd_pessoa');
            $table->foreign('cd_pessoa')->
                    references('cd_pessoa')->
                    on('pessoas');
            $table->unsignedInteger('cd_academia');
            $table->foreign('cd_academia')->
                    references('cd_academia')->
                    on('academias');
            $table->integer('cd_aluno_externo');
            $table->double('nr_peso', 3,2);
            $table->integer('ind_condicao_fisica');
            $table->integer('ind_tipo_treino');
            $table->integer('ind_nivel_experiencia');
        });
    }
    public function down() {
        Schema::dropIfExists('alunos');
    }
}
