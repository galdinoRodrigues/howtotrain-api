<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MusculoExercicios extends Migration {
    public function up() {
        Schema::create('musculo_exercicio', function (Blueprint $table) {
            $table->increments('cd_musculo_exercicio');
            $table->unsignedInteger('cd_musculo');
            $table->foreign('cd_musculo')->
                    references('cd_musculo')->
                    on('musculos');
            $table->unsignedInteger('cd_exercicio');
            $table->foreign('cd_exercicio')->
                references('cd_exercicio')->
                on('exercicios');
            $table->unsignedInteger('ind_primario_secundario');
        });
    }
    public function down() {
        Schema::dropIfExists('musculos_exercicios');
    }
}
