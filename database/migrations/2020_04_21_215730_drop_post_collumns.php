<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropPostCollumns extends Migration
{
    public function up() {
        Schema::table('post', function (Blueprint $table) {
            $table->dropForeign(['post_comment_id']);
            $table->dropForeign(['post_like_id']);
            $table->dropColumn('post_comment_id');
            $table->dropColumn('post_like_id');
        });
    }
    public function down() {
        Schema::table('post', function (Blueprint $table) {
           
        });
    }
}
