<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnPost4 extends Migration
{
    public function up()
    {
       
        Schema::table('post', function (Blueprint $table) {
            $table->unsignedInteger('gym_id')->nullable();
            $table->foreign('gym_id')->
                    references('cd_academia')->
                    on('academias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropColumn('gym_id');
    }
}
