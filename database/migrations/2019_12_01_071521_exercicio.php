<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Exercicio extends Migration {
    public function up() {
        Schema::create('exercicios', function (Blueprint $table) {
            $table->increments('cd_exercicio');
            $table->string('ds_nome', 220);
            $table->string('ds_nome_logico', 220);
            $table->text('ds_exercicio');
            $table->unsignedInteger('cd_grupo')->nullable();
            $table->foreign('cd_grupo')->
                    references('cd_grupo')->
                    on('grupos');
        });
    }
    public function down() {
        Schema::dropIfExists('exercicios');
    }
}