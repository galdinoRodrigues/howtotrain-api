<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cd_pessoa')->nullable();
            $table->foreign('cd_pessoa')->
                    references('cd_pessoa')->
                    on('pessoas');
            $table->text('description');
            $table->string('picture_url',220);
            $table->unsignedInteger('qtd_like')->default(0);
            $table->unsignedInteger('qtd_comment')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post');
    }
}
