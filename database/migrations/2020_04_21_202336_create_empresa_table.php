<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fg_excluido');
            $table->integer('fg_ativo');
            $table->string('nome',220);
            $table->timestamps();

            $table->unsignedInteger('cd_endereco');
            $table->foreign('cd_endereco')->
                    references('cd_endereco')->
                    on('enderecos');

            $table->unsignedInteger('id_tipo_empresa');
            $table->foreign('id_tipo_empresa')->
                    references('id')->
                    on('tipo_empresa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa');
    }
}
