<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Treino extends Migration {
    public function up() {
        Schema::create('treinos', function (Blueprint $table) {
            $table->increments('cd_treino');
            $table->string('ds_nome', 220);
            $table->string('ds_picture');
            $table->boolean('fg_carga');
            $table->unsignedInteger('ind_tipo_treino');
            $table->unsignedInteger('ind_nivel_treino');
            $table->unsignedInteger('ind_sexo');
            $table->unsignedInteger('cd_grupo')->nullable();
            $table->foreign('cd_grupo')->
                    references('cd_grupo')->
                    on('grupos');
        });
    }
    public function down() {
        Schema::dropIfExists('treinos');
    }
}