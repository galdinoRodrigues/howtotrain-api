<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlunoProfessor extends Migration {
    public function up() {
        Schema::create('aluno_professor', function (Blueprint $table) {
            $table->unsignedInteger('cd_aluno');
            $table->foreign('cd_aluno')->
                    references('cd_aluno')->
                    on('alunos');
            $table->unsignedInteger('cd_professor');
            $table->foreign('cd_professor')->
                    references('cd_professor')->
                    on('professores');
        });
    }
    public function down() {
        Schema::dropIfExists('aluno_professor');
    }
}
