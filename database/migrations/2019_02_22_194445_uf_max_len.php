<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UfMaxLen extends Migration {
    public function up() {
        Schema::table('enderecos', function (Blueprint $table) {
            $table->string('ds_ut', 20)->change();
        });
    }
    public function down() {
        Schema::table('enderecos', function (Blueprint $table) {
            $table->string('ds_ut',2)->change();
        });
    }
}
