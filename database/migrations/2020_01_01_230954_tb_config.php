<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbConfig extends Migration {
    public function up() {
        Schema::create('config', function (Blueprint $table) {
            $table->string('api_version')->default('1.0.0');
            $table->dateTime('last_date_update_exercise')->default(DB::raw('NOW()'));
        });
    }
    public function down() {
        Schema::dropIfExists('config');
    }
}
