<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEmpresaTable extends Migration
{
    public function up() {
        Schema::table('empresa', function (Blueprint $table) {
            $table->string('url_imagem',220);
        });
    }
    public function down() {
        Schema::table('empresa', function (Blueprint $table) {
            $table->dropColumn('url_imagem');
        });
    }
}
