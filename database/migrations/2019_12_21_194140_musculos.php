<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Musculos extends Migration {
    public function up() {
        Schema::create('musculos', function (Blueprint $table) {
            $table->increments('cd_musculo');
            $table->string('ds_musculo', 220);
        });
    }
    public function down() {
        Schema::dropIfExists('musculos');
    }
}
