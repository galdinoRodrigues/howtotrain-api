<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanOriginalCd extends Migration {
    /**
     * Run the migrations.
     * @return void
     */
    public function up() {
        Schema::table('planos', function (Blueprint $table) {
            $table->unsignedInteger('cd_plano_original')->nullable();
            $table->foreign('cd_plano_original')->
                references('cd_plano')->
                on('planos');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down() {
        Schema::table('planos', function (Blueprint $table) {
            $table->dropForeign('planos_cd_plano_original_foreign');
            $table->dropColumn('cd_plano_original');
        });
    }
}
