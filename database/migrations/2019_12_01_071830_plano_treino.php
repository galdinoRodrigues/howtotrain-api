<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanoTreino extends Migration {
    public function up() {
        Schema::create('planos', function (Blueprint $table) {
            $table->increments('cd_plano');
            $table->string('ds_nome', 220);
            $table->text('ds_plano');
            $table->unsignedInteger('ind_tipo_treino');
            $table->unsignedInteger('ind_nivel_treino');
            $table->unsignedInteger('nr_validade_dias');
            $table->unsignedInteger('cd_professor')->nullable();
            $table->foreign('cd_professor')->
                    references('cd_professor')->
                    on('professores');
        });
    }
    public function down() {
        Schema::dropIfExists('planos');
    }
}