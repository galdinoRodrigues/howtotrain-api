<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostFields extends Migration
{
    public function up()
    {
        Schema::table('post', function (Blueprint $table)
        {
            $table->dropColumn('holder_id');

            $table->unsignedInteger('cd_empresa')->nullable(true);
            $table->foreign('cd_empresa')->
                    references('id')->
                    on('empresa');

            $table->unsignedInteger('cd_pessoa')->nullable(true);
            $table->foreign('cd_pessoa')->
                    references('cd_pessoa')->
                    on('pessoas');

            $table->unsignedInteger('cd_produto')->nullable(true);
            $table->foreign('cd_produto')->
                    references('id')->
                    on('product');

            $table->dropForeign('post_type_id_foreign');
            $table->dropColumn('type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post', function (Blueprint $table)
        {
            $table->dropForeign('post_cd_empresa_foreign');
            $table->dropColumn('cd_empresa');

            $table->dropForeign('post_cd_pessoa_foreign');
            $table->dropColumn('cd_pessoa');

            $table->dropForeign('post_cd_produto_foreign');
            $table->dropColumn('cd_produto');

            $table->unsignedInteger('holder_id');

            $table->unsignedInteger('type_id')->nullable(true);
            $table->foreign('type_id')->
                    references('id')->
                    on('post_type');
        });
    }
}
