<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GymValuePerPerson extends Migration {
    public function up() {
        Schema::table('academias', function (Blueprint $table) {
            $table->double('price')->default(1);
        });
    }
    public function down() {
        Schema::table('academias', function (Blueprint $table) {
            $table->dropColumn('price');
        });
    }
}
