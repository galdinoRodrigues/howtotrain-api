<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanoTreinoTreinos extends Migration {
    public function up() {
        Schema::create('plano_treino', function (Blueprint $table) {
            $table->unsignedInteger('cd_plano');
            $table->foreign('cd_plano')->
                    references('cd_plano')->
                    on('planos');
            $table->unsignedInteger('cd_treino');
            $table->foreign('cd_treino')->
                    references('cd_treino')->
                    on('treinos');
        });
    }
    public function down() {
        Schema::dropIfExists('plano_treino');
    }
}
