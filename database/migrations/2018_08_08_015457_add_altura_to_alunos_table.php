<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlturaToAlunosTable extends Migration {
    public function up() {
        Schema::table('alunos', function (Blueprint $table) {
            $table->integer('cm_altura');
        });
    }
    public function down() {
        Schema::table('alunos', function (Blueprint $table) {
            $table->dropColumn('cm_altura');
        });
    }
}
