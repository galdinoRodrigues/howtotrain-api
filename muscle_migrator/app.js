const fs = require('fs');
const mysql = require('mysql');
const path = require('path');

const dbTextContent = fs.readFileSync(path.join(__dirname , 'old_db.json'));
const json = JSON.parse(dbTextContent);

const conn = mysql.createConnection({
    host: 'vps8336.publiccloud.com.br',
    user: 'migdbuser',
    password: 'Pq4BqnndWm2l9qgKrdW9A7xXVZBZYhLTIlQYKBbbLwgKhiO287FzBmkB3mGiCMdc',
    database: 'howtotrain'
});

// const conn = mysql.createConnection({
//     host: process.env.HOST,
//     user: process.env.USER,
//     password: process.env.PASSWORD,
//     database: process.env.DATABASE
// });

function get(table, fields, condition) {
    return new Promise((resolve, reject) => {
        try {
            const query = `
            SELECT
                ${fields.join(', ')}
            FROM
                ${table}
            WHERE
                ${
                    condition.map(c => {
                        if (typeof Object.entries(c)[0][1] === 'string') {
                            return `${Object.entries(c)[0][0]} = '${Object.entries(c)[0][1]}'`;
                        } else {
                            return `${Object.entries(c)[0][0]} = ${Object.entries(c)[0][1]}`;
                        }
                    })
                    .join(' AND ')
                }
            ;`;
            return conn.query(query, (error, results, _fields) => {
                if (error) {
                    return reject(error);
                }
                if (!results || results.length === 0) {
                    resolve();
                }
                resolve(results[0]);
            });
        } catch(e) {
            reject(e);
        }
    });
}


function insertGetId(table, data) {
    return new Promise((resolve, reject) => {
        try {
            const query = `INSERT INTO ${table} SET ?;`;
            return conn.query(query, data, (error, results, _fields) => {
                if (error) {
                    return reject(error);
                }
                if (!results || !results.insertId) {
                    reject();
                }
                resolve(results.insertId);
            });
        } catch(e) {
            reject(e);
        }
    });
}

conn.connect(async error => {
    if(error) {
        return console.error('>>>> Could not connect to db');
    }
    setTimeout(() => conn.end(), 300000);
    const muscles = [];

    for (const item of json) {
        const ex = await get(
            'exercicios',
            ['cd_exercicio', 'ds_nome', 'cd_grupo'],
            [ { 'cd_exercicio': item['ID']} ]
        );
        if (
            ex.cd_grupo !== parseInt(item['grupoMuscular']['ID']) ||
            ex.ds_nome !== item['nome']
        ) {
            console.error('>>>> Found diff!!!');
            conn.end();
            process.exit();
        }

        const rawMuscles = item['mListMusculos'];

        const tempMuscles = [];
        for (const muscle of rawMuscles) {
            let numMuscles = 1;
            if (!muscle.nome) {
                continue;
            }
            if (muscle.nome.includes(',')) {
                numMuscles = muscle.nome.split(',').length;
            }
            for (let i = 0; i < numMuscles; i ++) {
                tempMuscles.push({
                    indPrimarioSecundario: parseInt(muscle.indPrimarioSecundario),
                    nome: muscle.nome.split(',')[i].trim()
                });
            }
        }

        for (const muscle of tempMuscles) {
            const index = muscles.findIndex(m => m.nome === muscle.nome);
            if (index > -1) {   
                const indexEx = muscles[index].exercises.findIndex(e => e.id === parseInt(item['ID']));
                if (indexEx > -1) {
                    if (muscle.indPrimarioSecundario === 1) {
                        muscles[index].exercises[indexEx].primario = true;
                    } else if (muscle.indPrimarioSecundario === 2) {
                        muscles[index].exercises[indexEx].secundario = true;
                    }
                } else {
                    muscles[index].exercises.push({
                        id: parseInt(item['ID']),
                        primario: muscle.indPrimarioSecundario === 1,
                        secundario: muscle.indPrimarioSecundario === 2
                    });
                }
            } else {
                muscles.push({
                    nome: muscle.nome,
                    exercises: [{
                        id: parseInt(item['ID']),
                        primario: muscle.indPrimarioSecundario === 1,
                        secundario: muscle.indPrimarioSecundario === 2
                    }]
                });
            }
        }
    }
    console.log('>>>> Gathered all muscles')
    for (muscle of muscles) {
        const dbMuscle = await get('musculos', ['cd_musculo'], [{ ds_musculo: muscle.nome }]);
        let id = null;
        if (!dbMuscle) {
            id = await insertGetId('musculos', { 'ds_musculo': muscle.nome });
        } else {
            id = dbMuscle.cd_musculo;
        }

        for (ex of muscle.exercises) {
            if (ex.primario) {
                const hasRel = await get(
                    'musculo_exercicio',
                    ['cd_musculo_exercicio'], [
                        { cd_musculo: id },
                        { cd_exercicio: ex.id },
                        { ind_primario_secundario: 1 }
                    ]);

                if (!hasRel) {
                    await insertGetId('musculo_exercicio', {
                        cd_musculo: id,
                        cd_exercicio: ex.id,
                        ind_primario_secundario: 1
                    });
                }
            }
            if (ex.secundario) {
                const hasRel = await get(
                    'musculo_exercicio',
                    ['cd_musculo_exercicio'], [
                        { cd_musculo: id },
                        { cd_exercicio: ex.id },
                        { ind_primario_secundario: 2 }
                    ]);

                if (!hasRel) {
                    await insertGetId('musculo_exercicio', {
                        cd_musculo: id,
                        cd_exercicio: ex.id,
                        ind_primario_secundario: 2
                    });
                }
            }
        }
    }
    console.log('>>>> FINISHED');

    conn.end();
    process.exit();
});

