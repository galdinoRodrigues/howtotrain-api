# HowToTrain - Seu Treino em suas mãos

> Este projeto contém a API utilizada pelo aplicativo e o Website principal do HowToTrain.
___

## Rodando o Projeto

É possível rodar o projeto de duas maneiras, utilizando o **Docker** ou a sua própria máquina.

## DOCKER
Certifique-se que você tem instalado em sua máquina tanto o **Docker** quanto o **Docker Compose**.
Entre na raiz do projeto pelo seu terminal e use os seguintes comandos:

### Rodar o Projeto
`Docker-compose up -d`

### Parar a execução do Projeto
`Docker-compose down`

### Listar os Containers Rodando
`Docker ps`

### Listar todos os Containers
`Docker ps -a`

## Instalando direto na máquina

composer install (pra instalar dependencias)
php composer.phar install

___

## Banco de Dados e Migrations

1 - Criar migrations
php artisan make:migration <Nome>

EX
php artisan make:migration CreatePostTable

2 - Alterar a migration conforme quiser

3 - Entrar no container, para testar local: docker exec -it <nome do container> sh
3.1 - Docker ps lista os containers

4 - subir a migration local
php artisan migrate

5 - dar push, e pull no server e rodar lá pra subir em prod

___

## Criando novos Endpoints

1 - Cria a controler
2 - add a rota em routes/api.php
Classe+metodo

3 - alterar o código conforme quiser
	4.1 - Quando estiver ok o código, subir pra master

4 - acessar o serviço
	5.1 - se nao tiver feito ainda, tornar o arquivo executável
	chmod +x ssh

Executar na pasta
./ssh
senha no whats, grupo serviços

6 - acessar a pasta no Caminho do projeto no servidor
sudo su
cd /var/www/howtotrain-api/

Merge
git pull origin dev

---
entrar no banco pelo ssh

sudo su
mysql -u root --password=senha howtotrain

