<?php

use Illuminate\Http\Request;

// * V1
Route::post('criarRegistro','RegisterController@createRegister');
Route::post('login','AuthController@login');
Route::post('esqueciSenha','LoginController@gerarCodigo');
Route::post('mudarSenha','LoginController@validarSenha');

Route::get('aluno/{id}','AlunoController@getAluno');
Route::put('aluno/{id}','AlunoController@completeData');
Route::post('emailContato','EmailController@enviarEmailContato')->middleware('cors');

Route::post('playlists','GoogleController@playlist');

// * V2
Route::group(['prefix' => 'v2'], function ()
{
    Route::post('login','AuthController@loginV2');

    Route::put('professor/{cd_registro}','ProfessorController@completeDataV2');
    Route::put('aluno/{cd_registro}','AlunoController@completeDataV2');

    Route::get('gyms', 'AcademiaController@listGyms');

    Route::get('exercises/{cd_exercise}', 'ExerciseController@getExerciseV2');
    Route::get('exercises', 'ExerciseController@listExercises');

    Route::get('groups', 'GroupController@listGroups');

    Route::post('academia/validarId', 'AcademiaController@validarId');
    Route::post('aluno/validarInvite', 'AlunoController@validarInvite');

    Route::group(['middleware' => 'jwt'], function()
    {
        Route::get('profile','PessoaController@getProfileV2');

        Route::get('professor/trains', 'TrainController@listFromTeacher');
        Route::post('professor/train', 'TrainController@create');
        Route::get('professor/train/{cd_treino}/exercises', 'TrainController@getTrainingExercises');

        Route::get('professor/plans', 'PlanController@listFromTeacher');
        Route::post('professor/plans', 'PlanController@create');
        Route::post('professor/plans/trains', 'PlanController@attachTrain');
        Route::post('professor/plans/attach-to-student', 'PlanController@attachPlanToStudent');
        Route::get('professor/plans/{cd_plano}', 'PlanController@detailFromTeacher');

        Route::get('professor/home', 'ProfessorController@homeData');
        Route::get('professor/alunos', 'ProfessorController@listarAlunosV2');
        Route::get('professores', 'ProfessorController@listAllTeachers');
        Route::get('professor/{cd_professor}','ProfessorController@getProfessorV2');
        Route::post('avaliacaoFisica','AvaliacaoControler@create');
        Route::get('avaliacaoFisica/aluno/{cd_aluno}','AvaliacaoControler@list');
        Route::get('avaliacaoFisica/{avaliacao_id}','AvaliacaoControler@detail');
        
        Route::get('aluno/plans/{cd_plano}', 'PlanController@detailFromStudent');
        Route::get('aluno/plans', 'PlanController@listFromStudent2');
        Route::get('aluno/{cd_aluno}', 'AlunoController@getAlunoV2');
        Route::get('alunos', 'AlunoController@listAllStudents');

        Route::get('notifications', 'NotificationController@list');

        Route::get('friends', 'FriendshipController@listFriends');
        Route::post('friend', 'FriendshipController@create');
        Route::get('friend/sent', 'FriendshipController@listSentRequests');
        Route::put('friend/accept/{cd_amizade}', 'FriendshipController@accept');
        Route::delete('friend/reject/{cd_amizade}', 'FriendshipController@reject');
        Route::delete('friend/sent/{cd_amizade}', 'FriendshipController@cancel');
        Route::delete('friend/{cd_amizade}', 'FriendshipController@deleteFriendship');
        // Route::get('professor/plans/trains', 'PlanController@listTeacherPlanTrains');
        // Route::get('professor/plan/{cd_plano}', 'PlanController@getTeacherPlan');

        // Route::get('aluno/plans', 'PlanController@listStudentPlans');
        // Route::get('aluno/plan/trains', 'PlanController@listStudentPlanTrains');
        // Route::get('aluno/plan/{cd_plano}', 'PlanController@getStudentPlan');

        Route::get('feed', 'FeedController@list');
    });
});

Route::group(['prefix' => 'reports'], function()
{
    Route::get('count-students','ReportsController@countStudents');
    Route::get('count-teachers','ReportsController@countTeachers');
    Route::get('count-gyms','ReportsController@countGyms');
    Route::get('count-exercises','ReportsController@countExercises');
    Route::get('count-trains','ReportsController@countTrains');
});