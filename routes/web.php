<?php
Route::get('/', 'PageController@home');
Route::get('/docs', 'PageController@docs');
Route::get('/termos', 'PageController@terms');

// Admin
Auth::routes();
Route::get('/admin', 'AdminController@loginScreen')->name('admin.login.screen');
Route::post('/admin/login', 'AdminController@login')->name('admin.login');
Route::get('/admin/logout', 'AdminController@logout')->name('admin.logout');
Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function() {
	Route::get('home', 'AdminController@home')->name('admin.home');
	Route::get('students', 'AdminController@students')->name('admin.students');
	Route::get('admins', 'AdminController@admins')->name('admin.admins');
	Route::get('teachers', 'AdminController@teachers')->name('admin.teachers');

	Route::group(['prefix' => 'gyms'], function() {
		Route::get('/', 'AdminController@gyms')->name('admin.gyms.list');
		Route::get('new', 'AdminController@createGymScreen')->name('admin.gyms.new');
		Route::post('new/teste/', 'AdminController@createGym');
	});
});
