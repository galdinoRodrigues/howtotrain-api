# Change Log

## PUT: v2/aluno/{cd_registro}

Antes esse método recebia cd_academia no corpo da request, agora ele recebe um campo **cd_empresa**.

## PUT: v2/professor/{cd_registro}

Antes esse método recebia cd_academia no corpo da request, agora ele recebe um campo **cd_empresa**.


## GET: /v2/gyms

Adicionei no response o campo *url*