window.addEventListener('DOMContentLoaded', function() {
    axios
        .get('/api/reports/count-students')
        .then(function(response) {
            if (response && response.data) {
                document.querySelector('#count-students').textContent = response.data.count;
            }
        })
        .catch(function(error) {});
    axios
        .get('/api/reports/count-teachers')
        .then(function(response) {
            if (response && response.data) {
                document.querySelector('#count-teachers').textContent = response.data.count;
            }
        })
        .catch(function(error) {});
    axios
        .get('/api/reports/count-gyms')
        .then(function(response) {
            if (response && response.data) {
                document.querySelector('#count-gyms').textContent = response.data.count;
            }
        })
        .catch(function(error) {});
    axios
        .get('/api/reports/count-exercises')
        .then(function(response) {
            if (response && response.data) {
                document.querySelector('#count-exercises').textContent = response.data.count;
            }
        })
        .catch(function(error) {});
    axios
        .get('/api/reports/count-trains')
        .then(function(response) {
            if (response && response.data) {
                document.querySelector('#count-trains').textContent = response.data.count;
            }
        })
        .catch(function(error) {});
});