# HowToTrain Node API

## Description

API created for the HowToTrain App API and Admin dashboard.

## Installation

```bash
$ npm install
```

## Running the app

To run the app make sure you have the **.env file** on the root of the project or that you pass all the environment variables through when running.

```bash
# development mode
$ npm run start

# watch mode (for the API)
$ npm run start:dev

# production mode
$ npm run start:prod

# watch mode (for the css and js)
$ npm run gulp
```

## Test

We are currently not using tests in the API but it has the built-in functionality.

```bash
# unit tests
$ npm run test
# e2e tests
$ npm run test:e2e
# test coverage
$ npm run test:cov
```

## Database

The Database of choice is Mysql and is hosted on the cloud. It is easy to run a local version of the database and point to it by changing the .env variables values.
