'use strict'

const gulp = require('gulp');
const sass = require('gulp-sass');
const minify = require('gulp-minify');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');

sass.compiler = require('node-sass');

gulp.task('sass', () =>
	gulp
		.src('./src/views/**/*.scss')
		.pipe( sass.sync().on('error', sass.logError) )
		.pipe( cleanCSS({ compatibility: 'ie9' }) )
		.pipe( rename({ suffix: '.min' }))
		.pipe( gulp.dest('./public/css') )
);

gulp.task('js', () =>
	gulp
		.src('./src/views/**/*.js')
		.pipe(
			minify({
				ext: { min: '.min.js' },
				noSource: true,
			})
		)
		.pipe(gulp.dest('./public/js'))
);

gulp.task('sass:watch', () => {
	gulp.watch('./src/views/**/*.scss',gulp.series('sass'));
	gulp.watch('./src/views/**/*.js',gulp.series('js'));
});