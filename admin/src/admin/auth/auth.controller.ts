import { Controller, Get, Post, Render, Res } from '@nestjs/common';
import { Admin } from 'src/models/admin.model';
import { AdminService } from '../admin.service';

@Controller('/admin')
export class AuthController {
	constructor(
		private adminService: AdminService
	){}

	@Get()
	@Render('admin/auth/login')
	loginPage() {
		// TODO: Check if user is authorized and redirect to home
	}

	@Get('/home')
	@Render('admin/dashboard/dashboard')
	dashboardPage(@Res() res) {
		// TODO: Check if user is unauthorized and return error;
		return res.status(302).redirect('/admin');
	}

	@Post('login')
	login(): Promise<Admin[]> {
		return this.adminService.findAll();
	}
}
