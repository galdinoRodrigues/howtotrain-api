import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Admin } from 'src/models/admin.model';
import { Repository } from 'typeorm';

@Injectable()
export class AdminService {
	constructor(
		@InjectRepository(Admin) private adminRepository: Repository<Admin>
	) {}

	findAll(): Promise<Admin[]> {
		return this.adminRepository.find();
	}
}