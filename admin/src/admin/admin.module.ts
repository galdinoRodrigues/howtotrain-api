import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Admin } from 'src/models/admin.model';
import { AdminService } from './admin.service';
import { AuthController } from './auth/auth.controller';

@Module({
	imports: [
		TypeOrmModule.forFeature([ Admin ])
	],
	controllers: [ AuthController ],
	providers: [ AdminService ]
})
export class AdminModule {}
