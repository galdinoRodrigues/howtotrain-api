import { Controller, Get, Render } from '@nestjs/common';

@Controller()
export class AppController {
	@Get()
	@Render('home/home')
	homePage() {}

	@Get('/api/ping')
	ping() {
		return 'Pong';
	}
}
