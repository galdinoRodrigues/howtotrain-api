import {
	Column,
	CreateDateColumn,
	Entity,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';

// Custom table name
@Entity('admins')
export class Admin {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({ length: 191 })
	name: string;

	@Column({ length: 191 })
	email: string;

	@Column({ length: 191 })
	password: string;

	@Column({ nullable: true })
	remember_token: string;

	@CreateDateColumn({ type: 'timestamp', nullable: true })
	created_at: string;

	@UpdateDateColumn({ type: 'timestamp', nullable: true })
	updated_at: string;
}
