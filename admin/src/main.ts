/* Custom Configs and Imports*/
import * as dotenv from 'dotenv';
import * as path from 'path';
if (process.env.NODE_ENV !== 'prod') {
	const envPath = path.join(__dirname, '..', '.env');
	dotenv.config({ path: envPath });
}

/* Nest Core Imports */
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';

/* Main Function */
async function bootstrap() {
	const port = process.env.PORT || 3000;
	const app = await NestFactory.create<NestExpressApplication>(AppModule);

	app.useStaticAssets(path.join(__dirname, '..', 'public'));
	app.setBaseViewsDir(path.join(__dirname, '..', 'src', 'views'));
	app.setViewEngine('hbs');

	await app.listen(port);
}

/* Running server */
bootstrap();
