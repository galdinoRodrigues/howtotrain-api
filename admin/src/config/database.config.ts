import { TypeOrmModule } from '@nestjs/typeorm';

const connection: TypeOrmModule = {
	type: 'mysql',
	host: process.env.DB_HOST,
	port: parseInt(process.env.DB_PORT),
	username: process.env.DB_USER,
	password: process.env.DB_PASSWORD,
	database: process.env.DB_NAME,
	retryAttempts: 5,
	synchronize: false,
	entities: ['dist/models/*.model{.ts,.js}'],
};

export { connection };
