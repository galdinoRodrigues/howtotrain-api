let lastScroll = null;
window.onscroll = () => scrollFunction();

function scrollFunction() {
	const button = document.querySelector("#scrollTop");
	if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
		button.style.cursor = 'pointer';
		return button.style.opacity = "0.6";
	}
	button.style.cursor = 'default';
	document.querySelector("#scrollTop").style.opacity = "0";
}

function scrollToTop() {
	if (document.querySelector("#scrollTop").style.opacity == '0') {
		lastScroll = null;
		return;
	}
	let dist = (document.body.scrollToTop || document.documentElement.scrollTop) || 0;

	if (lastScroll !== null && lastScroll < dist) {
		lastScroll = null;
		return;
	}
	lastScroll = dist;
	if (dist % 5 != 0 ) {
		dist = parseInt(dist / 5) * 5;
	}
	dist -=  dist > 100 ? dist / 20 : dist / 10;
	if (dist < 10) {
		dist = 0;
	}
	document.body.scrollTop = dist;
	document.documentElement.scrollTop = dist;
	if (dist > 0) {
		setTimeout(() =>  scrollToTop(), 10);
	}
	else lastScroll = null;
}

const carouselButtonRight = document.querySelector('#about>.carousel>.arrow-button.right');
const carouselButtonLeft = document.querySelector('#about>.carousel>.arrow-button.left');

carouselButtonRight
	.addEventListener('click', () => {
		const row = document.querySelector('#about>.carousel>.row');
		const bar = document.querySelector('#about>.carousel>.bar');

		let position =  (isNaN(row.style.left) && row.style.left.indexOf('-') < -1) ? '50%' : (row.style.left || '50%');
		position = parseInt(position.substring(0, position.indexOf('%'))) - 50;

		if (bar.hasAttribute('wait')){
			return;
		}
		bar.setAttribute('wait','1.7s');

		setTimeout(() => bar.removeAttribute('wait'), 1700);

		let current = bar.getAttribute('current');
		let grid = window.getComputedStyle(bar).content;

		if (
			(grid == '"1"' && position == -300) ||
			(grid == '"2"' && position == -100) ||
			(grid == '"3"' && position  == -100)
		) {
			return;
		}

		console.log(grid, position);
		carouselButtonLeft.classList.remove('not-visible');
		if (
			(grid == '"1"' && position == -200) ||
			(grid == '"2"' && position == 0) ||
			(grid == '"3"' && position  <= 0)
		) {
			carouselButtonRight.classList.add('not-visible');
		}

		if (grid === '"3"') {
			row.style.left = '20%';
			document.querySelector('#about>.carousel .item:nth-child(2)').classList.remove('active');
			document.querySelector('#about>.carousel .item:nth-child(3)').classList.add('active');
		} else {
			row.style.left = (position - 50) + '%';
		}
	});

carouselButtonLeft
	.addEventListener('click', () => {
		const row = document.querySelector('#about>.carousel>.row');
		const bar = document.querySelector('#about>.carousel>.bar');

		let position =  (isNaN(row.style.left) && row.style.left.indexOf('-') < -1) ? '50%' : (row.style.left || '50%');
		position = parseInt(position.substring(0, position.indexOf('%'))) + 50;

		if (bar.hasAttribute('wait')){
			return;
		}
		bar.setAttribute('wait','1.7s');
		setTimeout(() => bar.removeAttribute('wait'), 1700);

		let current = bar.getAttribute('current');
		let grid = window.getComputedStyle(bar).content;


		if (position <= 100) {
			carouselButtonLeft.classList.add('not-visible');
		}

		if (position == 100) {
			bar.setAttribute('current', '0');
			bar.style.left = '50%';
			return;
		}


		carouselButtonRight.classList.remove('not-visible');

		if (grid == '"3"') {
			document.querySelector('#about>.carousel .item:nth-child(3)').classList.remove('active');
			document.querySelector('#about>.carousel .item:nth-child(2)').classList.add('active');
		}

		bar.style.opacity = '1';

		row.style.left = ((position + 50) > 50 ? 50 : (position + 50)) + '%';
	});

function validateEmail(email) {
	const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}

function toast(message, color) {
	const toast = document.querySelector('.toast');
	toast.style.background = color;
	toast.innerHTML = message;
	toast.style.top = '32px';
	setTimeout(() => toast.style.top = '-72px', 1800);
}

function sendEmail(from, message, callback) {
	var params = {
		email: from || document.querySelector('#email-from').value,
		message: message || document.querySelector('#email-message').value,
	};

	if (!validateEmail(params.email)) {
		toast("Email inválido!", "red");
		return;
	}
	if (!params.email || !params.message || params.email.trim() == '' || params.message.trim() == '') return;

	var formData = new FormData();

	for (var k in params) {
		formData.append(k, params[k]);
	}

	var request = {
		method: 'POST',
		body: formData
	};

	document.querySelector('.overlay').style.display = 'block';
	setTimeout(() => {
		fetch('/api/emailContato', request)
		.then(response => {
			document.querySelector('.overlay').style.display = 'none';
			if (response.status == 200) {
				document.querySelector('#email-from').value = '';
				document.querySelector('#email-message').value = '';
				toast('Email enviado!', '#02da02');
				if (callback) {
					callback();
				}
			} else{
				toast('Erro ao enviar!', 'red');
			}
		})
		.catch(error => {
			document.querySelector('.overlay').style.display = 'none';
			const toast = document.querySelector('.toast');
			toast('Erro ao enviar!', 'red');
		});
	}, 800);
}

document.querySelector('#send-mail').addEventListener('click', () => {sendEmail();})
document.querySelector('#email-from').onkeypress = function(e) {
	if (!e) e = window.event;
	var keyCode = e.keyCode || e.which;
	if (keyCode == '13') {
		sendEmail();
		return false;
	}
}

document.querySelector('#email-message').onkeypress = function(e) {
	if (!e) e = window.event;
	var keyCode = e.keyCode || e.which;
	if (keyCode == '13') {
		sendEmail();
		return false;
	}
}

const cardBasic = document.querySelector('#pricing .carousel .card:not(.premium)')
if (cardBasic) {
	const smallPriceDiv = cardBasic.querySelector('.small-price');
	const priceDiv = cardBasic.querySelector('.price');
	const priceInput = cardBasic.querySelector('.num-students');
	const priceRange = cardBasic.querySelector('input[type="range"]');
	const max = parseInt(priceRange.getAttribute('max'));
	const min = parseInt(priceRange.getAttribute('min'));

	priceInput.addEventListener('change', function() {
		if (priceInput.value > max) {
			priceInput.value = max;
		}
		if (priceInput.value < min) {
			priceInput.value = min;
		}

		const numAlunos = parseInt(priceInput.value);
		let preco = 14.70;
		if (numAlunos < 100) {
			smallPriceDiv.textContent = 'R$ 0,49/aluno';
			preco = numAlunos * 0.49;
		} else if (numAlunos >= 120) {
			smallPriceDiv.textContent = 'R$ 0,42/aluno';
			preco = numAlunos * 0.42;
		} else {
			smallPriceDiv.textContent = 'R$ 0,42/aluno';
			preco = 120 * 0.42;
		}

		priceRange.value = numAlunos;
		priceInput.value = numAlunos;

		const splitted = preco.toFixed(2).replace(',', '.').split('.');
		const dezenas = splitted[0];
		const decimals = splitted[1];

		priceDiv.innerHTML = 'R$ <span>' + dezenas.padStart(2, '0') +  '</span>,' + decimals.padStart(2, '0') +' /mês';
	});

	priceRange.addEventListener('input', function() {
		const numAlunos = parseInt(priceRange.value);
		let preco = 14.70;
		if (numAlunos < 100) {
			smallPriceDiv.textContent = 'R$ 0,49/aluno';
			preco = numAlunos * 0.49;
		} else if (numAlunos >= 120) {
			smallPriceDiv.textContent = 'R$ 0,42/aluno';
			preco = numAlunos * 0.42;
		} else {
			smallPriceDiv.textContent = 'R$ 0,42/aluno';
			preco = 120 * 0.42;
		}

		priceInput.value = numAlunos;

		const splitted = preco.toFixed(2).replace(',', '.').split('.');
		const dezenas = splitted[0];
		const decimals = splitted[1];

		priceDiv.innerHTML = 'R$ <span>' + dezenas.padStart(2, '0') +  '</span>,' + decimals.padStart(2, '0') +' /mês';
	});
}

const cardBusiness = document.querySelector('#pricing .carousel .card.premium')
if (cardBusiness) {
	const smallPriceDiv = cardBusiness.querySelector('.small-price');
	const priceDiv = cardBusiness.querySelector('.price');
	const priceInput = cardBusiness.querySelector('.num-students');
	const priceRange = cardBusiness.querySelector('input[type="range"]');
	const max = parseInt(priceRange.getAttribute('max'));
	const min = parseInt(priceRange.getAttribute('min'));

	priceInput.addEventListener('change', function() {
		if (priceInput.value > max) {
			priceInput.value = max;
		}
		if (priceInput.value < min) {
			priceInput.value = min;
		}

		const numAlunos = parseInt(priceInput.value);
		let preco = 29.70;
		if (numAlunos < 100) {
			smallPriceDiv.textContent = 'R$ 0,99/aluno';
			preco = numAlunos * 0.99;
		} else if (numAlunos >= 120) {
			smallPriceDiv.textContent = 'R$ 0,89/aluno';
			preco = numAlunos * 0.89;
		} else {
			smallPriceDiv.textContent = 'R$ 0,89/aluno';
			preco = 120 * 0.89;
		}

		priceRange.value = numAlunos;
		priceInput.value = numAlunos;

		const splitted = preco.toFixed(2).replace(',', '.').split('.');
		const dezenas = splitted[0];
		const decimals = splitted[1];

		priceDiv.innerHTML = 'R$ <span>' + dezenas.padStart(2, '0') +  '</span>,' + decimals.padStart(2, '0') +' /mês';
	});

	priceRange.addEventListener('input', function() {
		const numAlunos = parseInt(priceRange.value);
		let preco = 29.70;
		if (numAlunos < 100) {
			smallPriceDiv.textContent = 'R$ 0,99/aluno';
			preco = numAlunos * 0.99;
		} else if (numAlunos >= 120) {
			smallPriceDiv.textContent = 'R$ 0,89/aluno';
			preco = numAlunos * 0.89;
		} else {
			smallPriceDiv.textContent = 'R$ 0,89/aluno';
			preco = 120 * 0.89;
		}

		priceInput.value = numAlunos;

		const splitted = preco.toFixed(2).replace(',', '.').split('.');
		const dezenas = splitted[0];
		const decimals = splitted[1];

		priceDiv.innerHTML = 'R$ <span>' + dezenas.padStart(2, '0') +  '</span>,' + decimals.padStart(2, '0') +' /mês';
	});
}

let subscribeButtons = document.querySelectorAll('.subscribe');
if (subscribeButtons) {
	const subscribeOverlay = document.querySelector('.subscribe-overlay');
	const closeOverlay = document.querySelector('.close-overlay');
	const submitButton = document.querySelector('.subscribe-overlay .box button');
	const headerIntro = document.querySelector('.subscribe-overlay .box .intro');

	closeOverlay.addEventListener('click', () => {
		subscribeOverlay.classList.remove('active');
	});

	submitButton.addEventListener('click', () => {
		let message = '';
		const from = document.querySelector('.subscribe-overlay .email').value;

		const phone = document.querySelector('.subscribe-overlay .phone').value;
		const name = document.querySelector('.subscribe-overlay .name').value;

		if (!name.trim()) {
			return toast("Nome é obrigatório!", "red");
		}
		if (!from.trim()) {
			return toast("E-mail é obrigatório!", "red");
		}
		if (!phone.trim()) {
			return toast("Telefone é obrigatório!", "red");
		}

		message = name + ' de telefone ' + phone + ' solicitou uma estimativa hoje ' + new Date().toLocaleDateString() + ' as ' + new Date().getHours().toString().padStart(2, '0') + ':' + new Date().getMinutes().toString().padStart(2, '0') + ' solicitando ' + headerIntro.textContent;

		sendEmail(from, message, () => {
			subscribeOverlay.classList.remove('active');
		});
	});

	subscribeButtons.forEach(subBtn => {
		subBtn.addEventListener('click', () => {
			if (subscribeOverlay.classList.contains('active')) {
				subscribeOverlay.classList.remove('active');
				return;
			}

			subscribeOverlay.classList.add('active');
			if (subBtn.classList.contains('basic')) {
				submitButton.classList.remove('premium');
				headerIntro.classList.remove('premium');

				const price = document.querySelector('.card:not(.premium) .price').textContent.trim();
				const numStudents = document.querySelector('.card:not(.premium) .num-students').value;

				headerIntro.innerHTML = 'Plano Premium <br> <span>' + numStudents + ' alunos por ' + price + '</span>';

			} else if (subBtn.classList.contains('premium')) {
				submitButton.classList.add('premium');
				headerIntro.classList.add('premium');

				const price = document.querySelector('.card.premium .price').textContent.trim();
				const numStudents = document.querySelector('.card.premium .num-students').value;

				headerIntro.innerHTML = 'Plano Premium <br> <span>' + numStudents + ' alunos por ' + price + '</span>';
			}
		});
	});
}

function mascara(o,f){
	v_obj=o
	v_fun=f
	setTimeout("execmascara()",1)
}

function execmascara(){
	v_obj.value=v_fun(v_obj.value)
}

function mtel(v){
	v=v.replace(/\D/g,"");
	v=v.replace(/^(\d{2})(\d)/g,"($1) $2");
	v=v.replace(/(\d)(\d{4})$/,"$1-$2");
	return v;
}


// if (Flickity) {
// 	new Flickity('.main-carousel', {
// 		cellAlign: 'left',
// 		contain: true
// 	});
// }