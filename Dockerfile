FROM php:7.1-fpm-alpine

RUN docker-php-ext-install pdo pdo_mysql mbstring

RUN apk --update add curl

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer
